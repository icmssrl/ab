jQuery.sap.declare("model.ODS");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Storage");

model.ODS =
{
		storageId: "ODS",

	    getModel: function()
	    {
	    	if (this.oModel === undefined)
    		{
					this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get("ODS"));
    		}
	    	return this.oModel;
	    },

	    saveModel: function()
	    {
	    	util.Storage.put(this.storageId, this.oModel.oData);
	    },

	    oDataToStorage: function(success, error)
	    {
	    	c = this;

	    	model.Fiori.getODS
	    	(
    			function(data){
    				delete data.__metadata;
							console.log("--To Storage ODS 8--");
    				console.log("---- ODS -----");
    				console.log(data);
    				util.Storage.put("ODS", data);
    				c.oModel = undefined;
    				success();
    			},
    			error
	    	);
	    },

	    getMaterialsInODS: function(Aufnr)
	    {
	    	var m = {results: []};
	    	var oModel = this.getModel();
	    	var results = oModel.oData.results;
	    	for (var i = 0; i < results.length; i++) {
	    		var item = results[i];
	    		var materials = item.Materials.results;
	    		if (item.Aufnr == Aufnr)
	    		{
	    			for (var j = 0; j < materials.length; j++) {
	    				var material = materials[j];
	    			  	if (material.Aufnr !== "" && material.Matnr !== "ZINSPECTION")
    			  		{
									material.QtyReq = parseFloat(material.QtyReq.toString().replace(",", "."));
									material.QtyIssue = parseFloat(material.QtyIssue.toString().replace(",", "."));
									material.QtyStock = parseFloat(material.QtyStock.toString().replace(",", "."));
									//FIXME Da sostituire con un formatter quando c'è tempo
    			  			material.QtyReq = material.QtyReq.toString().replace(".", ",");
    			  			material.QtyIssue = material.QtyIssue.toString().replace(".", ",");
    			  			material.QtyStock = material.QtyStock.toString().replace(".", ",");
	    			  		m.results.push(material);
    			  		}
	    			}
	    			break;
	    		}
	    	}
	    	return new sap.ui.model.json.JSONModel(m);
	    },

	    getMaterialsInStock: function(Aufnr)
	    {
	    	var m = {results: []};
	    	var oModel = this.getModel();
	    	var results = oModel.oData.results;
	    	for (var i = 0; i < results.length; i++) {
	    		var item = results[i];
	    		var materials = item.Materials.results;
	    		if (item.Aufnr == Aufnr )
	    		{
	    			for (var j = 0; j < materials.length; j++) {
	    				var material = materials[j];
	    			  	if ((material.Aufnr === "" || material.Aufnr === undefined)&&(material.Matnr !== "ZINSPECTION"))
    			  		{
    			  			material.QtyReq = parseFloat(material.QtyReq).toString();
    			  			material.QtyIssue = parseFloat(material.QtyIssue).toString();
    			  			material.QtyStock = parseFloat(material.QtyStock).toString();
	    			  		m.results.push(material);
    			  		}
	    			}
	    			break;
	    		}
	    	}
	    	return new sap.ui.model.json.JSONModel(m);
	    },

	    getODS: function(Aufnr)
	    {
	    	var oModel = this.getModel();
	    	var results = oModel.oData.results;
	    	for (var i = 0; i < results.length; i++) {
	    		var item = results[i];
	    		if (item.Aufnr == Aufnr)
	    		{
	    			return new sap.ui.model.json.JSONModel(item);
	    		}
	    	}
	    },

	    remove: function()
	    {
	    	util.Storage.remove(this.storageId);
	    	this.oModel = undefined;
	    },
};
