jQuery.sap.declare("model.Countries");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("util.Storage");

model.Countries = {

	storageId: "countries",

	getModel: function () {
		if (this.oModel === undefined) {
			this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get("countries"));
		}
		return this.oModel;
	},

	oDataToStorage: function (success, error) {
		c = this;
		model.Fiori.getCountries(
			function (data) {
				delete data.__metadata;
				console.log("--Country To Storage 1--");
				data.results.unshift({Land1: "", Landx: ""});
				util.Storage.put("countries", data);
				c.getModel().setData(data);
				success();
			},
			error
		);
	},

	remove: function () {
		util.Storage.remove(this.storageId);
		this.oModel = undefined;
	},

};
