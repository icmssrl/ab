jQuery.sap.declare("model.Credential");

jQuery.sap.require("util.Storage");

model.Credential = 
{
		storageId: "credentials",
		
		getModel: function()
	    {
	    	if (this.oModel === undefined)
    		{
    			var data = util.Storage.get(this.storageId);
    			if (data === undefined || data === null)
    			{
    				data = this.newCredential();
    			}
				this.oModel = new sap.ui.model.json.JSONModel(data);
    		}
	    	return this.oModel;
	    },

	    saveModel: function()
	    {
	    	var data = this.getModel().getProperty("/");
	    	util.Storage.put(this.storageId, data);
	    },

	    newCredential: function()
	    {
	    	var credential = {
	    		username: "",
	    		password: "",
	    	};
	    	return credential;
	    },

	    remove: function()
	    {
	    	util.Storage.remove(this.storageId);
	    	this.oModel = undefined;
	    },

	    getUsername: function()
	    {
	    	return this.getModel().getProperty("/username");
	    },

	    getPassword: function()
	    {
	    	return this.getModel().getProperty("/password");
	    },
};