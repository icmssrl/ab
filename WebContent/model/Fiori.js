jQuery.sap.declare("model.Fiori");
jQuery.sap.require("setting.Core");

jQuery.sap.require("model.Credential");

jQuery.sap.require("model.Timesheet");


// jQuery.sap.require("util.DebugDate");

model.Fiori = {
    serviceUrl: setting.Core.serverUrl + "ZFIORI_SRV",

    getODataModel: function () {
        if (this.model === undefined) {
            this.oDataModel = new sap.ui.model.odata.ODataModel(this.serviceUrl, true);
        }
        // this.oDataModel = new sap.ui.model.odata.ODataModel(this.serviceUrl, true, model.Credential.getUsername(), model.Credential.getPassword());
        // this.oDataModel = new sap.ui.model.odata.ODataModel(this.serviceUrl, true, "devel1", "Sapdev00");
        return this.oDataModel;
    },

    getUser: function (success, error) {
        this.getODataModel().read(
            "/UserSet('')",
            null,
            null,
            true,
            success,
            error
        );
    },

    getTimesheet: function (success, error) {
        this.getODataModel().read(
            "/TimesheetSet?$expand=ODS",
            null,
            null,
            true,
            success,
            error
        );
    },

    getTimesheetList: function (success, error) {
        this.getODataModel().read(
            "/TimesheetListSet",
            null,
            null,
            true,
            success,
            error
        );
    },

    getODS: function (success, error) {
        this.getODataModel().read(
            "/ODSSet?$expand=Materials,Fasi,Text",
            null,
            null,
            true,
            success,
            error
        );
    },

    getODST: function (success, error) {
        this.getODataModel().read(
            "/ODSTSet",
            null,
            null,
            true,
            success,
            error
        );
    },

    getMaterials: function (orderId, success, error) {
        this.getODataModel().read(
            "/MaterialSet?$filter=Aufnr eq '" + orderId + "'",
            null,
            null,
            true,
            success,
            error
        );
    },

    getCountries: function (success, error) {
        this.getODataModel().read(
            "/CountrySet",
            null,
            null,
            true,
            success,
            error
        );
    },

    updateTimesheet: function (Pernr, Aufnr, DateAu, oData, success, error) {
        var sPath = "TimesheetSet(Pernr='" + Pernr + "',Aufnr='" + Aufnr + "',DateAu=datetime'" + DateAu + "')";
        this.getODataModel().update(sPath, oData, null, success, error);
    },

    createTimesheet: function (oData, success, error) {
        var sPath = "TimesheetSet";
        this.getODataModel().create(sPath, oData, null, success, error);
    },

    synchronizeTimesheet: function (success, error) {
        var results = model.Timesheet.getModel().oData.results;
        console.log("--Synchro Timesheet 4--");
        if (results === undefined) {
            success();
            return;
        }

        if (results.length === 0) {
            success();
            return;
        }

        var oDataModel = this.getODataModel();
        var batchChanges = [];

        delete results.ODS;
        delete results.__metadata;

        for (var i = 0; i < results.length; i++) {
            var entry = results[i];
            delete entry.ODS;
            delete entry.__metadata;
            entry.DateAu = util.Formatter.dateTimeSapEntry(entry.DateAu);
            entry.Insdate = util.Formatter.dateTimeSapEntry(entry.Insdate);
            entry.Elabdat = util.Formatter.dateTimeSapEntry(entry.Elabdata);
            entry.Transfer = entry.Transfer;
            entry.DirChiamata = util.Formatter.flagToSap(entry.DirChiamata);
            entry.Elab = util.Formatter.flagToSap(entry.Elab);
            entry.Km = entry.Km.toString();
            if (!entry.Km) {
                entry.Km = "0";
            }
            batchChanges.push(oDataModel.createBatchOperation("TimesheetSet", "POST", entry));
        }
        oDataModel.addBatchChangeOperations(batchChanges);
        oDataModel.submitBatch(
            success,
            error,
            false
        );

    },

    synchronizeMaterials: function (success, error) {


        var results = model.ODS.getModel().oData.results;

        console.log("--Synchro Material 5--");
        if (results === undefined) {
            //            success(); // aggiunte funzioni -> FABIO 17/02/2016
            //            return; // aggiunte funzioni -> FABIO 17/02/2016
            var deferVuotoMat = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuotoMat.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuotoMat.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }

        if (results.length === 0) {
            //            success(); // aggiunte funzioni -> FABIO 17/02/2016
            //            return; // aggiunte funzioni -> FABIO 17/02/2016
            var deferVuotoMat = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuotoMat.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuotoMat.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }
        var deferMaterials = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016

        var oDataModel = this.getODataModel();
        var batchChanges = [];

        delete results.ODS;
        delete results.__metadata;

        for (var i = 0; i < results.length; i++) {
            var materials = results[i].Materials.results;

            for (var j = 0; j < materials.length; j++) {
                var entry = materials[j];

                if (entry.Status == "A" || entry.Status == "B") {
                    delete entry.ODS;
                    delete entry.__metadata;
                    /* Fabio 14/04/2016 */
//                    if(entry.check){
                        delete entry.check;
                    /* Fabio 19/10/2016 */
                        delete entry.toSync;
//                    }
                    /* Fabio 14/04/2016 */
                    entry.Aufnr = results[i].Aufnr;
                    entry.QtyReq = parseFloat(entry.QtyReq.toString().replace(",", ".")).toString();
                    entry.QtyIssue = parseFloat(entry.QtyIssue.toString().replace(",", ".")).toString();
                    if (entry.QtyIssue_state) {
                        delete entry.QtyIssue_state;
                    }
                    entry.QtyStock = parseFloat(entry.QtyStock.toString().replace(",", ".")).toString();
                    batchChanges.push(oDataModel.createBatchOperation("MaterialSet", "POST", entry)).toString();
                }
            }
        }

        // aggiunte funzioni -> FABIO 17/02/2016
        var fsuccess = function (result) {
            console.log("-- successo sincro Materiali --");

            if (result) {
                var ff = result.__batchResponses[0].response;
                if (ff) {
                    ff = ff.body;
                    var errorFromSap = JSON.parse(ff).error.message.value;
                    if (errorFromSap) {
                        deferMaterials.reject(errorFromSap)
                    }
                }
            }
            deferMaterials.resolve(result);
        };

        var ferror = function (err) {
            console.log(err.message);
            deferMaterials.reject(err)
        };

        fsuccess = _.bind(fsuccess, this);
        ferror = _.bind(ferror, this);
        // 

        //Controllo se ho dei dati da fare l'upload e carico
        if (batchChanges.length > 0) {
            oDataModel.addBatchChangeOperations(batchChanges);
            oDataModel.submitBatch(
                fsuccess,
                ferror,
                false
            );
        } else {
            //            fsuccess();
            var deferVuotoMat = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuotoMat.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuotoMat.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }

        return deferMaterials.promise; // aggiunte funzioni -> FABIO 17/02/2016
    },

    synchronizeFasi: function (success, error) {
        var results = model.ODS.getModel().oData.results;
        console.log("--Synchro Fasi 6--");
        if (results === undefined) {
            success();
            return;
        }

        if (results.length === 0) {
            success();
            return;
        }

        var oDataModel = this.getODataModel();
        var batchChanges = [];

        delete results.ODS;
        delete results.__metadata;

        for (var i = 0; i < results.length; i++) {
            var data = results[i].Fasi.results;

            for (var j = 0; j < data.length; j++) {
                var entry = data[j];

                delete entry.ODS;
                delete entry.__metadata;
                delete entry.toSync;

                batchChanges.push(oDataModel.createBatchOperation("FasiSet", "POST", entry));
            }
        }

        //Controllo se ho dei dati da fare l'upload e carico
        if (batchChanges.length > 0) {
            oDataModel.addBatchChangeOperations(batchChanges);
            oDataModel.submitBatch(
                success,
                error,
                false
            );
        } else {
            success();
        }

    },

    synchronizeODS: function (success, error) {
        var ods = model.ODS.getModel().oData.results;
        console.log("--Synchro ODS 7--");
        if (ods === undefined) {
            //            success(); // aggiunte funzioni -> FABIO 17/02/2016
            //            return; // aggiunte funzioni -> FABIO 17/02/2016
            var deferVuoto = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuoto.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuoto.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }

        if (ods.length === 0) {
            //            success(); // aggiunte funzioni -> FABIO 17/02/2016
            //            return; // aggiunte funzioni -> FABIO 17/02/2016
            var deferVuoto = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuoto.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuoto.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }
        var deferODS = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016

        var oDataModel = this.getODataModel();
        var batchChanges = [];

        for (var i = 0; i < ods.length; i++) {
            // Scorro i materiali dell'ordine per vedere se è chiuso
            var isClose = false;
            var materials = ods[i].Materials.results;
            for (var j = 0; j < materials.length; j++) {
                var status = materials[j].Status;
                if (status === "B") {
                    isClose = true;
                    break;
                }
            }

            // Se l'ordine è chiuso lo invio come da inserire
            if (isClose) {
                var entry = {};
                entry.Aufnr = ods[i].Aufnr;
                entry.Itime = ods[i].Itime;
                if (ods[i].Itime.ms < 0) {
                    ods[i].Itime.ms = Math.abs(ods[i].Itime.ms)
                }
                /* Fabio 11/04/2016 */
                entry.Idate = util.Formatter.dateTimeSapEntry(ods[i].Idate)
                    //                entry.Idate = ods[i].Idate;
                    /* Fabio 11/04/2016 */
                entry.Recdv = ods[i].Recdv;
                //---Maybe it's necessary to insert technical quarter and description//
                batchChanges.push(oDataModel.createBatchOperation("ODSSet", "POST", entry));
                console.log("----- INSERISCO UN ODS---------");
                console.log(entry);
            }
        }

        // aggiunte funzioni -> FABIO 17/02/2016
        var fsuccess = function (result) {
            console.log("-- successo sincro ODS --");

            if (result) {
                var ff = result.__batchResponses[0].response;
                if (ff) {
                    ff = ff.body;
                    var errorFromSap = JSON.parse(ff).error.message.value;
                    if (errorFromSap) {
                        deferODS.reject(errorFromSap)
                    }
                }
            }
            deferODS.resolve(result);
        };

        var ferror = function (err) {
            console.log(err.message);
            deferODS.reject(err)
        };

        fsuccess = _.bind(fsuccess, this);
        ferror = _.bind(ferror, this);
        // 

        //Controllo se ho dei dati da fare l'upload e carico
        if (batchChanges.length > 0) {
            oDataModel.addBatchChangeOperations(batchChanges);
            oDataModel.submitBatch(
                fsuccess,
                ferror,
                false
            );
        } else {
            //            fsuccess();
            var deferVuoto = Q.defer(); // aggiunte funzioni -> FABIO 17/02/2016
            deferVuoto.resolve(); // aggiunte funzioni -> FABIO 17/02/2016
            return deferVuoto.promise; // aggiunte funzioni -> FABIO 17/02/2016
        }

        return deferODS.promise; // aggiunte funzioni -> FABIO 17/02/2016
    },

    success: function (e) {
        console.log("successo");
        console.log(e);
    },

    error: function (e) {
        console.log("errore");
        console.log(e);
    },


};
