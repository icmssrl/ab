jQuery.sap.declare("model.i18n");

model.i18n = {
    getModel: function() {
        if (this.model === undefined) {
            this.model = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "i18n/text.properties"
            });
            this.T = this.model.getResourceBundle();
        }
        return this.model;
    },
    getData: function() {
        var o = this.getModel().getObject("/");
        if (o === null) {
            return undefined;
        }
        return o;
    }

};
