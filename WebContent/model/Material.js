jQuery.sap.declare("model.Material");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Formatter");

model.Material = 
{
	    getModel: function(orderId, success, error)
	    {
	    	model.Fiori.getMaterials(orderId, $.proxy(this.getMaterialsSuccess, this, success), error);
	    },
	    
	    getMaterialsSuccess: function(success, data)
	    {
	    	delete data.__metadata;
	    	this.data = data;
	    	this.oModel = new sap.ui.model.json.JSONModel(this.data);
	    	success(this.oModel);
	    },

	    remove: function()
	    {
	    	util.Storage.remove(this.storageId);
	    	this.oModel = undefined;
	    },
};
