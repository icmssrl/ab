jQuery.sap.declare("model.Synchronize");
jQuery.sap.require("util.Formatter");

model.Synchronize =
{
  _dateTime:"",


  setDateTime : function(lang)
  {
    this._dateTime = util.Formatter.getCurrentDateTimeToShow(lang);
  },
  getDateTime : function()
  {
    return this._dateTime;
  },
  isSynchronized: function()
  {
    if(this._dateTime =="")
    {
      return false;
    }
    else {
      return true;
    }
  }
  // getModel:function()
  // {
  //   this.synchroModel = new sap.ui.model.json.JSONModel();
  //   this.synchroModel.setData(this);
  //   return this.synchroModel;
  // }
}
