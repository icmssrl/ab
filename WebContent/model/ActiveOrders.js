jQuery.sap.require("util.Storage");
jQuery.sap.declare("model.ActiveOrders");
model.ActiveOrders =
{
  _orders :[],

  setOrdersToClose: function(items)
  {
    this._orders = items;

  },
  getOrders : function()
  {
    return this._orders;
  },
  getOrder: function(pos)
  {
    if(pos<0)
      return null;

    return this._orders[pos];
  },
  getOrderValue:function(pos)
  {
    if(pos<0)
      return null;

    return this._orders[pos].value;
  },
  getOrderPath:function(pos)
  {
    if(pos<0)
      return null;
    return this_order[pos].path;
  },
  getOrdersValues:function()
  {
    var result = [];
    for(var i= 0; i< this._orders.length; i++)
    {
      result.push(this._orders[i].value);
    }
    return result;
  },
  reset:function()
  {
    this._orders=[];
  }


}
