jQuery.sap.declare("model.Timesheet");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Storage");



model.Timesheet = {
	storageId: "timesheet",

	getModel: function () {
		if (this.oModel === undefined) {
			this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get("timesheet"));
		}
		return this.oModel;

	},

	oDataToStorage: function (success, error) {
		c = this;
		model.Fiori.getTimesheet(
			function (data) {
				delete data.__metadata;
				console.log("--Timesheet To Storage 10--");
				if(data.results && data.results.length>0)
				{
					for(var i = 0; i<data.results.length; i++)
					{
						if(data.results[i].Wrend.ms === 0 && data.results[i].Wrbeg.ms>0)
						{
							data.results[i].Wrend.ms = 24*60*60*1000;
						}
						data.results[i].Insdate = util.Formatter.dateTimeFromSap(data.results[i].Insdate);
						data.results[i].DateAu = util.Formatter.dateTimeFromSap(data.results[i].DateAu);
						data.results[i].Elabdat = util.Formatter.dateTimeFromSap(data.results[i].Elabdata);
					}
				}


				util.Storage.put("timesheet", data);
				c.oModel = undefined;
				success();
			},
			error
		);
	},

	getDetailModel: function (Pernr, Aufnr, DateAu, Wrbeg) {
		DateAu = new Date(DateAu);

		var oModel = this.getModel();
		var results = oModel.oData.results;
		for (var i = 0; i < results.length; i++) {
			var item = results[i];
			var dateTmp = new Date(item.DateAu).toDateString();
			if (item.Aufnr == Aufnr && dateTmp == DateAu.toDateString() && item.Wrbeg.ms == Wrbeg && item.Elab !== 'D') {
				if (!item.DirChiamata) {
					item.DirChiamata = false;
				} else {
					item.DirChiamata = true;
				}
				if (!item.Elab) {
					item.Elab = false;
				} else {
					item.Elab = true;
				}
				item.Km = parseInt(item.Km);
				if (item.Km.toString() === "NaN" || item.Km === 0) {
					item.Km = "";
				}
				return new sap.ui.model.json.JSONModel(item);
			}
		}

	},

	getDetailEmptyModel: function (optDate) {
		var report = {};

		report.Pernr = "";
		report.Aufnr = "";

		if (optDate) {
			report.DateAu = new Date(optDate.getTime());
		} else {
			report.DateAu = new Date();
		}

		//	        report.DateAu = optDate ? new Date(optDate): new Date();
		report.Trpin = {
			__edmType: "Edm.Time",
			ms: 0
		};
		report.Trpout = {
			__edmType: "Edm.Time",
			ms: 0
		};
		report.Wrbeg = {
			__edmType: "Edm.Time",
			ms: 0
		};
		report.Wrend = {
			__edmType: "Edm.Time",
			ms: 0
		};
		report.Pause = {
			__edmType: "Edm.Time",
			ms: 0
		};
		report.Km = "";
		report.Transfer = "";
		report.DirChiamata = "";
		report.Elab = "";
		report.Insdate = new Date();
		report.Elabdat = null; //new Date();
		report.Elabtime = null; //util.Formatter.timeSap("");
		report.Testo = "";
		report.ODS = {};
		report.ODS.Name1 = "";
		report.Targa = "";
		return new sap.ui.model.json.JSONModel(report);
	},

	save: function () {
		console.log(this.oModel.oData);
		util.Storage.put(this.storageId, this.oModel.oData);
	},

	remove: function () {
		util.Storage.remove(this.storageId);
		this.oModel = undefined;
	},
};
