jQuery.sap.declare("model.TimesheetList");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Storage");

model.TimesheetList =
{
		storageId: "timesheetList",

	    getModel: function()
	    {
	    	if (this.oModel === undefined)
    		{
					this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get(model.TimesheetList.storageId));
    		}
	    	return this.oModel;
	    },

	    oDataToStorage: function(success, error)
	    {
	    	c = this;
	    	model.Fiori.getTimesheetList
	    	(
    			function(data){
    				delete data.__metadata;
						console.log("TIMESHEETLIST To Storage 3");
						console.log(data);
    				util.Storage.put(model.TimesheetList.storageId, data);
    				c.oModel = undefined;
    				success();
    			},
    			error
	    	);
	    },

	    save: function()
	    {
	    	console.log(this.oModel.oData);
	    	util.Storage.put(model.TimesheetList.storageId, this.oModel.oData);
	    },

	    remove: function()
	    {
	    	util.Storage.remove(model.TimesheetList.storageId);
	    	this.oModel = undefined;
	    },
};
