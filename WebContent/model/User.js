jQuery.sap.declare("model.User");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Storage");

model.User =
{
		storageId: "user",

		getModel: function()
	    {
	    	if (this.oModel === undefined)
    		{
				this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get("user"));
    		}
	    	return this.oModel;
	    },

	    oDataToStorage: function(success, error)
	    {
	    	c = this;
	    	model.Fiori.getUser
	    	(
    			function(data){
    				delete data.__metadata;
						console.log("--User To Storage 2--");
    				util.Storage.put("user", data);
    				c.oModel.setData(data);
    				success();
    			},
    			error
	    	);
	    },

		logout : function(success, error) {
			$.ajax({
				type : "GET",
				url : setting.Core.logoffUrl, // Clear SSO cookies: SAP Provided
												// service to do that
			}).done(success).fail(error);
		},

		remove: function()
	    {
	    	util.Storage.remove(this.storageId);
	    	this.oModel = undefined;
	    },




};
