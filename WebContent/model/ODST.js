jQuery.sap.declare("model.ODST");
jQuery.sap.require("model.Fiori");

jQuery.sap.require("util.Storage");

model.ODST =
{
		storageId: "ODST",

	    getModel: function()
	    {
	    	if (this.oModel === undefined)
    		{
				this.oModel = new sap.ui.model.json.JSONModel(util.Storage.get("ODST"));
    		}
	    	return this.oModel;
	    },

	    saveModel: function()
	    {
	    	util.Storage.put(this.storageId, this.oModel.oData);
	    },

	    oDataToStorage: function(success, error)
	    {
	    	c = this;

	    	model.Fiori.getODST
	    	(
    			function(data){
    				delete data.__metadata;
						console.log("--ODST To Storage 9--");
    				console.log(data);
    				util.Storage.put("ODST", data);
    				c.oModel = undefined;
    				success();
    			},
    			error
	    	);
	    },

	    remove: function()
	    {
	    	util.Storage.remove(this.storageId);
	    	this.oModel = undefined;
	    },
};
