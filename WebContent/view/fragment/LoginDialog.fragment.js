sap.ui.jsfragment("view.fragment.LoginDialog", {
	createContent : function(oController) {
		var dialog = new sap.m.Dialog({
			title: "{i18n>LOGIN}",
			content: 
				[
					this.userInput = new sap.m.Input({
					    placeholder: "{i18n>EMAIL}",
					}),
					this.passwordInput = new sap.m.Input({
					    placeholder: "{i18n>PASSWORD}",
					    type: "Password",
					}),
//					this.logon = new sap.m.Button({
//					    text: "{i18n>CONFIRM}",
//					    press: [oController.onSynchronizeConPress, oController]
//					}),
				 ],
			 beginButton: new sap.m.Button({
				text: "{i18n>CONFIRM}",
				press: [oController.onDialogConfirmPress, oController]
			 }),
			 endButton: new sap.m.Button({
				text: "{i18n>CANCEL}",
				press: [oController.onDialogCancelPress, oController] 
			 })
		});
		return dialog;
	}
});
