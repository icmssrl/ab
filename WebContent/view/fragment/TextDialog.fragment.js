sap.ui.jsfragment("view.fragment.TextDialog", {
	createContent : function(oController) {
		var dialog = new sap.m.Dialog({
			title: "{i18n>ORDER} {order>/Aufnr}",
			endButton: new sap.m.Button({
				text: "{i18n>OK}",
				press: [oController.onTextClose, oController]
			}),

			// content:[
			// 	this.list = new sap.m.List({
			// 		items: {
			// 			path: "order>/Text/results",
						// template: new sap.m.CustomListItem({
						// 	content: [
						// 		new sap.m.Text({
						// 			text:
						// 			{
						// 				path:"order>Tdline",
						// 				formatter: function(value)
						// 				{
						// 					console.log(value);
						// 					return value;
						// 				}
						// 			},
						// 			// editable: false
						// 		}),
						//
						// 	]
						// })
						// template: new sap.m.FeedListItem
						// ({
						// 	icon:"sap-icon://notification",
						// 	text:"{text>text}"
						// })
			// 		}
			// 	}),
			// ]
			content:
			[
				new sap.m.FeedListItem
				({
				 	icon:"sap-icon://notification",
					text:"{text>/text}"
				})
			]
		});

		dialog.addContent(this.list);

		return dialog;
	}
});
