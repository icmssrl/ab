sap.ui.jsfragment("view.fragment.OrdersDialog", {
	createContent: function (oController) {
		var dialog = new sap.m.TableSelectDialog({
			confirm: [oController.onOrdersConfirm, oController],
			liveChange: [oController.onOrdersSearch, oController],
			columns: [new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay : "Inline",
				header: new sap.m.Label({
					text: "{i18n>ORDER}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay : "Inline",
				header: new sap.m.Label({
					text: "{i18n>DESCRIPTION}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay : "Inline",
				header: new sap.m.Label({
					text: "{i18n>DATE}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay : "Inline",
				header: new sap.m.Label({
					text: "{i18n>CLIENT}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay : "Inline",
				header: new sap.m.Label({
					text: "{i18n>NAME}",
					design: sap.m.LabelDesign.Bold
				})
			}), ],

		});

		var template = new sap.m.ColumnListItem({
			cells: [new sap.m.Text({
				text: "{orders>Aufnr}"
			}), new sap.m.Text({
				text: "{orders>Ktext}"
			}), new sap.m.Text({
				text: {
					path: "orders>Gstrp",
					formatter: util.Formatter.date,
				},
			}), new sap.m.Text({
				text: "{orders>Kunum}"
			}), new sap.m.Text({
				text: "{orders>Name1}"
			}), ]
		});

		dialog.bindAggregation("items", {
			path: "orders>/results",
			template: template,
		});

		return dialog;
	}
});