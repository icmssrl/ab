sap.ui.jsfragment("view.fragment.MultiCloseDialog", {
	createContent : function(oController) {
		var dialog = new sap.m.Dialog({
			title: "{i18n>MULTICONFIRM}",
			stretch:true,
			beginButton: new sap.m.Button({
				text: "{i18n>OK}",
				press: [oController.closeConfirmMulti, oController]
			}),
			endButton: new sap.m.Button({
				text: "{i18n>CANCEL}",
				press: [oController.multiCloseCancel, oController]
			}),
			content:[
				this.form = new sap.ui.layout.form.SimpleForm({
					layout : "ResponsiveGridLayout",
					labelSpanL : 12,
					labelSpanM : 12,
					labelSpanS : 12,
					emptySpanL : 1,
					emptySpanM : 1,
					emptySpanS : 1,
					columnsL : 2,
					columnsM : 2,
					editable : true,
					content :
					[
						new sap.ui.core.Title({
							text : "{i18n>CLOSE_CONFIRM}"
						}),
						new sap.m.Label({
							text : "{i18n>ACTUAL_COUNTER}"
						}),
						new sap.m.Input({
							type: "Number",
							enabled: true,
							value: "{multi>/actualrecdv}",
							valueState: "{multi>/actualrecdv_state}",
							change:[oController.onCounterChange, oController]
						}),
                        new sap.m.Label({
                            text : "{i18n>CLOSE_DATE}"
                        }),
                        new sap.m.DatePicker({
                            displayFormat: "{enabledFG>/lang}",
//                            displayFormat: "dd-MM-yyyy", // Fabio 18/04/2016
                            valueFormat: "{enabledFG>/lang}",
//                            valueFormat: "dd-MM-yyyy", // Fabio 18/04/2016
                            valueState: "{multi>/closeDate_state}",
                            dateValue: {
                                path : "multi>/closeDate",
                                formatter : util.Formatter.DateISOtoDate
                            },
                            change: [oController.onMultiContDateChange, oController]
                        }),
						new sap.m.Label({
							text : "{i18n>LAST_READ_COUNTER}"
						}),
						new sap.m.Input({
							enabled: false,
							value: "{multi>/lastCounter}"
						}),
                        new sap.m.Label({
                            text : "{i18n>LAST_READ_DATE}"
                        }),
						new sap.m.Input({
							enabled: false,
							value: {
								path : "multi>/lastReadDate",
								formatter : util.Formatter.date
							}
						}),
						new sap.m.Label({
							text : "{i18n>LAST_READ_TIME}"
						}),
						new sap.m.Input({
							enabled: false,
							value: {
								path : "multi>/lastReadTime",
								formatter : util.Formatter.time
							}
						}),
					]
				}),
			]
		});
		this.orderList = new sap.m.List({

		headerDesign: "Plain",

		// mode: "SingleSelectMaster",
		mode: "None",
		// growing:true,
		// growingThreshold:20,
		columns: [new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				demandPopin: true,
				minScreenWidth:"Tablet",
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>ORDER}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				// header: new sap.m.Label({
				// 	text: "{i18n>DESCRIPTION}",
				// 	design: sap.m.LabelDesign.Bold
				// })
			}),
			 new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>DATE}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			 new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>CLIENT}",
					design: sap.m.LabelDesign.Bold
				})
			})
		// new sap.m.Column({
		// 	hAlign : "Center",
		// 	vAlign : "Middle",
		// 	header : new sap.m.Label({
		// 		design : sap.m.LabelDesign.Bold
		// 	})
		// }),new sap.m.Column({
		// 	hAlign : "Center",
		// 	vAlign : "Middle",
		// 	header : new sap.m.Label({
		// 		design : sap.m.LabelDesign.Bold
		// 	})
		// }),
		]
	});
	var template = new sap.m.ColumnListItem({
		type: "Inactive",

		cells: [new sap.m.Text({
				text: "{multi>Aufnr}"
			}).addStyleClass("multiText"),
			 new sap.m.Text({
				text: "{multi>Ktext}"
			}).addStyleClass("multiText"),
			 new sap.m.Text({
				text: {
					path: "multi>Gstrp",
					formatter: util.Formatter.date
				}
			}).addStyleClass("multiText"),
			 new sap.m.Text({
				text: "{multi>Name1}"
			}).addStyleClass("multiText"),
		// new sap.m.Button({
		// 	type: "Transparent",
		// 	icon : "sap-icon://sys-cancel",
		// 	press: [oController.onClosePress, oController]
		// }),
		// new sap.m.Button({
			// visible: {
			// 	path: "orders>Bemot",
			// 	formatter: function (Bemot){
			// 		if (Bemot == "03")
			// 		{
			// 			return true;
			// 		}
			// 		else return false;
			// 	}

			// },
			// enable: {
			// 	path: "orders>/Materials",
			// 	formatter: function(materials)
			// 	{
			// 		console.log(materials);
			// 	}

			// },
		// 	type: "Transparent",
		// 	icon : "sap-icon://action",
		// 	press: [oController.onMaterialPress, oController]

		// }),
		]
	});
	this.orderList.bindAggregation("items", {
		path: "multi>/results",
		template: template
	});
	dialog.insertContent(this.orderList,0);
		// this.listForm = new sap.ui.layout.form.SimpleForm({
		// 	layout : "ResponsiveGridLayout",
		// 	labelSpanL : 12,
		// 	labelSpanM : 12,
		// 	labelSpanS : 12,
		// 	emptySpanL : 1,
		// 	emptySpanM : 1,
		// 	emptySpanS : 1,
		// 	columnsL : 2,
		// 	columnsM : 2,
		// 	editable : true,
		// 	content :
		// 	[
		// 		new sap.ui.core.Title({
		// 			text : "{i18n>ORDER_LIST}"
		// 		}),
		// 		this.orderList
		// 	]
		// })
		//
		//
		// dialog.insertContent(this.listForm, 0);

		return dialog;
	}
});
