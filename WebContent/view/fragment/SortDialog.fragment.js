sap.ui.jsfragment("view.fragment.SortDialog", {
	createContent: function (oController) {
		var dialog = new sap.m.Dialog({
			title: "{i18n>ORDERBY}",
			endButton: new sap.m.Button({
				text: "{i18n>OK}",
				press: [oController.onSortClose, oController]
			}),
			content: [
				new sap.m.HBox({
					items: [
						new sap.m.Select({
							selectedKey: "{sort>/sortBy}",
							items: [
								new sap.ui.core.Item({
									text: "{i18n>ORDER}",
									key: "Aufnr"
								}),
								new sap.ui.core.Item({
									text: "{i18n>DESCRIPTION}",
									key: "Ktext"
								}),
								new sap.ui.core.Item({
									text: "{i18n>DATE}",
									key: "Gstrp"
								}),
								new sap.ui.core.Item({
									text: "{i18n>CLIENT}",
									key: "Name1"
								}),
						]
						}),
						new sap.m.Select({
							selectedKey: "{sort>/sortType}",
							items: [
								new sap.ui.core.Item({
									text: "{i18n>INCREASING}",
									key: "asc"
								}),
								new sap.ui.core.Item({
									text: "{i18n>DECREASING}",
									key: "desc"
								}),
						]
						}),
					]
				}),
			]
		});

		dialog.addContent(this.list);

		return dialog;
	}
});
