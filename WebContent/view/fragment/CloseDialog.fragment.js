sap.ui.jsfragment("view.fragment.CloseDialog", {
	createContent : function(oController) {
		var dialog = new sap.m.Dialog({
			title: "{order>/Ktext}",
			beginButton: new sap.m.Button({
				text: "{i18n>OK}",
				press: [oController.closeConfirm, oController]
			}),
			endButton: new sap.m.Button({
				text: "{i18n>CANCEL}",
				press: [oController.closeCancel, oController]
			}),
			content:[
				this.form = new sap.ui.layout.form.SimpleForm({
					layout : "ResponsiveGridLayout",
					labelSpanL : 12,
					labelSpanM : 12,
					labelSpanS : 12,
					emptySpanL : 1,
					emptySpanM : 1,
					emptySpanS : 1,
					columnsL : 2,
					columnsM : 2,
					editable : true,
					content :
					[
						new sap.ui.core.Title({
							text : "{i18n>CLOSE_CONFIRM}"
						}),
						new sap.m.Label({
							text : "{i18n>ACTUAL_COUNTER}"
						}),
						new sap.m.Input({
							type: "Number",
							enabled: true,
							value: "{order>/actualrecdv}",
							valueState: "{order>/actualrecdv_state}",
							change:[oController.onCounterChange, oController]
						}),
                        new sap.m.Label({
                            text : "{i18n>CLOSE_DATE}"
                        }),
//                        new sap.m.DateTimeInput({
//                            valueFormat: "{enabledFG>/lang}",
//                            dateValue: {
//                                path : "order>/closeDate",
//                                formatter : util.Formatter.DateISOtoDate
//                            },
//                            change: [oController.onContDateChange, oController]
//                        }),
                        new sap.m.DatePicker({
                            displayFormat: "{enabledFG>/lang}",
//                            displayFormat: "dd-MM-yyyy", // Fabio 11/04/2016
                            valueFormat: "{enabledFG>/lang}",
//                            valueFormat: "dd-MM-yyyy", // Fabio 11/04/2016
                            valueState: "{order>/closeDate_state}",
                            dateValue: {
                                path : "order>/closeDate",
                                formatter : util.Formatter.DateISOtoDate
                            },
                            change: [oController.onContDateChange, oController]
                        }),
						new sap.m.Label({
							text : "{i18n>LAST_READ_COUNTER}"
						}),
						new sap.m.Input({
							enabled: false,
							value: "{order>/Recdv}"
						}),
                        new sap.m.Label({
                            text : "{i18n>LAST_READ_DATE}"
                        }),
						new sap.m.Input({
							enabled: false,
							value: {
								path : "order>/Idate",
								formatter : util.Formatter.date
							}
						}),
						new sap.m.Label({
							text : "{i18n>LAST_READ_TIME}"
						}),
						new sap.m.Input({
							enabled: false,
							value: {
								path : "order>/Itime",
								formatter : util.Formatter.time
							}
						}),
					]
				}),
			]
		});

		dialog.addContent(this.list);

		return dialog;
	}
});
