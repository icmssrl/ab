sap.ui.jsfragment("view.fragment.FasiDialog", {
	createContent : function(oController) {
		var dialog = new sap.m.Dialog({
			title: "{order>/Ktext}",
			endButton: new sap.m.Button({
				text: "{i18n>OK}",
				press: [oController.onFasiClose, oController]
			}),
			beginButton: new sap.m.Button({
				icon: "sap-icon://multi-select",
				press: [oController.onSelectAll, oController]
			}),
			content:[
				this.list = new sap.m.List({
					items: {
						path: "fasi>/results",
						template: new sap.m.CustomListItem({
							content: [
								new sap.m.CheckBox({
									text: "{fasi>Ltxa1}",
									selected : {
									path: "fasi>Usr10",
										formatter: function(status) {
											if (status === "X")
											{
												return true;
											}
											else
											{
												return false;
											}
										}
									},
									select: [oController.onCheckFasi, oController]
								}),

							]
						})
					}
				}),
			]
		});

		dialog.addContent(this.list);

		return dialog;
	}
});