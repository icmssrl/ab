sap.ui.jsview("view.Home", {

	/** Specifies the Controller belonging to this View.
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf view.Home
	*/
	getControllerName : function() {
		return "view.Home";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	* Since the Controller is given to this method, its event handlers can be attached right away.
	* @memberOf view.Home
	*/
	createContent : function(oController) {
 		return new sap.m.Page({
			// title: "{user>/ESname}",
			enableScrolling: false,
			customHeader: new sap.m.Bar({
				contentMiddle: [
					new sap.m.Label({text: "{user>/ESname}"})
				],
				contentRight:
				[
					new sap.m.Label({text: " {synchro>/dateTime}", visible:"{synchro>/en}"}).addStyleClass("synchroLabel")
				]
			}),
			content: [
				new sap.m.TileContainer({
					tiles:
						[
							new sap.m.StandardTile({
								icon: "sap-icon://timesheet",
								title: "{i18n>TIMESHEET}",
								press: [oController.onTimesheetPress, oController]
							}),
							new sap.m.StandardTile({
								title: "{i18n>MATERIALS}",
								icon: "sap-icon://inventory",
								press: [oController.onMaterialsPress, oController]
							}),
						 ]
				}),
			],
			footer : new sap.m.Bar({
				contentMiddle : [
					new sap.m.Button({
						text : "{i18n>SYNCHRONIZE}",
						icon : "sap-icon://synchronize",
						press : [ oController.onSynchronize, oController ]
					}),
					// new sap.m.Button({
					// 	text : "{i18n>SETTINGS}",
					// 	icon : "sap-icon://action-settings",
					// 	press : [ oController.onSettings, oController ]
					// }),
					],
				contentRight: [
					new sap.m.Label({text: "{i18n>VERSION}"})
				]
			})

		});
	}

});
