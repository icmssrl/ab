jQuery.sap.require("model.User");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");

jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.Synchronize");

sap.ui.controller("view.timesheet.ReportsHeader", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.timesheet.Reports
	 */
	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

		// EVENT BUS
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);
	},

	_handleRouteMatched: function (evt) {

		var name = evt.getParameter("name");
		if ("timesheet.ReportsHeader" !== name) {
			return;
		}

		this.refreshModel();

	},

	refreshModel: function () {
		var tss = model.Timesheet.getModel().getData().results;

		var data = {};

		var today = new Date();
		var tm1 = new Date();
		tm1.setDate(tm1.getDate() - 1);
		var tm2 = new Date();
		tm2.setDate(tm2.getDate() - 2);
		var tm3 = new Date();
		tm3.setDate(tm3.getDate() - 3);
		var tm4 = new Date();
		tm4.setDate(tm4.getDate() - 4);
		var tm5 = new Date();
		tm5.setDate(tm5.getDate() - 5);
		var tm6 = new Date();
		tm6.setDate(tm6.getDate() - 6);
		var tm7 = new Date();
		tm7.setDate(tm7.getDate() - 7);
		var tm8 = new Date();
		tm8.setDate(tm8.getDate() - 8);


		data.items = [
			{
				Data: today,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm1,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm2,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm3,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm4,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm5,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm6,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm7,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
			{
				Data: tm8,
				workHours: 0,
				tripHours: 0,
				pause: 0,
				totKm: 0
			},
		];

		for (var j = 0; j < data.items.length; j++) {
			var day = data.items[j];

			if (!tss) break;

			for (var i = 0; i < tss.length; i++) {
				var ts = tss[i];
				// Se la data è uguale
				if ((new Date(ts.DateAu).getDate() === new Date(day.Data).getDate())&&
				(new Date(ts.DateAu).getMonth() === new Date(day.Data).getMonth())&&
				(new Date(ts.DateAu).getYear() === new Date(day.Data).getYear())&&(ts.Elab !== "D"))
				{
					// Ore di lavoro totali
					var wh = ts.Wrend.ms - ts.Wrbeg.ms - ts.Pause.ms
					day.workHours = day.workHours + wh;

					// Ore di viaggio totali
					var th = ts.Trpin.ms + ts.Trpout.ms;
					day.tripHours = day.tripHours + th;

					// Ore di pausa
					day.pause = day.pause + ts.Pause.ms;

					//Tot Km
					var tKm = (ts.Km =="")? 0 : parseFloat(ts.Km);
					day.totKm = day.totKm + tKm;
				}
			}
		}

		this.model = new sap.ui.model.json.JSONModel(data);
		this.view.setModel(this.model, "timesheet");

		this.synchroModel = new sap.ui.model.json.JSONModel({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.view.setModel(this.synchroModel, "synchro");
	},
	/**
	 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
	 * (NOT before the first rendering! onInit() is used for that one!).
	 * @memberOf view.timesheet.Reports
	 */
	//	onBeforeRendering: function() {
	//
	//	},

	/**
	 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * @memberOf view.timesheet.Reports
	 */
	//	onAfterRendering: function() {
	//
	//	},

	/**
	 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
	 * @memberOf view.timesheet.Reports
	 */
	//	onExit: function() {
	//
	//	}


	timesheetSuccess: function (oModel) {
		this.view.setModel(oModel, "timesheet");
	},

	testsuccess: function (data) {
		console.log(data);
	},

	testerror: function (data) {
		console.log(data);
	},

	onAddPress: function () {
		this.router.navTo("timesheet.NewReport");
	},

	onTimesheetList: function () {
		this.router.navTo("timesheet.List");
	},

	onListItemPress: function (evt) {
		var view = this.getView();
		var model = view.getModel("timesheet");
		var path = evt.getSource().getBindingContext("timesheet").getPath();
		var item = model.getProperty(path);

		this.router.navTo("timesheet.Reports", {
			day: item.Data.toString(),
		});
	},

	onHomePress: function () {
		this.router.navTo("home");
	},

	onSynchronize: function () {
		util.Synchronize.begin();
	},

	onSort: function () {
		this.sortDialog = sap.ui.jsfragment("view.fragment.SortTSDialog", this);
		this.getView().addDependent(this.sortDialog);
		this.sortModel = new sap.ui.model.json.JSONModel({
			sortBy: "Aufnr",
			sortType: "asc"
		});
		this.sortDialog.setModel(this.sortModel, "sort");
		this.sortDialog.open();
	},

	onSortClose: function () {
		var by = this.sortModel.getData().sortBy;
		var type = this.sortModel.getData().sortType;
		var basc = true;
		if (type === "asc") basc = false;

		var sorter = new sap.ui.model.Sorter(by, basc);
		if (by === "Gstrp") {
			sorter.fnCompare = function (a, b) {
				return new Date(a) - new Date(b);
			}
		}

		this.getView().list.getBinding("items").sort([sorter]);
		this.sortDialog.close();
	},

	onSynchronizeComplete: function () {
		this.view.setModel(model.User.getModel(), "user");
		this.view.setModel(model.Timesheet.getModel(), "timesheet");
		model.Timesheet.getModel().refresh();
		// model.Synchronize.setDateTime();
		this.refreshModel();
	},

	onAddPress: function () {
		this.router.navTo("timesheet.NewReport");
	},

	onTimesheetList: function () {
		this.router.navTo("timesheet.List");
	},

	onSynchronize: function () {
		util.Synchronize.begin();
	},

});
