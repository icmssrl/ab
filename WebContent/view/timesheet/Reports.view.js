jQuery.sap.require("util.Formatter");

sap.ui.jsview("view.timesheet.Reports", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	getControllerName: function () {
		return "view.timesheet.Reports";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({
			//			title : "{user>/ESname}",
			customHeader: new sap.m.Bar({
				contentMiddle: [
					new sap.m.Label({
						text: {
							path: "page>/",
							formatter: util.Formatter.date
						}
					})
				],
				contentLeft: [new sap.m.Button({
					icon: "sap-icon://home",
					press: [oController.onHomePress, oController]
				}), new sap.m.Button({
					icon: "sap-icon://nav-back",
					press: [oController.onNavBackPress, oController]
				})],
//                , new sap.m.Button({
//					icon: "sap-icon://alphabetical-order",
//					press: [oController.onSort, oController]
//				})],
				contentRight: [
					new sap.m.Label({text: " {synchro>/dateTime}", visible:"{synchro>/en}"}).addStyleClass("synchroLabel"),
					new sap.m.Button({
						icon: "sap-icon://synchronize",
						press: [oController.onSynchronize, oController]
					})
				]
				//				contentMiddle: [new sap.m.Label({
				//					text: "{user>/ESname}"
				//				})],
				//				contentRight: [
				//					new sap.m.Button({
				//						icon: "sap-icon://synchronize",
				//						press: [oController.onSynchronize, oController]
				//					}),
				//				]
			}),
						footer: new sap.m.Bar({
							contentMiddle: [
							// new sap.m.Button({
							// 		text: "{i18n>TIMESHEET_LIST}",
							// 		icon: "sap-icon://time-entry-request",
							// 		press: [oController.onTimesheetList, oController]
							// 	}),
							new sap.m.Button({
									text: "{i18n>NEW}",
									icon: "sap-icon://add",
									press: [oController.onAddPress, oController]
								}),
							]
						})
		});

		this.list = new sap.m.Table("reportTable", {
			inset: false,
			headerDesign: "Plain",
			columns: [
				new sap.m.Column({
					hAlign: "Center",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>ORDER}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
					hAlign: "Center",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>CLIENT}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
					hAlign: "Center",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>ORA_INIZIO}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
					hAlign: "Center",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>ORA_FINE}",
						design: sap.m.LabelDesign.Bold
					})
				}), new sap.m.Column({
					hAlign: "Center",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
//					popinDisplay: "Inline",
				}), ],
		});

		this.template = new sap.m.ColumnListItem({
			// type : sap.m.ListType.Active,
			type: {
				path: "timesheet>Elab",
				formatter: util.Formatter.reportListItemType
			},
			press: [oController.onListItemPress, oController],
			cells: [
				new sap.m.Text({
					text: {
						path: "timesheet>Aufnr",
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>ODS/Name1",
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>Wrbeg/ms",
						formatter: util.Formatter.msToHHMM
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>Wrend/ms",
						formatter: util.Formatter.msToHHMM
					}
				}),
				new sap.m.Button({
					icon: "sap-icon://delete",
					type: "Transparent",
					enabled: {
						path: "timesheet>Elab",
						formatter: function (elab) {
							if (elab === true) return false;
							return true;
						}
					},
					press: [oController.onDeletePress, oController]
				}),
				]
		});

		this.list.bindAggregation("items", {
			path: "timesheet>/results",
			template: this.template,
			sorter: new sap.ui.model.Sorter("DateAu")
		});
		this.page.addContent(this.list);

		return this.page;
	}

});
