jQuery.sap.require("util.Formatter");

sap.ui.jsview("view.timesheet.ReportsHeader", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	getControllerName: function () {
		return "view.timesheet.ReportsHeader";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({
			//			title : "{user>/ESname}",
			customHeader: new sap.m.Bar({
				contentLeft: [new sap.m.Button({
					icon: "sap-icon://home",
					press: [oController.onHomePress, oController]
				})],
				contentRight: [
					new sap.m.Label({text: "{synchro>/dateTime}", visible:"{synchro>/en}"}).addStyleClass("synchroLabel"),
					new sap.m.Button({
						icon: "sap-icon://synchronize",
						press: [oController.onSynchronize, oController]
					})
				]
			}),
			footer: new sap.m.Bar({
				contentMiddle: [
				new sap.m.Button({
						text: "{i18n>TIMESHEET_LIST}",
						icon: "sap-icon://time-entry-request",
						press: [oController.onTimesheetList, oController]
					}),
				 new sap.m.Button({
						text: "{i18n>NEW}",
						icon: "sap-icon://add",
						press: [oController.onAddPress, oController]
					}),
				],
			})
		});

		this.list = new sap.m.Table({
			inset: false,
			headerDesign: "Plain",
			columns: [new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				header: new sap.m.Label({
					text: "{i18n>DATE}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>TOT_ORE_LAVORATE}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>TOT_ORE_VIAGGIO}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>TOT_KM}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign: "Center",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>ORE_PAUSA}",
					design: sap.m.LabelDesign.Bold
				})
			}), ],
		});

		this.template = new sap.m.ColumnListItem({
			// type : sap.m.ListType.Active,
			type: sap.m.ListType.Active,
			//			},
			press: [oController.onListItemPress, oController],
			cells: [
				new sap.m.Text({
					text: {
						path: "timesheet>Data",
						formatter: util.Formatter.date,
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>workHours",
						formatter: util.Formatter.msToHHMM
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>tripHours",
						formatter: util.Formatter.msToHHMM
					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>totKm",

					}
				}),
				new sap.m.Text({
					text: {
						path: "timesheet>pause",
						formatter: util.Formatter.msToHHMM
					}
				}),
			]
		});

		this.list.bindAggregation("items", {
			path: "timesheet>/items",
			template: this.template,
			sorter: new sap.ui.model.Sorter("DateAu")
		});
		this.page.addContent(this.list);

		return this.page;
	}

});
