jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");

sap.ui.controller("view.timesheet.List", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.timesheet.Reports
	 */
	onInit: function() {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);
	},

	_handleRouteMatched: function(evt) {
		var name = evt.getParameter("name");
		if ("timesheet.List" !== name) {
			return;
		}

		this.view.setModel(model.User.getModel(), "user");
		this.view.setModel(model.TimesheetList.getModel(), "timesheet");
		model.Timesheet.getModel().refresh();
	},

	onNavBack: function() {
		this.router.myNavBack();
	}
	
});
