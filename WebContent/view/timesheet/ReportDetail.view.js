sap.ui.jsview("view.timesheet.ReportDetail", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 *
	 * @memberOf view.timesheet.ReportDetail
	 */
	getControllerName: function () {
		return "view.timesheet.ReportDetail";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 *
	 * @memberOf view.timesheet.ReportDetail
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({
			title: "{i18n>TIMESHEET_DETAIL}",
			showNavButton: true,
			navButtonPress: [oController.onNavButtonPress, oController],
			footer: new sap.m.Bar({
				contentMiddle: [
//					new sap.m.Button({
//						text: "{i18n>DELETE}",
//						icon: "sap-icon://delete",
//						enabled: "{enabled>/delete}",
//						press: [oController.onDeletePress, oController]
//					}),
					new sap.m.Button({
						text: "{i18n>SAVE}",
						icon: "sap-icon://save",
						enabled: "{enabled>/save}",
						press: [oController.onSavePress, oController]
					}),
				]
			})
		});

		this.form = new sap.ui.layout.form.SimpleForm({
			layout: "ResponsiveGridLayout",
			labelSpanL: 4,
			labelSpanM: 4,
			labelSpanS: 4,
			emptySpanL: 1,
			emptySpanM: 1,
			emptySpanS: 1,
			columnsL: 2,
			columnsM: 2,
			editable: true,
			content: [new sap.ui.core.Title({
					text: ""
				}), new sap.m.Label({
					text: "{i18n>ORDER}",
					enabled: "{enabled>/create}",
				}), this.order = new sap.m.Input({
					showValueHelp: true,
					valueHelpOnly: true,
					valueHelpRequest: [oController.onOrdersHelp, oController],
					value: "{/Aufnr}",
					liveChange: [oController.orderChange, oController],
					enabled: "{enabled>/create}",
				}), new sap.m.Label({
					text: "{i18n>CLIENT}"
				}), new sap.m.Input({
					value: "{/ODS/Name1}",
					enabled: false,
				}), new sap.m.Label({
					text: "{i18n>DATE}"
				}), this.DateAu = new sap.m.DateTimeInput({
					enabled: "{enabled>/create}",
//					valueFormat: "dd-MM-yyyy",
					valueFormat: "{enabled>/lang}",
					value: {
						path: "/DateAu",
						formatter: util.Formatter.date,
					},
					change: [oController.dateauChange, oController],
				}), new sap.m.Label({
					text: "{i18n>TESTO}"
				}), new sap.m.Input({
					value: "{/Testo}",
					enabled: "{enabled>/edit}",
					maxLength: 200,
				}), new sap.m.Label({
					text: "{i18n>ORE_ANDATA}"
				}),
				this.Trpin = new sap.m.Select({
					change: [oController.trpinChange, oController],
					enabled: "{enabled>/edit}",
					selectedKey: {
						path: "/Trpin",
						formatter: util.Formatter.time,
					},
					items: {
						path: "time>/items",
						template: new sap.ui.core.Item({
							text: "{time>value}",
							key: "{time>value}"
						}),
					},
				}),
				new sap.m.Label({
					text: "{i18n>ORE_RITORNO}"
				}),
				this.Trpout = new sap.m.Select({
					change: [oController.trpoutChange, oController],
					enabled: "{enabled>/edit}",
					selectedKey: {
						path: "/Trpout",
						formatter: util.Formatter.time,
					},
					items: {
						path: "time>/items",
						template: new sap.ui.core.Item({
							text: "{time>value}",
							key: "{time>value}"
						}),
					},
				}),
				new sap.m.Label({
					text: "{i18n>ORA_INIZIO}"
				}),
				this.Wrbeg = new sap.m.Select({
					change: [oController.wrbegChange, oController],
					enabled: "{enabled>/create}",
					selectedKey: {
						path: "/Wrbeg",
						formatter: util.Formatter.time,
					},
					items: {
						path: "time>/items",
						template: new sap.ui.core.Item({
							text: "{time>value}",
							key: "{time>value}"
						}),
					},
				}),
				// this.Wrbeg = new sap.m.DateTimeInput({
				// 	type : "Time",
				// 	value : {
				// 		path : "/Wrbeg",
				// 		formatter : util.Formatter.time,
				// 	},
				// 	enabled: "{enabled>/edit}",
				// 	change: [oController.wrbegChange, oController]
				// }),
				new sap.m.Label({
					text: "{i18n>ORA_FINE}"
				}),
				this.Wrend = new sap.m.Select({
					change: [oController.wrendChange, oController],
					enabled: "{enabled>/edit}",
					selectedKey: {
						path: "/Wrend",
						formatter: util.Formatter.time,
					},
					items: {
						path: "time>/items",
						template: new sap.ui.core.Item({
							text: "{time>value}",
							key: "{time>value}"
						}),
					},
				}),
				// this.Wrend = new sap.m.DateTimeInput({
				// 	type : "Time",
				// 	value : {
				// 		path : "/Wrend",
				// 		formatter : util.Formatter.time,
				// 	},
				// 	enabled: "{enabled>/edit}",
				// 	change: [oController.wrendChange, oController]
				// }),
				new sap.m.Label({
					text: "{i18n>ORE_PAUSA}"
				}),
				new sap.m.Select({
					change: [oController.pauseChange, oController],
					enabled: "{enabled>/edit}",
					selectedKey: {
						path: "/Pause",
						formatter: util.Formatter.time,
					},
					items: {
						path: "time>/items",
						template: new sap.ui.core.Item({
							text: "{time>value}",
							key: "{time>value}"
						}),
					},
				}),
				new sap.m.Label({
					type: "Number",
					text: "{i18n>KM}"
				}),
				new sap.m.Input({
					value: "{/Km}",
					enabled: "{enabled>/edit}",
					liveChange: [oController.onKmChange, oController]
				}), new sap.m.Label({
					text: "{i18n>TARGA}"
				}), this.targa = new sap.m.Input({
					value: {
						parts: [{
							path: "user>/ETarga"
						}, {
							path: "/Targa"
						}, ],
						formatter: function (userTarga, targa) {
							if (targa === "" || targa === null || targa === undefined) {
								return userTarga;
							} else {
								return targa;
							}
						}
					},
					enabled: "{enabled>/edit}",
				}),
				new sap.m.Label({
					text: "{i18n>TRANSFER}"
				}),
				this.Transfer = new sap.m.Select({
					change: [oController.onTransferChange, oController],
					enabled: "{enabled>/edit}",
					selectedKey: "{/Transfer}",
					items: [
						new sap.ui.core.Item({
							text: "",
							key: ""
						}),
						new sap.ui.core.Item({
							text: "{i18n>NATIONAL}",
							key: "N"
						}),
						new sap.ui.core.Item({
							text: "{i18n>FOREIGN}",
							key: "E"
						}),
					]
				}),

				this.foreign = new sap.m.Select({
					selectedKey: "{/Land1}",
					enabled: {
						path: "/Transfer",
						formatter: function(transfer)
						{
							if (transfer === "E") return true;
							return false;
						}
					},
					items: {
						path: "countries>/results",
						template: new sap.ui.core.Item({
							text: "{countries>Landx}",
							key: "{countries>Land1}"
						}),
					},
				}),

				new sap.m.Label({}),
				this.DirChiamata = new sap.m.CheckBox({
					text: "{i18n>DIRCHIAMATA}",
					selected: "{/DirChiamata}",
					select: [oController.onDirChiamataChange, oController],
					enabled: "{enabled>/edit}",
				}),
			]
		});

		this.page.addContent(this.form);
		return this.page;
	}

});
