jQuery.sap.require("model.User");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");

jQuery.sap.require("model.Fiori");

sap.ui.controller("view.timesheet.Reports", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.timesheet.Reports
	 */
	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

		// EVENT BUS
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);
	},

	_handleRouteMatched: function (evt) {

		var name = evt.getParameter("name");
		if ("timesheet.Reports" !== name) {
			return;
		}

		this.day = new Date(evt.getParameter("arguments").day);
		var mPage = new sap.ui.model.json.JSONModel(this.day);

		this.view.setModel(mPage, "page");
		this.view.setModel(model.User.getModel(), "user");
		this.view.setModel(model.Timesheet.getModel(), "timesheet");
		model.Timesheet.getModel().refresh();

		// -- Filtro in base al giorno
		if (this.day) {
			// var that = this;
			//
			// // -- Funzione di comparazione
			// var fFilter = function(e)
			// {
			// 	var item = new Date(e);
			// 	var selected = new Date(that.day);
			// 	if ((item.getDate() === selected.getDate())&&(item.getMonth() === selected.getMonth())&&(item.getYear() === selected.getYear()))
			// 	{
			// 		return true;
			// 	}
			// 	else
			// 	{
			// 		return false;
			// 	}
			// };
			// // --
			//
			// var fDay = new sap.ui.model.Filter("DateAu");
			// fDay.fnFilter = fFilter;
			console.log(this.day);
			var stringDate = util.Formatter.formatDateToSapFormat(new Date(this.day));
			// fromDate = util.Formatter.format
			// fromDate.setHours(0);
			// fromDate.setMinutes(0);
			// fromDate.setSeconds(0);
			// var toDate = new Date(this.day);
			// toDate.setHours(23);
			// toDate.setMinutes(59);
			// toDate.setSeconds(59);
			var fDay = new sap.ui.model.Filter("DateAu", sap.ui.model.FilterOperator.Contains, stringDate);
			this.getView().list.getBinding("items").filter([fDay]);

		}
		// --

		var sorter = new sap.ui.model.Sorter("DateAu", false);
		sorter.fnCompare = function (a, b) {
			return new Date(a) - new Date(b);
		};

		var sTime = new sap.ui.model.Sorter("", false);
		sTime.fnCompare = function (a, b) {
			var date = new Date(a.DateAu) - new Date(b.DateAu);
			var time = a.Wrbeg.ms - b.Wrbeg.ms;
			if (date === 0) return time;
			return date;
		}
		this.getView().list.getBinding("items").sort([sTime]);

		this.synchroModel = new sap.ui.model.json.JSONModel({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.view.setModel(this.synchroModel, "synchro");

	},
	/**
	 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
	 * (NOT before the first rendering! onInit() is used for that one!).
	 * @memberOf view.timesheet.Reports
	 */
	//	onBeforeRendering: function() {
	//
	//	},

	/**
	 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * @memberOf view.timesheet.Reports
	 */
	//	onAfterRendering: function() {
	//
	//	},

	/**
	 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
	 * @memberOf view.timesheet.Reports
	 */
	//	onExit: function() {
	//
	//	}


	timesheetSuccess: function (oModel) {
		this.view.setModel(oModel, "timesheet");
	},

	testsuccess: function (data) {
		console.log(data);
	},

	testerror: function (data) {
		console.log(data);
	},

	// onAddPress: function () {
	// 	this.router.navTo("timesheet.NewReport");
	// },

	onTimesheetList: function () {
		this.router.navTo("timesheet.List");
	},

	onListItemPress: function (evt) {
		var view = this.getView();
		var model = view.getModel("timesheet");
		var path = evt.getSource().getBindingContext("timesheet").getPath();
		var item = model.getProperty(path);

		this.router.navTo("timesheet.ReportDetail", {
			query: {
				Pernr: item.Pernr,
				Aufnr: item.Aufnr,
				DateAu: item.DateAu,
				Wrbeg: item.Wrbeg.ms
			}
		});
	},

	onHomePress: function () {
		this.router.navTo("home");
	},

	onSynchronize: function () {
		util.Synchronize.begin();
	},

	onSort: function () {
		this.sortDialog = sap.ui.jsfragment("view.fragment.SortTSDialog", this);
		this.getView().addDependent(this.sortDialog);
		this.sortModel = new sap.ui.model.json.JSONModel({
			sortBy: "Aufnr",
			sortType: "asc"
		});
		this.sortDialog.setModel(this.sortModel, "sort");
		this.sortDialog.open();
	},

	onSortClose: function () {
		var by = this.sortModel.getData().sortBy;
		var type = this.sortModel.getData().sortType;
		var basc = true;
		if (type === "asc") basc = false;

		var sorter = new sap.ui.model.Sorter(by, basc);
		if (by === "Gstrp") {
			sorter.fnCompare = function (a, b) {
				return new Date(a) - new Date(b);
			}
		}

		this.getView().list.getBinding("items").sort([sorter]);
		this.sortDialog.close();
	},

	onSynchronizeComplete: function () {
		this.view.setModel(model.User.getModel(), "user");
		this.view.setModel(model.Timesheet.getModel(), "timesheet");
			if (this.day) {
				var stringDate = util.Formatter.formatDateToSapFormat(new Date(this.day));
		var fDay = new sap.ui.model.Filter("DateAu", sap.ui.model.FilterOperator.Contains, stringDate);
		this.getView().list.getBinding("items").filter([fDay]);
	}
		model.Timesheet.getModel().refresh();
		this.synchroModel.setData({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.synchroModel.refresh();
	},

	onNavBackPress: function () {
		this.router.myNavBack();
	},

	onDeletePress: function (evt) {
		var path = evt.getSource().getBindingContext("timesheet").getPath();

		var onClose = function (path, oAction) {
			if (oAction === sap.m.MessageBox.Action.OK) {
				this.getView().getModel("timesheet").setProperty(path + "/Elab", "D");
				this.getView().getModel("timesheet").refresh();


				// this.router.navTo("timesheet.Reports", {"day":this.day});
			}
		}
		onClose = $.proxy(onClose, this, path);

		sap.m.MessageBox.confirm(this.getView().getModel("i18n").getProperty("DELETE_TIMESHEET_CONFIRMATION"), {
			title: this.getView().getModel("i18n").getProperty("CONFIRM"), // default
			onClose: onClose // default
		});
	},

	//------New Report----
	onAddPress: function () {

		// var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern : "dd-MM-yyyy"});
		// var dateString = dateFormat.format(this.day);
		this.router.navTo("timesheet.NewReport", {
			"day": util.Formatter.formatDateToUrl(this.day)
		});
	},

});
