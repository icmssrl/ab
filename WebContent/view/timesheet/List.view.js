jQuery.sap.require("util.Formatter");

sap.ui.jsview("view.timesheet.List", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	getControllerName : function() {
		return "view.timesheet.List";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 *
	 * @memberOf view.timesheet.Reports
	 */
	createContent : function(oController) {
		this.page = new sap.m.Page({
			customHeader: new sap.m.Bar({
				contentLeft: [ new sap.m.Button({
					icon: "sap-icon://nav-back",
					press: [ oController.onNavBack, oController ]
				})],
				contentMiddle: [ new sap.m.Label({text: "{user>/ESname}"})]
			}),
		});

		this.list = new sap.m.Table({
			inset: false,
			headerDesign : "Plain",
			mode: "None",
			columns : [ new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>DATE}",
					design : sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>HOURS}",
					design : sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>HOURS_TRAVEL}",
					design : sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>HOURS_VACATION}",
					design : sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>TOT_KM}",
					design : sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign : "Center",
				vAlign : "Middle",
				header : new sap.m.Label({
					text : "{i18n>TRANSFER}",
					design : sap.m.LabelDesign.Bold
				})
			}), ]
		});

		var template = new sap.m.ColumnListItem({
			cells : [
				new sap.m.Text({
				text : {
					path: "timesheet>Workdate",
					formatter: util.Formatter.date
				},
			}), new sap.m.Text({
				text : "{timesheet>Ore}"
			}), new sap.m.Text({
				text : "{timesheet>Viaggi}"
			}), new sap.m.Text({
				text : "{timesheet>Ferie}"
			}),
			new sap.m.Text({
				text :
				{
					path:"timesheet>Km",
					formatter : util.Formatter.formatKm
				}
			}),
			new sap.m.Text({
				text: {
					path: "timesheet>Trasf",
					formatter: util.Formatter.transfer
				},
			}), ]
		});

		this.list.bindAggregation("items", {
			path : "timesheet>/results",
			template : template,
		});
		this.page.addContent(this.list);

		return this.page;
	}

});
