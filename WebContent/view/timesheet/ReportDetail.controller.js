jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.User");

jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Common");

jQuery.sap.require("sap.m.MessageBox");


sap.ui.controller("view.timesheet.ReportDetail", {

    /**
     * Called when a controller is instantiated and its View controls (if available) are already created.
     * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
     * @memberOf view.timesheet.ReportDetail
     */
    onInit: function () {
        this.view = this.getView();
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

        this.iModel = model.i18n.getModel();

        util.Common.setFunctionsContext(this);
    },

    _handleRouteMatched: function (evt) {

        var name = evt.getParameter("name");
        this.name = name;

        if ("timesheet.ReportDetail" !== name && "timesheet.NewReport" !== name) {
            return;
        }

        this.view.setModel(model.User.getModel(), "user");
        this.view.setModel(model.Time.getModel(), "time");
        this.view.setModel(model.Countries.getModel(), "countries");
        
        var lang = sap.ui.getCore().getConfiguration().getLanguage();
        if(lang !== "it" && lang !== "it-IT") {
            var patternData = "MM-dd-yyyy";
        } else {
            var patternData = "dd-MM-yyyy";
        }

        var enabled = {
            edit: true,
            create: false,
            delete: true,
            save: true,
            lang: patternData
        };

        if ("timesheet.ReportDetail" == name) {
            console.log(evt.getParameter("arguments"));
            data = evt.getParameter("arguments")["?query"];
            this.view.setModel(model.Timesheet.getDetailModel(data.Pernr, data.Aufnr, data.DateAu, data.Wrbeg));
            console.log(this.view.getModel());

            if (this.view.getModel().getProperty("/Elab") === true) {
                enabled.edit = false;
                enabled.create = false;
                enabled.delete = false;
                enabled.save = false;
            }
        }

        if ("timesheet.NewReport" == name) {

            enabled.create = true;
            enabled.create = true;
            enabled.delete = false;

            // modified to add Report with setted Date

            this.day = util.Formatter.formatUrlToDate(evt.getParameters().arguments.day);
            this.getView().setModel(model.Timesheet.getDetailEmptyModel(this.day));
            this.view.order.setEnabled(true);
            this.view.setModel(model.ODST.getModel(), "orders");
        }

        var enabledModel = new sap.ui.model.json.JSONModel(enabled);

        this.getView().setModel(enabledModel, "enabled");
    },

    onNavButtonPress: function () {

        if (this.trpinChangeClone) {
            this.getView().getModel().setProperty("/Trpin/ms", this.trpinChangeClone);
            this.trpinChangeClone = undefined;
        }
        if (this.trpoutChangeClone) {
            this.getView().getModel().setProperty("/Trpout/ms", this.trpoutChangeClone);
            this.trpoutChangeClone = undefined;
        }
        if (this.wrbegChangeClone) {
            this.getView().getModel().setProperty("/Wrbeg/ms", this.wrbegChangeClone);
            this.wrbegChangeClone = undefined;
        }
        if (this.wrendChangeClone) {
            this.getView().getModel().setProperty("/Wrend/ms", this.wrendChangeClone);
            this.wrendChangeClone = undefined;
        }
        if (this.pauseChangeClone) {
            this.getView().getModel().setProperty("/Pause/ms", this.pauseChangeClone);
            this.pauseChangeClone = undefined;
        }

        this.router.myNavBack();

    },

    onOrdersHelp: function () {
        if (!this._ordersDialog) {
            this._ordersDialog = sap.ui.jsfragment("view.fragment.OrdersDialog", this);
            this.getView().addDependent(this._ordersDialog);
        }

        this._ordersDialog.setModel(this.getView().getModel("orders"), "orders");
        this._ordersDialog.setModel(this.getView().getModel("i18n"), "i18n");
        this._ordersDialog.open();
    },

    onOrdersConfirm: function (evt) {
        var view = this.getView();
        var model = view.getModel("orders");
        var path = evt.getParameters().selectedContexts[0].sPath;
        var item = model.getProperty(path);

        console.log(item);

        view.getModel().setProperty("/Aufnr", item.Aufnr);
        view.getModel().setProperty("/ODS/Name1", item.Name1);
    },

    onOrdersSearch: function (evt) {
        var val = evt.getParameter("value");
        var aFilters = [];
        aFilters.push(new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.Contains, val));
        aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.Contains, val));
        aFilters.push(new sap.ui.model.Filter("Kunum", sap.ui.model.FilterOperator.Contains, val));
        aFilters.push(new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, val));

        var oFilter = new sap.ui.model.Filter({
            filters: aFilters
        });

        evt.getSource().getBinding("items").filter([oFilter]);
    },

    onSavePress: function () {

        var timesheetDate = this.view.DateAu.getDateValue();

        if (!this.checkDate(timesheetDate))
            return;

        var valid = true;
        var message = "";
        //Input Validation
        //Valido l'orari di partenza/arrivo
        var beg = util.Formatter.timeToMs(this.getView().Wrbeg.getSelectedKey());

        var end = util.Formatter.timeToMs(this.getView().Wrend.getSelectedKey());
        //		if (end === 0) {
        //			message = message + this.iModel.getProperty("ORA_FINE") + ";";
        //			valid = false;
        //		}

        if (beg > end) {
            valid = false;
        }

        var today = new Date();
        today.setDate(today.getDate() - 10);
        if (this.view.DateAu.getEnabled() === true && this.view.getModel().getProperty("/DateAu") < today) {
            this.view.DateAu.setValueState("Error");
            valid = false;
        } else {
            this.view.DateAu.setValueState("None");
        }

        if (this.view.order.getValue() === "" || this.view.order.getValue() === undefined) {
            this.view.order.setValueState("Error");
            valid = false;
        } else {
            this.view.order.setValueState("None");
        }

        if (this.view.Transfer.getSelectedKey() === "E" && !this.view.foreign.getSelectedKey()) {
            message = message + this.iModel.getProperty("TRANSFER") + ";";
            valid = false;
        }

        // Se i campi del form sono validi continuo con il salvataggio
        if (valid) {
            var item = this.view.getModel().oData;
            item.Targa = this.view.targa.getValue();

            if (this.view.Transfer.getSelectedKey() === "E") {
                item.Land1 = this.view.foreign.getSelectedKey();
            } else {
                item.Land1 = "";
            }

            item.Aufnr = util.Formatter.removeLeadingZero(item.Aufnr);
            //---------I moved this instructions before the condition, so i can check even a modified existing report//-------------
            var user = model.User.getModel().getData();
            var ts = model.Timesheet.getDetailModel(user.EPernr, item.Aufnr, item.DateAu, item.Wrbeg.ms);
            //---------------------------------------------------------------------------
            if (this.name == "timesheet.NewReport") {


                // Cerco se ho già un timesheet con questi dati e in caso blocco

                if (ts) {
                    sap.m.MessageBox.alert(this.iModel.getProperty("DUPLICATE_TIMESHEET"));
                    return;
                }
            }

            // Controllo se gli orari possono essere compatibili con il resto del timesheet
            var durataAndata = util.Formatter.timeToMs(this.getView().Trpin.getSelectedKey());
            var durataRitorno = util.Formatter.timeToMs(this.getView().Trpout.getSelectedKey());
            var oraAndata = beg - durataAndata;
            var oraRitorno = durataRitorno + end;

            //---modified to correct modification Operations ---
            var data = new Date(this.view.getModel().getProperty("/DateAu"));

            var tss = model.Timesheet.getModel().getData();

            var errorText = false;

            // Ore Lavorate totali
            var oraRitornoNT = new Date(oraRitorno);
            oraRitornoNT.setMinutes(oraRitornoNT.getMinutes() + oraRitornoNT.getTimezoneOffset());

            var oraAndataNT = new Date(oraAndata);
            oraAndataNT.setMinutes(oraAndataNT.getMinutes() + oraAndataNT.getTimezoneOffset());

            var eTotOreLavorate = oraRitornoNT.getTime() - oraAndataNT.getTime();
            // ----

            for (var i = 0; i < tss.results.length; i++) {
                var elem = tss.results[i];

                if (((ts !== undefined) && (ts.getData() == elem)) || (elem.Elab == "D"))
                    continue;

                var eData = elem.DateAu;

                if ((new Date(eData).getDate() != data.getDate()) ||
                    (new Date(eData).getMonth() != data.getMonth()) ||
                    (new Date(eData).getYear() != data.getYear()))
                    continue;

                var eOraInizio = new Date(elem.Wrbeg.ms).getTime();
                var eOraFine = new Date(elem.Wrend.ms).getTime();
                var eDurataAndata = new Date(elem.Trpin.ms).getTime();
                var eDurataRitorno = new Date(elem.Trpout.ms).getTime();
                var eOraAndata = eOraInizio - eDurataAndata;
                var eOraRitorno = eOraFine + eDurataRitorno;
                //Incremento le ore lavorate totali
                // Ore Lavorate totali
                var eOraRitornoNT = new Date(eOraRitorno);
                eOraRitornoNT.setMinutes(eOraRitornoNT.getMinutes() + eOraRitornoNT.getTimezoneOffset());
                var eOraAndataNT = new Date(eOraAndata);
                eOraAndataNT.setMinutes(eOraAndataNT.getMinutes() + eOraAndataNT.getTimezoneOffset());


                var eTotOreLavorate = eTotOreLavorate + (eOraRitornoNT.getTime() - eOraAndataNT.getTime());
                if (eTotOreLavorate > 86400000) {
                    errorText = this.iModel.getProperty("WORK_HOURS_GT_24");
                }
                // ----

                // Controllo che la data di inzio non sia all'interno di un altro ordine
                if (beg >= eOraAndata && beg < eOraRitorno && !errorText) {
                    errorText = this.iModel.getProperty("INVALID_BEGIN_DATE");
                }
                if (end > eOraAndata && end <= eOraRitorno && !errorText) {
                    errorText = this.iModel.getProperty("INVALID_END_DATE");
                }
                // if (beg === eOraRitorno && oraAndata !== eOraFine)
                // {
                // 	errorText = this.iModel.getProperty("INVALID_ROUND_TRIP");
                // }
                // if (end === eOraAndata && oraRitorno !== eOraInizio)
                // {
                // 	errorText = this.iModel.getProperty("INVALID_RETURN_TRIP");
                // }

                // Il viaggio di ritorno o andata vengono segnati su un solo timesheet
                if (oraAndata >= eOraAndata && oraAndata < eOraRitorno && !errorText) {
                    errorText = this.iModel.getProperty("INVALID_ROUND_TRIP");
                }
                if (oraRitorno > eOraAndata && oraRitorno < eOraRitorno && !errorText) {
                    errorText = this.iModel.getProperty("INVALID_RETURN_TRIP");
                }
                if (oraAndata <= eOraAndata && oraRitorno >= eOraRitorno && !errorText) {
                    errorText = this.iModel.getProperty("INVALID_RETURN_TRIP");
                }
                //-----------------------
                if (errorText)
                    break;
            }


            // Controllo che il giorno di ritorno/andata non sia cambiato in modo da vedere se con il viaggio sono finito nel giorno precedente o in quello successivo
            var or = new Date(oraRitorno);
            or.setMinutes(or.getMinutes() + or.getTimezoneOffset());
            // if (or.getDate() !== 1 && or.getHours() !== 0 || (or.getHours() === 0 && or.getMinutes() === 30)) //Why?
            if (or.getDate() > 1 && (or.getHours() !== 0 || or.getMinutes() !== 0)) {
                errorText = this.iModel.getProperty("INVALID_RETURN_TRIP");
            }
            var oa = new Date(oraAndata);
            oa.setMinutes(oa.getMinutes() + oa.getTimezoneOffset());
            if (oa.getDate() !== 1) {
                errorText = this.iModel.getProperty("INVALID_ROUND_TRIP");
            }
            //--

            if (errorText) {
                sap.m.MessageBox.alert(this.iModel.getProperty("TIMESHEET_TIME_CONFLICT") + ": " + errorText);

                return;
            }

            var tmp = model.Timesheet.getModel().oData;

            if (this.name == "timesheet.NewReport") {
                var clone = jQuery.extend(true, {}, item);
                console.log(clone);
                clone.DateAu = util.Formatter.dateToSap(clone.DateAu).toISOString();
                // clone.Km = (clone.Km == "") ? 0: clone.Km;
                tmp.results.push(clone);
            }
            // else
            // {
            // 	item.Km = (item.Km == "")? 0:item.Km;
            // }
            // var clone = jQuery.extend(true, {}, item);
            // console.log(clone);
            // clone.DateAu = util.Formatter.dateSap(clone.DateAu).toISOString();
            // tmp.results.push(clone);
            model.Timesheet.getModel().setData(tmp);
            model.Timesheet.getModel().refresh();


            model.Timesheet.save();
            sap.m.MessageBox.alert(this.iModel.getProperty("SAVED"));
            model.Timesheet.getModel().refresh();
            
            // FABIO 25/02/2016
            this.trpinChangeClone = undefined;
            this.trpoutChangeClone = undefined;
            this.wrbegChangeClone = undefined;
            this.wrendChangeClone = undefined;
            this.pauseChangeClone = undefined;
            //
            
            this.router.myNavBack();

        } else {
            sap.m.MessageBox.alert(this.iModel.getProperty("FORM_INVALID") + ": " + message);
        }
    },




    orderChange: function (evt) {
        var source = evt.getSource();
    },

    //TIME CHANGING...
    trpinChange: function (evt) {
        if (this.name != "timesheet.NewReport" && !this.trpinChangeClone) {
            this.trpinChangeClone = _.clone(this.getView().getModel().getProperty("/Trpin/ms"));
        }
        this.getView().getModel().setProperty("/Trpin/ms", util.Formatter.timeToMs(evt.getParameter('selectedItem').getProperty("key")));
    },

    trpoutChange: function (evt) {
        if (this.name != "timesheet.NewReport" && !this.trpoutChangeClone) {
            this.trpoutChangeClone = _.clone(this.getView().getModel().getProperty("/Trpout/ms"));
        }
        this.getView().getModel().setProperty("/Trpout/ms", util.Formatter.timeToMs(evt.getParameter('selectedItem').getProperty("key")));
    },

    wrbegChange: function (evt) {
        if (this.name != "timesheet.NewReport" && !this.wrbegChangeClone) {
            this.wrbegChangeClone = _.clone(this.getView().getModel().getProperty("/Wrbeg/ms"));
        }
        this.getView().getModel().setProperty("/Wrbeg/ms", util.Formatter.timeToMs(evt.getParameter('selectedItem').getProperty("key")));
    },

    wrendChange: function (evt) {
        if (this.name != "timesheet.NewReport" && !this.wrendChangeClone) {
            this.wrendChangeClone = _.clone(this.getView().getModel().getProperty("/Wrend/ms"));
        }
        this.getView().getModel().setProperty("/Wrend/ms", util.Formatter.timeToMs(evt.getParameter('selectedItem').getProperty("key")));
    },

    pauseChange: function (evt) {
        if (this.name != "timesheet.NewReport" && !this.pauseChangeClone) {
            this.pauseChangeClone = _.clone(this.getView().getModel().getProperty("/Pause/ms"));
        }
        this.getView().getModel().setProperty("/Pause/ms", util.Formatter.timeToMs(evt.getParameter('selectedItem').getProperty("key")));
    },

    dateauChange: function (evt) {
        var src = evt.getSource();

        var newDateValue = evt.getParameter('newDateValue');

        this.checkDate(newDateValue);
        // if(newDateValue > nowDate)
        // {
        // 	sap.m.MessageBox.alert(this.iModel.getProperty("FUTURE_DATE"));
        // 	src.setValueState("Error");
        // 	this.view.DateAu.setDateValue("");
        // }
        // else {
        // 	src.setValueState("None");
        // 	this.view.DateAu.setDateValue(newDateValue);
        // }
        this.getView().getModel().setProperty("/DateAu", evt.getParameter('newDateValue'));
    },

    checkDate: function (date) {
        var nowDate = new Date();
        var next30days = new Date();
        var previous7days = new Date();
        previous7days.setDate(previous7days.getDate() - 9);
        next30days.setDate(next30days.getDate() + 50);

        if (date > next30days) {
            sap.m.MessageBox.alert(this.iModel.getProperty("FUTURE_DATE"));
            this.view.DateAu.setValueState("Error");
            this.view.DateAu.setDateValue("");
            return false;
        } else {

            if (date < previous7days) {
                sap.m.MessageBox.alert(this.iModel.getProperty("OUT_OF_RANGE_DATE"));
                this.view.DateAu.setValueState("Error");
                this.view.DateAu.setDateValue("");
                return false;
            }
            this.view.DateAu.setValueState("None");
            this.view.DateAu.setDateValue(date);
            return true;
        }

    },

    onTransferChange: function (evt) {
        var s = this.getView().Transfer.getSelectedKey();
        this.getView().getModel().setProperty("/Transfer", s);
        // console.log(this.getView().getModel().getProperty("/Transfer"));
    },
    onKmChange: function (evt) {
        var val = evt.getParameter("newValue");
        this.getView().getModel().setProperty("/Km", val);
    },

    onDirChiamataChange: function (evt) {
        var s = this.getView().DirChiamata.getSelected();
        this.getView().getModel().setProperty("/DirChiamata", s);
        console.log(this.getView().getModel().getProperty("/DirChiamata"));
    },

    onDeletePress: function (evt) {
        var onClose = function (oAction) {
            if (oAction === sap.m.MessageBox.Action.OK) {
                this.getView().getModel().setProperty("/Elab", "D");
                this.router.navTo("timesheet.Reports");
            }
        }
        onClose = $.proxy(onClose, this);

        sap.m.MessageBox.confirm(this.iModel.getProperty("DELETE_TIMESHEET_CONFIRMATION"), {
            title: this.iModel.getProperty("CONFIRM"), // default
            onClose: onClose // default
        });
    }

});
