sap.ui.controller("view.Login", {

	onInit: function () {
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);
		util.Common.setFunctionsContext(this);
	},

	_handleRouteMatched: function (evt) {
		var name = evt.getParameter("name");
		if ("login" !== name) {
			return;
		}
		setTimeout(function(){
	
			$('input').blur();
		}, 300);


		this.getView().setModel(model.Credential.getModel(), "cred");
	},

	onSavePress: function (evt) {

		util.Busy.show();

		if (util.Validator.required(this.getView().userInput) && util.Validator.required(this.getView().passwordInput)) {
			var username = this.getView().userInput.getValue();
			var password = this.getView().passwordInput.getValue();

			model.Credential.getModel().setProperty("/username", username);
			model.Credential.getModel().setProperty("/password", password);

			model.Credential.saveModel();

			console.log(model.Credential.getUsername());
			console.log(model.Credential.getPassword());

			console.log(username);
			console.log(password);

			// Build Auth String
			var tok = username + ':' + password;
			var hash = btoa(tok);
			var auth = "Basic " + hash;

			console.log(hash);
			console.log(auth);

			$.ajaxSetup({
				headers: {
					"Authorization": auth
				}
			});

			var success = function () {
				this.router.navTo("home");
			};
			success = $.proxy(success, this);

			var error = function (e) {
				console.error(e)
			};
			error = $.proxy(error, this);

			var url = setting.Core.serverUrl + "ZFIORI_SRV/$metadata"

			$.ajax({
				url: url,
				type: "GET", //or POST?
				// dataType: "jsonp",
				xhrFields: {
					withCredentials: true
				},
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", auth);
				},
				success: success,
				error: error
			});
		}

		util.Busy.hide();
	},

	onResetPress: function (evt) {
		// model.Credential.remove();
		// this.getView().setModel(model.Credential.getModel(), "cred");
		// model.ODS.remove();
		// model.Timesheet.remove();
		// model.User.remove();
		// model.User.logout();
	},

	errorHandler: function (err) {
		$.ajax({
			type: "GET",
			url: settings.Core.logoffUrl, //Clear SSO cookies: SAP Provided service to do that
		});
		var jsonErr = JSON.parse(err.response.body);
		jQuery.sap.require("sap.m.MessageBox");
		sap.m.MessageBox.alert("Error: " + jsonErr.error.message.value);
	},

	onNavButtonPress: function () {
		this.router.navTo("home");
	},

});
