jQuery.sap.require("model.User");
jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("util.Error");
jQuery.sap.require("util.Common");

jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.Home", {

	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 *
	 * @memberOf view.Home
	 */

	onInit : function() {
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

		util.Common.setFunctionsContext(this);

		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);
	},

	_handleRouteMatched : function(evt) {
		var name = evt.getParameter("name");
		if ("home" !== name) {
			return;
		}

		if (util.Synchronize.sync)
		{
			util.Synchronize.sync = false;
			util.Synchronize.begin();
		}

		this.getView().setModel(model.User.getModel(), "user");
		this.synchroModel = new sap.ui.model.json.JSONModel();
		this.synchroModel.setData({"dateTime":model.Synchronize.getDateTime(), "en":model.Synchronize.isSynchronized()});
		this.getView().setModel(this.synchroModel, "synchro");
        
        setInterval(util.Synchronize.refreshConnection, 600000);
	},

	getModelSuccess : function(oModel) {
		console.log("Success");
		this.getView().setModel(oModel, "user");
	},

	onTimesheetPress: function()
	{
		this.router.navTo("timesheet.ReportsHeader");
	},

	onMaterialsPress: function()
	{
		// this.router.navTo("materials.ServiceOrders");
		this.router.navTo("materials.ClientOrders");
	},

	onSynchronize: function()
	{
		util.Synchronize.begin();
	},

	onSettings : function() {
		this.router.navTo("login");
	},

	onSynchronizeComplete : function()
	{
		this.synchroModel.setData({"dateTime":model.Synchronize.getDateTime(), "en":model.Synchronize.isSynchronized()});
		this.synchroModel.refresh();
	}

});
