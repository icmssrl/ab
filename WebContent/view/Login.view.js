sap.ui.jsview("view.Login", {

    /** Specifies the Controller belonging to this View.
     * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
     * @memberOf wrapper.Login
     */
    getControllerName: function() {
        return "view.Login";
    },

    /** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
     * Since the Controller is given to this method, its event handlers can be attached right away.
     * @memberOf wrapper.Login
     */
    createContent: function(oController) {

        this.page = new sap.m.Page({
            showNavButton : false,
            navButtonPress : [ oController.onNavButtonPress, oController ],
            title: "{i18n>SETTINGS}",
            content: [
                this.form = new sap.ui.layout.form.SimpleForm({
                layout : "ResponsiveGridLayout",
                labelSpanL : 4,
                labelSpanM : 4,
                labelSpanS : 4,
                emptySpanL : 1,
                emptySpanM : 1,
                emptySpanS : 1,
                columnsL : 2,
                columnsM : 2,
                editable : true,
                content :
                    [
                        new sap.ui.core.Title({
                            text : ""
                        }),
                        new sap.m.Label({
                            text : "{i18n>USERNAME}",

                        }),
                        this.userInput = new sap.m.Input({
                            value: "{cred>/username}",
                            

                        }),
                        new sap.m.Label({
                            text : "{i18n>PASSWORD}",
                        }),
                        this.passwordInput = new sap.m.Input({
                                type: "Password",
                                value: "{cred>/password}",

                        }),
                    ]
                }),
            ],
            footer : new sap.m.Bar({
                contentMiddle : [ new sap.m.Button({
                    text : "{i18n>LOGIN}",
                    icon : "sap-icon://log",
                    press : [ oController.onSavePress, oController ]
                }),
                // new sap.m.Button({
                //     text : "{i18n>RESET}",
                //     icon : "sap-icon://redo",
                //     press : [ oController.onResetPress, oController ]
                // }),
                ]
            })

        });
        return this.page;
    }

});
