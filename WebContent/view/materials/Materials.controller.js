jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");

jQuery.sap.require("model.Material");


jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Common");

jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.materials.Materials", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.Materials.ServiceOrders
	 */
	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);
		util.Common.setFunctionsContext(this);
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);
	},

	_handleRouteMatched: function (evt) {
		var name = evt.getParameter("name");
		this.name = name;

		if ("materials.Materials" !== name) {
			return;
		}
		this.ods = evt.getParameter("arguments").ods;

		this.iModel = model.i18n.getModel();

		//Imposto il modello del materiale
		this.view.setModel(model.ODS.getODS(this.ods), "ods");
		this.view.setModel(model.ODS.getMaterialsInODS(this.ods), "material");
		this.view.setModel(model.ODS.getMaterialsInStock(this.ods), "materialStock");
		this.view.setModel(model.ODS.getODS(this.ods), "ODS");
		this.view.setModel(model.User.getModel(), "user");

		this.synchroModel = new sap.ui.model.json.JSONModel({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.view.setModel(this.synchroModel, "synchro");

		this.setCssForLandscape();
		sap.ui.Device.orientation.attachHandler(this.setCssForLandscape);
	},

	setCssForLandscape: function (e) {

		util.Busy.show();

		var ls;
		if (e) {
			ls = e.landscape;
		} else {
			ls = window.innerHeight < window.innerWidth;
		}

		if (!ls) {
			setTimeout(function () {
				// Input
				var input = jQuery(".inputIssue").parent();
				var row = input.parent();
				input.css('width', '40%');
				row.css('float', 'left');
				row.css('width', '50%');
				row.css('margin-right', '10px');

				// Quantità
				var text = jQuery(".textQty").parent();
				var textRow = text.parent();
				textRow.css('float', 'left');
				textRow.css('width', '50%');
				textRow.css('margin-right', '10px');

				util.Busy.hide();
			}, 300);
		} else {
			util.Busy.hide();
		}

	},

	userModelSuccess: function (oModel) {
		this.view.setModel(oModel, "user");
	},

	ODSSuccess: function (oModel) {
		this.view.setModel(oModel, "orders");
	},

	onSavePress: function () {
		var onSave = jQuery.proxy(this.onSave, this);
		setTimeout(onSave, 300);
	},

	onSave: function () {
		var items = [];

		var materials = this.view.getModel("material").oData.results;
		var stock = this.view.getModel("materialStock").oData.results;

		var errorFound = false;

		//Carico i materiali dell'ordine
		for (var i = 0; i < materials.length; i++) {
			var mt = materials[i];

			// Check sulle quantità inserite
			var qi = parseFloat(mt.QtyIssue);
			var qr = parseFloat(mt.QtyStock);

			if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
				mt.QtyIssue_state = "Error";
				errorFound = true;
			} else {
				mt.QtyIssue = qi.toString();
				mt.QtyStock = qr.toString();
				mt.QtyIssue_state = "None";
			}

			items.push(materials[i]);
		}

		//Carico i materiali che sono solo in stock
		for (i = 0; i < stock.length; i++) {

			var mt = stock[i];

			// Check sulle quantità inserite
			var qi = parseInt(mt.QtyIssue);
			var qr = parseInt(mt.QtyStock);

			if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
				mt.QtyIssue_state = "Error";
				errorFound = true;
			} else {
				mt.QtyIssue = qi.toString();
				mt.QtyStock = qr.toString();
				mt.QtyIssue_state = "None";
			}

			items.push(stock[i]);
		}

		if (errorFound) {
			this.getView().getModel("material").refresh();
			this.getView().getModel("materialStock").refresh();
			sap.m.MessageBox.alert(this.iModel.getProperty("CHECK_QUANTITY"));
			return;
		}

		var odsModel = model.ODS.getModel();
		var ods = odsModel.oData;

		for (var i = 0; i < ods.results.length; i++) {
			var odst = ods.results[i];
			if (odst.Aufnr == this.ods) {
				odst.Materials.results = items;
			}
		}
		odsModel.setData(ods);
		model.ODS.saveModel();

		this.getView().getModel("material").refresh();
		this.getView().getModel("materialStock").refresh();

		sap.m.MessageBox.alert(this.iModel.getProperty("SAVED"));
	},

	onActivatePress: function () {
		var oModel = this.view.getModel("orders");
		var selectedContexts = this.view.list.getSelectedContexts();
		var selectedItems = [];

		for (var i = 0; i < selectedContexts.length; i++) {
			var item = oModel.getProperty(selectedContexts[i].getPath());
			selectedItems.push(item);
		}
		model.ODS.setSelectedItems(selectedItems);
	},

	onNavBackPress: function () {
		sap.ui.Device.orientation.detachHandler(this.setCssForLandscape);
		this.router.myNavBack();
	},

	onSelectPress: function (evt) {
		selected = evt.getParameter("selected");
		var mModel = this.view.getModel("material");
		var path = evt.getSource().getBindingContext("material").getPath();
		if (selected === true) {
			mModel.setProperty(path + "/Status", "A");
		} else {
			mModel.setProperty(path + "/Status", "");
		}
	},

	onSelectStockPress: function (evt) {
		selected = evt.getParameter("selected");
		var mModel = this.view.getModel("materialStock");
		var path = evt.getSource().getBindingContext("materialStock").getPath();
		if (selected === true) {
			mModel.setProperty(path + "/Status", "A");
		} else {
			mModel.setProperty(path + "/Status", "");
		}
	},

	onSelectAllPress: function (evt) {
		var model = this.getView().getModel("material");
		var materials = model.getData().results;

		for (var i = 0; i < materials.length; i++) {
			materials[i].Status = "A";
		}

		model.refresh();
	}
});
