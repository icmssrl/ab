jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.User");

jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Common");

jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.materials.ClientOrders", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.Materials.ServiceOrders
	 */
	enable: {
		steps: false,
		close: false,
		detail: false
	},

	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

		this.onOrdersHelp = $.proxy(this.onOrdersHelp, this);
		this.saveSuccess = $.proxy(this.saveSuccess, this);

		// EVENT BUS
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);

		//Moved here to try if coming back here from serviceOrder, the same item is shown
		this.clientsModel = new sap.ui.model.json.JSONModel();
		var clientData = this.getClientInfo(model.ODS.getModel().getData());
		this.clientsModel.setData(clientData);
		this.view.setModel(this.clientsModel, "clients");

		// var sorter = new sap.ui.model.Sorter("Tplnr", false);
		var sorter = new sap.ui.model.Sorter("Name1", false);
		this.getView().list.getBinding("items").sort([sorter]);

	},

	_handleRouteMatched: function (evt) {
		var name = evt.getParameter("name");
		this.name = name;

		if ("materials.ClientOrders" !== name) {
			return;
		}
		util.Common.setFunctionsContext(this);

		var eModel = new sap.ui.model.json.JSONModel(this.enable);
		this.view.setModel(eModel, "enable");

		model.User.getModel(this.userModelSuccess, util.Error.showStandardError);

		var ordersModel = model.ODS.getModel();
		this.view.setModel(ordersModel, "orders");

//		 var clientsModel = new sap.ui.model.json.JSONModel();
//		 var clientData = this.getClientInfo(ordersModel.getData());
        var clientData = this.getClientInfo(model.ODS.getModel().getData());
		 this.clientsModel.setData(clientData);
		 this.view.setModel(this.clientsModel, "clients");
		//
		// // var sorter = new sap.ui.model.Sorter("Tplnr", false);
		// var sorter = new sap.ui.model.Sorter("Name1", false);
		// this.getView().list.getBinding("items").sort([sorter]);

		this.synchroModel = new sap.ui.model.json.JSONModel({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.view.setModel(this.synchroModel, "synchro");
		//

		// var page = sap.ui.getCore().byId("clientPageId");
		// var appStatusModel = sap.ui.getCore().getModel("appStatus");
		// if(appStatusModel.getProperty("/positionClicked"))
		// {
		// 	var position = appStatusModel.getProperty("/positionClicked");
		// 	page.scrollTo(-position + 200, 0);
		// }

		// var page = sap.ui.getCore().byId("clientPageId");
		// var appStatusModel = sap.ui.getCore().getModel("appStatus");
		// if(appStatusModel.getProperty("/clientSelected"))
		// {
		// 	var item= this.getClientItem(appStatusModel.getProperty("/clientSelected"));
		// 	if(item && item !== null )
		// 		item.focus();
		// 	// var domSelected= item.getDomRef();
		// 	//
		// 	// domSelected.scrollIntoView();
		// 	// page.scrollToElement(item);
		// 	appStatusModel.setProperty("/clientSelected", "");
		// }
        
        /* Fabio 11/04/2016 */
        this.getView().list.getBinding("items").filter([]);
        this.getView().search.setProperty("value", "");

	},
	getClientItem:function(obj)
	{
		var items = this.getView().list.getItems();
		// var list  = sap.ui.getCore().byId("clientListId");
		if(!items || items.length === 0)
			return null;

		for(var i = 0; i< items.length; i++)
		{
			var itemObj = items[i].getBindingContext("clients").getObject();
			// var itemObj = items[i].getObject();
			if(itemObj.Tplnr === obj.Tplnr && itemObj.Pltxt === obj.Pltxt && itemObj.Name1 === obj.Name1)
				return items[i];
		}

		return null;
	},
	getClientInfo:function(ordersData)
	{
		var clients = {"results":[]};
		var orders = ordersData.results;
		var trovato = false;
		for(var i = 0;i< orders.length; i++)
		{
			trovato = false;
				for(var j = 0; j< clients.results.length; j++)
				{
					if((util.Formatter.formatClientName(clients.results[j].Name1) == util.Formatter.formatClientName(orders[i].Name1))&&(clients.results[j].Tplnr == orders[i].Tplnr))
					{
						trovato = true;
						break;
					}
				}
				if(!trovato)
				{
					clients.results.push({"Tplnr": orders[i].Tplnr, "Pltxt":orders[i].Pltxt, "Name1":orders[i].Name1});
				}
			}


		return clients;

	},
	userModelSuccess: function (oModel) {
		this.view.setModel(oModel, "user");
	},

	ODSSuccess: function (oModel) {
		this.view.setModel(oModel, "orders");
	},

	// onMaterialPress: function (evt) {
	// 	var selectedItem = this.getSelectedItem();
	// 	if (selectedItem === false) return;
	//
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var view = this.getView();
	// 	var model = view.getModel("orders");
	// 	var item = model.getProperty(path);
	// 	this.router.navTo("materials.Materials", {
	// 		ods: item.Aufnr
	// 	});
	// },

	onHomePress: function () {
		this.router.navTo("home");
	},

	// onClosePress: function (evt) {
	// 	var selectedItem = this.getSelectedItem();
	// 	if (selectedItem === false) return;
	//
	// 	var iModel = model.i18n.getModel();
	//
	// 	// Creo il modello relativo all'ordine
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var mModel = this.view.getModel("orders");
	// 	var order = mModel.getProperty(path);
	//
	// 	// Imposto il campo per la data di chiusura
	// 	order.closeDate = undefined;
	// 	var oModel = new sap.ui.model.json.JSONModel(order);
	//
	// 	var tmpDate = new Date(order.Idate);
	// 	var year = tmpDate.getFullYear().toString();
	// 	// In sap è stato impostato che se la data è 2999 non richiede la lettura delle ore motore
	// 	if (year !== "2999") {
	// 		// Apro il Dialog per la chiusura dell'ordine
	// 		this.closeDialog = sap.ui.jsfragment("view.fragment.CloseDialog", this);
	// 		this.getView().addDependent(this.CloseDialog);
	// 		this.closeDialog.setModel(oModel, "order");
	// 		this.closeDialog.setModel(iModel, "i18n");
	// 		this.closeDialog.open();
	// 	} else {
	// 		var onClose = function (oAction) {
	// 			if (oAction === "OK") {
	// 				// FIXME Imposto una data e ora valida perchè 2999 non viene accettata
	// 				order.Idate = new Date(order.Idate.toString());
	// 				order.Itime = util.Formatter.currentTime();
	// 				this.closeOrder();
	// 			}
	// 		};
	// 		onClose = jQuery.proxy(onClose, this);
	//
	// 		sap.m.MessageBox.confirm(model.i18n.getModel().getProperty("CONFIRM_CLOSE"), {
	// 			title: "Confirm", // default
	// 			onClose: onClose // default
	// 		});
	// 	}
	//
	// },

	// onContDateChange: function (evt) {
	// 	var model = this.closeDialog.getModel("order");
	// 	var value = util.Formatter.dateShortToDateISO(evt.getParameters().newValue);
	// 	model.setProperty("/closeDate", value);
	// },

	//FIXME QUESTA FUNZIONE è DA SISTEAMRE PERCHè CREA UN RIFERIMENTO CIRCOLARE SULL'ITEM
	// closeConfirm: function () {
	//
	// 	var selectedItem = this.getSelectedItem();
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var orderModel = this.view.getModel("orders");
	//
	// 	var item = orderModel.getProperty(path);
	//
	// 	var selectedOrderModel = this.closeDialog.getModel("order");
	// 	// Se non ho inserito un contatore allora non esco
	// 	if (!item.actualrecdv) {
	// 		selectedOrderModel.setProperty("/actualrecdv_state", "Error");
	// 		return;
	// 	}
	//
	// 	if (!item.closeDate) {
	// 		selectedOrderModel.setProperty("/closeDate_state", "Error");
	// 		return;
	// 	}
	//
	// 	this.closeDialog.close();
	//
	// 	item.Recdv = item.actualrecdv;
	// 	// Quando confermo la chiusura ripasso il dato al campo dell'odata.
	// 	item.Idate = new Date(item.closeDate);
	// 	item.Itime = util.Formatter.currentTime();
	// 	orderModel.refresh();
	//
	// 	this.closeOrder();
	// },

	// closeOrder: function () {
	// 	var selectedItem = this.getSelectedItem();
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var orderModel = this.view.getModel("orders");
	// 	var item = orderModel.getProperty(path);
	//
	// 	var materials = item.Materials.results;
	// 	for (var i = 0; i < materials.length; i++) {
	// 		var material = materials[i];
	//
	// 		// Se ho un ordine che non deve visualizzare i materiali
	// 		if (item.Bemot === "01") {
	// 			if (material.Aufnr !== "") {
	// 				material.Status = "B";
	// 			}
	// 		}
	// 		// Se ho un ordine che visualizza i materiali
	// 		else {
	// 			// Se ho un materiale dell'ordine
	// 			if (material.Aufnr !== "") {
	// 				// Se non è fleggato lo segno come da scaricare ma imposto la quantità a 0
	// 				if (material.Status === "") {
	// 					material.Status = "B";
	// 					material.QtyIssue = "0.000";
	// 				} else
	// 				// Se è stato fleggato lo passo normale
	// 				{
	// 					material.Status = "B";
	// 				}
	// 			} else
	// 			// Se ho un materiale dello stock
	// 			{
	// 				// Se è stato fleggato lo passo
	// 				if (material.Status === "A") {
	// 					material.Status = "B";
	// 				}
	// 			}
	// 		}
	//
	// 		// if (item.Bemot !== "01") {
	// 		// 	if (material.Status === "A") {
	// 		// 		material.Status = "B";
	// 		// 	}
	// 		// } else {
	// 		// 	if (material.Aufnr !== "") {
	// 		// 		material.Status = "B";
	// 		// 	}
	// 		// }
	// 	}
	// 	orderModel.setProperty(path, item);
	// 	model.ODS.model = orderModel;
	// 	model.ODS.saveModel();
	//
	// 	var iModel = model.i18n.getModel();
	// 	sap.m.MessageBox.alert(iModel.getProperty("ODS_CLOSED"));
	// },


	// closeCancel: function () {
	// 	this.closeDialog.close();
	// },

	// onStepsPress: function (evt) {
	// 	var selectedItem = this.getSelectedItem();
	// 	if (selectedItem === false) return;
	//
	// 	var mModel = this.view.getModel("orders");
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var order = mModel.getProperty(path);
	// 	order.actualrecdv = "";
	// 	var fasi = order.Fasi;
	// 	var oModel = new sap.ui.model.json.JSONModel(order);
	// 	var fModel = new sap.ui.model.json.JSONModel(fasi);
	//
	// 	this.fasiDialog = sap.ui.jsfragment("view.fragment.FasiDialog", this);
	// 	this.getView().addDependent(this.fasiDialog);
	//
	// 	this.fasiDialog.setModel(fModel, "fasi");
	// 	this.fasiDialog.setModel(oModel, "order");
	// 	this.fasiDialog.open();
	// },
	//
	// onFasiClose: function () {
	// 	this.fasiDialog.close();
	// },

	// getSelectedItem: function () {
	// 	var selectedItem = this.view.list.getSelectedItem();
	// 	if (selectedItem === undefined || selectedItem === null) {
	// 		var iModel = model.i18n.getModel();
	// 		sap.m.MessageBox.alert(iModel.getProperty("NO_ITEM_SELECT"));
	// 		return false;
	// 	}
	// 	return selectedItem;
	// },
	//
	// onSelectionChange: function (evt) {
	// 	var selectedItem = this.view.list.getSelectedItem();
	// 	var path = selectedItem.getBindingContext("orders").getPath();
	// 	var o = this.view.getModel("orders").getProperty(path);
	//
	// 	var eModel = this.view.getModel("enable");
	//
	// 	eModel.setProperty("/steps", true);
	// 	eModel.setProperty("/close", true);
	//
	//
	//
	// 	// Imposto l'abilitazione del tasto close in base all'ordine aperto o chiuso
	// 	var materials = o.Materials.results;
	// 	var close = true;
	// 	for (var i = 0; i < materials.length; i++) {
	// 		var status = materials[i].Status;
	// 		if (status === "B") {
	// 			close = false;
	// 		}
	// 	}
	// 	eModel.setProperty("/close", close);
	//
	// 	// Imposto il tasto dei dettagli
	// 	if (o.Bemot !== "01") {
	// 		eModel.setProperty("/detail", close);
	// 	} else {
	// 		eModel.setProperty("/detail", false);
	// 	}
	// },

	// onCheckFasi: function (evt) {
	// 	selected = evt.getParameter("selected");
	// 	var orders = this.view.getModel("orders").getData();
	// 	var fasiModel = this.fasiDialog.getModel("fasi");
	// 	var path = evt.getSource().getBindingContext("fasi").getPath();
	//
	// 	fase = fasiModel.getProperty(path);
	//
	// 	// Imposto la fase
	// 	if (selected === true) {
	// 		fase.Usr10 = "X";
	// 	} else {
	// 		fase.Usr10 = "";
	// 	}
	//
	// },

	// onSelectAll: function () {
	// 	var enable = "";
	//
	// 	var fasi = this.fasiDialog.getModel("fasi").getData().results;
	//
	// 	if (fasi[0] && !fasi[0].Usr10) enable = "X";
	//
	// 	for (var i = 0; i < fasi.length; i++) {
	// 		fasi[i].Usr10 = enable;
	// 	}
	//
	// 	this.fasiDialog.getModel("fasi").refresh();
	// },

	// onOrdersSearch: function (evt) {
	// 	var val = evt.getParameter("newValue");
	// 	var aFilters = [];
	// 	aFilters.push(new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.Contains, val));
	// 	aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.Contains, val));
	// 	aFilters.push(new sap.ui.model.Filter("Gstrp", sap.ui.model.FilterOperator.Contains, val));
	// 	aFilters.push(new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, val));
	//
	// 	var oFilter = new sap.ui.model.Filter({
	// 		filters: aFilters
	// 	});
	// 	this.getView().list.getBinding("items").filter([oFilter]);
	// },
	onClientsSearch: function (evt) {
		var val = evt.getParameter("newValue");
		var aFilters = [];
		aFilters.push(new sap.ui.model.Filter("Tplnr", sap.ui.model.FilterOperator.Contains, val));
		aFilters.push(new sap.ui.model.Filter("Pltxt", sap.ui.model.FilterOperator.Contains, val));

		aFilters.push(new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, val));

		var oFilter = new sap.ui.model.Filter({
			filters: aFilters
		});
		this.getView().list.getBinding("items").filter([oFilter]);
	},



	onSynchronize: function () {
		util.Synchronize.begin();
	},

	onSort: function()
	{
		this.sortDialog = sap.ui.jsfragment("view.fragment.ClientsSortDialog", this);
		this.getView().addDependent(this.sortDialog);
		this.sortModel = new sap.ui.model.json.JSONModel({sortBy: "Tplnr", sortType: "asc"});
		this.sortDialog.setModel(this.sortModel, "sort");
		this.sortDialog.open();
	},

	onSortClose: function()
	{
		var by = this.sortModel.getData().sortBy;
		var type = this.sortModel.getData().sortType;
		var basc = true;
		if (type === "asc") basc = false;

		var sorter = new sap.ui.model.Sorter(by, basc);
		// if (by === "Gstrp")
		// {
		// 	 sorter.fnCompare = function(a, b) {
		// 		 return new Date(a) - new Date(b);
		// 	 }
		// }

		this.getView().list.getBinding("items").sort([sorter]);
		this.sortDialog.close();
	},

	onSynchronizeComplete: function()
	{
		var ordersModel = model.ODS.getModel();
		this.view.setModel(ordersModel, "orders");
		this.getView().getModel("orders").refresh();
		this.synchroModel.setData({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.synchroModel.refresh();
        var clientData = this.getClientInfo(model.ODS.getModel().getData());
		this.clientsModel.setData(clientData);
        this.clientsModel.refresh();
	},

	onItemPress:function(evt)
	{


		var client = evt.getSource().getBindingContext("clients").getObject();
		// var appStatusModel = sap.ui.getCore().getModel("appStatus");
		// appStatusModel.setProperty("/clientSelected", client);

		// var appStatusModel = sap.ui.getCore().getModel("appStatus");
		// var item = evt.getSource();
		// var jqueryItem = $('#'+item.getId());
		// var position = jqueryItem.offset().top + jqueryItem.offsetParent().offset().top ;
		// //
		// appStatusModel.setProperty("/positionClicked", position );
		// appStatusModel.setProperty("/clientSelected", client);

		var headquarter = client.Tplnr;
		var name = client.Name1;

		this.router.navTo("materials.ServiceOrders", {"headquarter":headquarter, "name":util.Formatter.formatClientName(name)});
	}

});
