jQuery.sap.require("model.ActiveOrders");
jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.materials.Steps", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.Materials.ServiceOrders
	 */
	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);
		util.Common.setFunctionsContext(this);
	},

	_handleRouteMatched: function (evt) {
		var name = evt.getParameter("name");
		this.name = name;

		if ("materials.Steps" !== name) {
			return;
		}
		this.headquarter = evt.getParameters().arguments.headquarter;
		this.name = evt.getParameters().arguments.name;

		// if(model.ActiveOrders.getOrders().length == 0)
		// {
		//   sap.m.MessageBox.alert(model.i18n.getModel().getProperty("NO_ORDER_SELECTED"));
		//   return;
		// }



		this.iModel = model.i18n.getModel();

		this.ordersStepsModel = new sap.ui.model.json.JSONModel();
		this.ordersStepsModel.setData({
			"orders": model.ActiveOrders.getOrdersValues()
		});
		this.orders = this.ordersStepsModel.getData().orders;
		this.getView().setModel(this.ordersStepsModel, "orders");
		this.getView().page.removeAllContent();
		var panels = [];
		for (var i = 0; i < this.orders.length; i++) {
			var panel = new sap.m.Panel();
			panel.setHeaderText(this.orders[i].Aufnr + ": " + this.orders[i].Ktext);
            
            /* 19/10/2016 */
            for (var t = 0; t < this.orders[i].Fasi.results.length; t++) {
                if (this.orders[i].Fasi.results[t].Usr10 === "X") {
                    var materiali = _.where(this.orders[i].Materials.results, {vornr: this.orders[i].Fasi.results[t].Vornr});
                    for (var q = 0; q < materiali.length; q++) {
                        materiali[q].check = true;
                    }
                }   
            }
            /* 19/10/2016 */
            
			var orderStepList = new sap.m.List({
				items: {
					path: "orders>/orders/" + i + "/Fasi/results/",
					template: new sap.m.CustomListItem({
						content: [
							new sap.m.HBox({
								items: [
										new sap.m.CheckBox({
										// text: "{orders>Ltxa1}",
										select: [this.onCheckFasi, this],
										selected: {
											path: "orders>Usr10",
											formatter: function (status) {
												if (status === "X") {
													return true;
												} else {
													return false;
												}
											}
										},
                                        enabled: {
//                                            path: "orders>Usr10",
                                            parts:
                                            [{
                                                    path: "orders>Usr10"
                                            },
                                            {
                                                    path: "orders>toSync",
                                            }],
                                            formatter: function (status, sync) {
                                                if (!sync || sync === "") {
                                                    if (status === "X") {
                                                        return false;
                                                    } else {
                                                        return true;
                                                    }
                                                } else {
                                                    return true;
                                                }
											}
                                        }
									}),
										new sap.m.TextArea({
										editable: false,
										value: "{orders>Ltxa1}",
										width: "100%"
									}).addStyleClass("textAreaStyle")
									],
								alignItems: "Center",
								width: "100%"
							})
							]




					})
				}
			});
			panel.addContent(orderStepList);


			this.getView().page.addContent(panel);
		}




		// this.view.setModel(model.ODS.getODS(this.ods), "ods");
		// this.view.setModel(model.ODS.getMaterialsInODS(this.ods), "material");
		// this.view.setModel(model.ODS.getMaterialsInStock(this.ods), "materialStock");
		// this.view.setModel(model.ODS.getODS(this.ods), "ODS");
		// this.view.setModel(model.User.getModel(), "user");

		this.setCssForLandscape();
		sap.ui.Device.orientation.attachHandler(this.setCssForLandscape);
	},

	setCssForLandscape: function (e) {

		util.Busy.show();

		setTimeout(function () {
			// Input
			$('textarea').css("margin-right", "0px");
			$('textarea').css("margin-left", "0px");
			$('textarea').css("padding-right", "0px");
			$('textarea').css("padding-left", "0px");
			$('textarea').css("border-style", "none");
			// $('textArea').css("height", "100%");
			var textArea = jQuery(".textAreaStyle");

			var input = jQuery(".textAreaStyle").parent();
			var row = input.parent();
			input.css('width', '100%');

			row.css('width', '100%');


			util.Busy.hide();
		}, 300);

	},

	// userModelSuccess: function (oModel) {
	// 	this.view.setModel(oModel, "user");
	// },
	//
	// ODSSuccess: function (oModel) {
	// 	this.view.setModel(oModel, "orders");
	// },
	//
	// onSavePress: function () {
	// 	var onSave = jQuery.proxy(this.onSave, this);
	// 	setTimeout(onSave, 300);
	// },
	//
	// onSave: function () {
	// 	var items = [];
	//
	// 	var materials = this.view.getModel("material").oData.results;
	// 	var stock = this.view.getModel("materialStock").oData.results;
	//
	// 	var errorFound = false;
	//
	// 	//Carico i materiali dell'ordine
	// 	for (var i = 0; i < materials.length; i++) {
	// 		var mt = materials[i];
	//
	// 		// Check sulle quantità inserite
	// 		var qi = parseFloat(mt.QtyIssue);
	// 		var qr = parseFloat(mt.QtyStock);
	//
	// 		if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
	// 			mt.QtyIssue_state = "Error";
	// 			errorFound = true;
	// 		} else {
	// 			mt.QtyIssue = qi.toString();
	// 			mt.QtyStock = qr.toString();
	// 			mt.QtyIssue_state = "None";
	// 		}
	//
	// 		items.push(materials[i]);
	// 	}
	//
	// 	//Carico i materiali che sono solo in stock
	// 	for (i = 0; i < stock.length; i++) {
	//
	// 		var mt = stock[i];
	//
	// 		// Check sulle quantità inserite
	// 		var qi = parseInt(mt.QtyIssue);
	// 		var qr = parseInt(mt.QtyStock);
	//
	// 		if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
	// 			mt.QtyIssue_state = "Error";
	// 			errorFound = true;
	// 		} else {
	// 			mt.QtyIssue = qi.toString();
	// 			mt.QtyStock = qr.toString();
	// 			mt.QtyIssue_state = "None";
	// 		}
	//
	// 		items.push(stock[i]);
	// 	}
	//
	// 	if (errorFound) {
	// 		this.getView().getModel("material").refresh();
	// 		this.getView().getModel("materialStock").refresh();
	// 		sap.m.MessageBox.alert(this.iModel.getProperty("CHECK_QUANTITY"));
	// 		return;
	// 	}
	//
	// 	var odsModel = model.ODS.getModel();
	// 	var ods = odsModel.oData;
	//
	// 	for (var i = 0; i < ods.results.length; i++) {
	// 		var odst = ods.results[i];
	// 		if (odst.Aufnr == this.ods) {
	// 			odst.Materials.results = items;
	// 		}
	// 	}
	// 	odsModel.setData(ods);
	// 	model.ODS.saveModel();
	//
	// 	this.getView().getModel("material").refresh();
	// 	this.getView().getModel("materialStock").refresh();
	//
	// 	sap.m.MessageBox.alert(this.iModel.getProperty("SAVED"));
	// },
	//
	// onActivatePress: function () {
	// 	var oModel = this.view.getModel("orders");
	// 	var selectedContexts = this.view.list.getSelectedContexts();
	// 	var selectedItems = [];
	//
	// 	for (var i = 0; i < selectedContexts.length; i++) {
	// 		var item = oModel.getProperty(selectedContexts[i].getPath());
	// 		selectedItems.push(item);
	// 	}
	// 	model.ODS.setSelectedItems(selectedItems);
	// },

	onNavBackPress: function () {
		sap.ui.Device.orientation.detachHandler(this.setCssForLandscape);
		// this.router.myNavBack();
		this.router.navTo("materials.ServiceOrders", {
			"headquarter": this.headquarter,
			"name": this.name
		});
	},

	onForwardPress: function (evt) {
		this.router.navTo("materials.StepMaterials", {
			"headquarter": this.headquarter,
			"name": this.name,
			"numOrder": 0
		});
	},

	onCheckFasi: function (evt) {
		var selected = evt.getParameter("selected");
		var path = evt.getSource().getBindingContext("orders").getPath();
        
         /* 19/10/2016 */
        var indexPath = parseInt(path.split("/")[2]);
        var indexPathFasi = path.split("/")[path.split("/").length -1];
        var orderSelected = this.getView().getModel("orders").getData().orders[indexPath];
        var faseSelezionata = orderSelected.Fasi.results[indexPathFasi];
        var materiali = _.where(orderSelected.Materials.results, {vornr: faseSelezionata.Vornr});
         /* 19/10/2016 */
        
		// Imposto la fase
		if (selected === true) {
			this.getView().getModel("orders").setProperty(path + "/Usr10", "X");
			
            /* 19/10/2016 */
            this.getView().getModel("orders").setProperty(path + "/toSync", "X");
            for (var i = 0; i < materiali.length; i++) {
                materiali[i].check = true;
                materiali[i].toSync = true;
                materiali[i].Status = "A";
            }
            /* 19/10/2016 */
		} else {
			this.getView().getModel("orders").setProperty(path + "/Usr10", "");
            /* 19/10/2016 */
            this.getView().getModel("orders").setProperty(path + "/toSync", "");
            for (var t = 0; t < materiali.length; t++) {
                materiali[t].check = false;
                materiali[t].toSync = false;
                materiali[t].QtyIssue = parseInt(materiali[t].QtyReq) < parseInt(materiali[t].QtyStock) ? materiali[t].QtyReq : materiali[t].QtyStock;
                if (materiali[t].Status === "A") {
                    materiali[t].Status = "";
                }
            }
            /* 19/10/2016 */
		}

	},
    
    onSelectAllPress: function () {
        var orders = this.getView().getModel("orders").getData().orders;
        var materiali;
        var tornaVero = false;
        var fasiToCheck = [];
        for (var i=0; i<orders.length; i++) {
            var tempFasi = this.getView().getModel("orders").getData().orders[i].Fasi.results;
            for (var r=0; r<tempFasi.length; r++) {
                fasiToCheck.push(tempFasi[r]);
            }
        }
        if (_.find(fasiToCheck, {Usr10: ''}) && _.find(fasiToCheck, {Usr10: 'X'})) {
            tornaVero = true;
        }
        
        for (var i=0; i<orders.length; i++) {
            var fasi = this.getView().getModel("orders").getData().orders[i].Fasi.results;
            
            if (_.find(fasi, {Usr10: ''}) || tornaVero) {
                for (var t=0; t<fasi.length; t++) {
                    if (fasi[t].Usr10 !== "X") {
                        fasi[t].Usr10 = "X";
                        fasi[t].toSync = "X";
                        materiali = _.where(orders[i].Materials.results, {vornr: fasi[t].Vornr});
                        for (var w = 0; w < materiali.length; w++) {
                            if (materiali[w].Status === "") {
                                materiali[w].check = true;
                                materiali[w].toSync = true;
                                materiali[w].Status = "A";
                            }
                        }
                    }
                    
                    
                }
            } else {
                for (var t=0; t<fasi.length; t++) {
                    
                    
                    if (fasi[t].toSync) {
                        fasi[t].Usr10 = "";
                        fasi[t].toSync = "";
                        materiali = _.where(orders[i].Materials.results, {vornr: fasi[t].Vornr});
                        for (var w = 0; w < materiali.length; w++) {
                            if (materiali[w].toSync) {
                                materiali[w].check = false;
                                materiali[w].toSync = false;
                                materiali[w].QtyIssue = parseInt(materiali[w].QtyReq) < parseInt(materiali[w].QtyStock) ? materiali[w].QtyReq : materiali[w].QtyStock;
                                if (materiali[w].Status === "A") {
                                    materiali[w].Status = "";
                                }
                            }
                        }
                    }
                }
            }
        }
        this.getView().getModel("orders").refresh();
        
//        var model = this.getView().getModel("material");
//		var materials = model.getData().results;
//		
//		for (var i = 0; i < materials.length; i++) {
//            materials[i].check = true;
//            materials[i].toSync = true;
//            this.getView().getModel("orders").setProperty(path + "/Usr10", "X");
//			materials[i].Status = "A";
//		}
		
//		model.refresh();
    },

	onHomePress: function () {
			this.router.navTo("home");
		},
		// onSelectPress: function (evt) {
		// 	selected = evt.getParameter("selected");
		// 	var mModel = this.view.getModel("material");
		// 	var path = evt.getSource().getBindingContext("material").getPath();
		// 	if (selected === true) {
		// 		mModel.setProperty(path + "/Status", "A");
		// 	} else {
		// 		mModel.setProperty(path + "/Status", "");
		// 	}
		// },
		//
		// onSelectStockPress: function (evt) {
		// 	selected = evt.getParameter("selected");
		// 	var mModel = this.view.getModel("materialStock");
		// 	var path = evt.getSource().getBindingContext("materialStock").getPath();
		// 	if (selected === true) {
		// 		mModel.setProperty(path + "/Status", "A");
		// 	} else {
		// 		mModel.setProperty(path + "/Status", "");
		// 	}
		// },
		//
		 
});