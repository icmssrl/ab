jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.User");
jQuery.sap.require("model.ActiveOrders");
jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Common");

jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.materials.ServiceOrders", {

	/**
	 * Called when a controller is instantiated and its View controls (if available) are already created.
	 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	 * @memberOf view.Materials.ServiceOrders
	 */


	onInit: function () {
		this.view = this.getView();
		this.router = sap.ui.core.UIComponent.getRouterFor(this);
		this.router.attachRoutePatternMatched(this._handleRouteMatched, this);

		this.onOrdersHelp = $.proxy(this.onOrdersHelp, this);
		this.saveSuccess = $.proxy(this.saveSuccess, this);

		// EVENT BUS
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);

	},
	onAfterRendering:function()
	{


		this.resetList();
	},

	_handleRouteMatched: function (evt) {
		var name = evt.getParameter("name");
		this.name = name;

		if ("materials.ServiceOrders" !== name) {
			return;
		}
        
        /* Fabio 11/04/2016 */
//        this.getView().list.getBinding("items").filter([]);
        this.getView().ordersSearch.setProperty("value", "");
        /* Fabio 11/04/2016 */

		this.headquarter = evt.getParameters().arguments.headquarter;
		this.name = evt.getParameters().arguments.name;



		util.Common.setFunctionsContext(this);



		model.User.getModel(this.userModelSuccess, util.Error.showStandardError);
		//Commented to show only not closed orders->to check ------//
		var ordersModel = model.ODS.getModel();
		this.view.setModel(ordersModel, "orders");
		//-----------------------------------------------------------
		// var allOrders= model.ODS.getModel().getData().results;
		// for(var i = 0 ; i< )
		var headerModel = new sap.ui.model.json.JSONModel();
		headerModel.setData({"Tplnr":this.headquarter});
		this.getView().setModel(headerModel, "header");
		var filterQuarter = new sap.ui.model.Filter("Tplnr", sap.ui.model.FilterOperator.EQ, this.headquarter);

		var that = this;

		// -- Funzione di comparazione
		var fFilter = function(e)
		{

			var selected = that.name;
			if (util.Formatter.formatClientName(e).toUpperCase() == selected.toUpperCase())
			{
				return true;
			}
			else
			{
				return false;
			}
		};



		var filterName = new sap.ui.model.Filter({path: "Name1", test: fFilter});
		this.filterOrder = new sap.ui.model.Filter({filters:[filterQuarter, filterName], and:true});
		// this.filterOrder = new sap.ui.model.Filter({filters:[filterQuarter], and:true});
		this.getView().list.getBinding("items").filter(this.filterOrder);
		this.resetList();

		this.synchroModel = new sap.ui.model.json.JSONModel({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.view.setModel(this.synchroModel, "synchro");

		this.setCssForLandscape();
		sap.ui.Device.orientation.attachHandler(this.setCssForLandscape);

        
	},




	setCssForLandscape: function (e) {

		util.Busy.show();

		var ls;
		if (e) {
			ls = e.landscape;
		} else {
			ls = window.innerHeight < window.innerWidth;
		}

		if (!ls) {
			setTimeout(function () {
				// ods
				var orderSpan = $(".orderStyle");
				orderSpan.css('width', '100%');
				var order = jQuery(".orderStyle").parent();
				var row = order.parent();
//				order.css('width', '60%');
				// order.css('margin-right','5px');
				row.css('float', 'left');
				row.css('width', '50%');

				//Removing paddin of columnItem to let date and order code on the same row
				var listItem = row.parent();
				listItem.css('padding-left', '0.7em');
				listItem.css('padding-right', '0.7em');

				// row.css('border-width', 'none');
				// row.css('margin-right', '5px');

				// date
				var date = jQuery(".dateStyle").parent();
				var dateRow = date.parent();
				// dateRow.css('float', 'left');
				dateRow.css('width', '50%');
				dateRow.css('margin', '0%');



				util.Busy.hide();
			}, 300);
		} else {
			util.Busy.hide();
		}

	},

	resetList:function()
	{
		model.ActiveOrders.reset();
		var items = this.getView().list.getSelectedItems();
		for(var i = 0; i< items.length; i++)
		{
			items[i].setSelected(false);
		}
		return true;
	},

	userModelSuccess: function (oModel) {
		this.view.setModel(oModel, "user");
	},

	ODSSuccess: function (oModel) {
		this.view.setModel(oModel, "orders");
	},



	onHomePress: function () {
		this.router.navTo("home");
	},



	//Function to check if the orders are about the same motors
	checkZPoints : function(items)
	{
		var diff = false; //boolean to check if it was found a different value of zpoint
		if(items.length > 1)
		{
			var zpoint = undefined;

			for(var i = 0 ; i<items.length; i++)
			{
				var obj = items[i].getBindingContext("orders").getObject();
				if(!zpoint || (obj.Zpoint == zpoint) || (obj.Zpoint == ""))
				{
					zpoint = obj.Zpoint;
				}
				else
				{
					diff = true;
					break;
				}
			}
		}
		return !diff;
	},

	//
	// getSelectedItem: function () {
	// 	var selectedItem = this.view.list.getSelectedItem();
	// 	if (selectedItem === undefined || selectedItem === null) {
	// 		var iModel = model.i18n.getModel();
	// 		sap.m.MessageBox.alert(iModel.getProperty("NO_ITEM_SELECT"));
	// 		return false;
	// 	}
	// 	return selectedItem;
	// },
	// getSelectedItems : function()
	// {
	// 	var selectedItems = this.view.list.getSelectedItems();
	// 	if (selectedItems === undefined || selectedItems === null) {
	// 		var iModel = model.i18n.getModel();
	// 		sap.m.MessageBox.alert(iModel.getProperty("NO_ITEM_SELECT"));
	// 		return false;
	// 	}
	// 	return selectedItems;
	// },




	checkMaterialStatus:function(obj)
	{
		var materials = obj.Materials.results;
		var close = true;
		for (var k = 0; k < materials.length; k++)
		{
			var status = materials[k].Status;
			if (status === "B")
			{
				close = false;
			}
		}
		return close;

	},
	onCheckFasi: function (evt) {
		selected = evt.getParameter("selected");
		var orders = this.view.getModel("orders").getData();
		var fasiModel = this.fasiDialog.getModel("fasi");
		var path = evt.getSource().getBindingContext("fasi").getPath();

		fase = fasiModel.getProperty(path);

		// Imposto la fase
		if (selected === true) {
			fase.Usr10 = "X";
		} else {
			fase.Usr10 = "";
		}

	},


	onOrdersSearch: function (evt) {
		var val = evt.getParameter("newValue");
		var aFilters = [];


		aFilters.push(new sap.ui.model.Filter("Aufnr", sap.ui.model.FilterOperator.Contains, val));
		aFilters.push(new sap.ui.model.Filter("Ktext", sap.ui.model.FilterOperator.Contains, val));
		aFilters.push(new sap.ui.model.Filter("Gstrp", sap.ui.model.FilterOperator.Contains, val));
		aFilters.push(new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, val));

		var oFilter = new sap.ui.model.Filter({
		filters: aFilters, and:false});
		var completeFilter= new sap.ui.model.Filter({filters:[this.filterOrder, oFilter], and:true}); // Fabio 11/04/2016


//		 this.getView().list.getBinding("items").filter([oFilter]);
		this.getView().list.getBinding("items").filter(completeFilter); // Fabio 11/04/2016
	},

	onSynchronize: function () {
		util.Synchronize.begin();
	},

	onSort: function()
	{
		this.sortDialog = sap.ui.jsfragment("view.fragment.SortDialog", this);
		this.getView().addDependent(this.sortDialog);
		this.sortModel = new sap.ui.model.json.JSONModel({sortBy: "Aufnr", sortType: "asc"});
		this.sortDialog.setModel(this.sortModel, "sort");
		this.sortDialog.open();
	},

	onSortClose: function()
	{
		var by = this.sortModel.getData().sortBy;
		var type = this.sortModel.getData().sortType;
		var basc = true;
		if (type === "asc") basc = false;

		var sorter = new sap.ui.model.Sorter(by, basc);
		if (by === "Gstrp")
		{
			 sorter.fnCompare = function(a, b) {
				 return new Date(a) - new Date(b);
			 }
		}

		this.getView().list.getBinding("items").sort([sorter]);
		this.sortDialog.close();
	},

	onSynchronizeComplete: function()
	{
		var ordersModel = model.ODS.getModel();
		this.view.setModel(ordersModel, "orders");
		this.getView().getModel("orders").refresh();
		this.getView().list.getBinding("items").filter([this.filterOrder]);
		this.synchroModel.setData({"dateTime" : model.Synchronize.getDateTime(), "en": model.Synchronize.isSynchronized()});
		this.synchroModel.refresh();
	},
	onNavBackPress:function()
	{
		this.router.navTo("materials.ClientOrders");
		// this.router.myNavBack();
	},


	onElaboratePress:function(evt)
	{
		var selectedItems = this.getView().list.getSelectedItems();
		var itemObjs = [];
		var zpoint  = undefined;
		if(selectedItems.length>0)
		{
			// check zpoint(genset) is equal in every order
			for(var i = 0; i< selectedItems.length; i++)
			{

				var obj = selectedItems[i].getBindingContext("orders").getObject();
				itemObjs[i] = {"value":selectedItems[i].getBindingContext("orders").getObject(), "path":selectedItems[i].getBindingContext("orders").getPath()};
				if(!parseInt(zpoint))
				{
					zpoint = itemObjs[i].value.Zpoint;
				}

				if(zpoint != itemObjs[i].value.Zpoint && parseInt(itemObjs[i].value.Zpoint))
				{
					sap.m.MessageBox.alert(model.i18n.getModel().getProperty("FAILED_CLOSE_DIFFERENT_ZPOINT"));
					return;
				}
				if(!this.checkMaterialStatus(obj))
				{
					sap.m.MessageBox.alert(model.i18n.getModel().getProperty("ERROR_ON_ORDER")+" "+obj.Aufnr+": "+ model.i18n.getModel().getProperty("ORDER_ALREADY_CLOSED"));
					return;
				}
			}
			//Writing orders in model data, is it better to write in session?
			model.ActiveOrders.setOrdersToClose(itemObjs);
			this.router.navTo("materials.Steps", {"headquarter":this.headquarter, "name":this.name});
		}
		else
		{
	      sap.m.MessageBox.alert(model.i18n.getModel().getProperty("NO_ORDER_SELECTED"));
	      return;

		}


	},
	onDetailPress  :function(evt)
	{
		var src = evt.getSource();
		console.log(evt.getSource().toString());
		this.textDialog = sap.ui.jsfragment("view.fragment.TextDialog", this);
		this.getView().addDependent(this.textDialog);
		var oModel = new sap.ui.model.json.JSONModel();
		var textModel = new sap.ui.model.json.JSONModel();
		var order = src.getBindingContext("orders").getObject();
		var text={"text":""};
		if(order.Text && order.Text.results && order.Text.results.length>0)
		{
			var orderNotes = order.Text.results;
			for(var i= 0; i<orderNotes.length; i++)
			{
				text.text += (orderNotes[i].Tdline == "")? "\n" : orderNotes[i].Tdline;
			}
		}
		oModel.setData(order);
		textModel.setData(text);
		this.textDialog.setModel(oModel, "order");
		this.textDialog.setModel(textModel, "text");
		// this.textDialog.setModel(iModel, "i18n");
		this.textDialog.open();

	},
	onTextClose:function(evt)
	{
		this.textDialog.close();
	},

	onSelectionChange:function(evt)
	{

				// var list = evt.getSource();
				// var orderList = this.getView().getModel("orders").getData().results;
				// var selectedItems = list.getSelectedItems();

				// var selectedOrders =[];
				//
				// for(var i= 0; i< selectedItems.length; i++)
				// {
				// 	selectedOrders.push(selectedItems[i].getBindingContext("orders").getObject());
				// }
				// for(var i = 0; i<orderList.length; i++)
				// {
				// 	for(var j = 0; j< selectedOrders.length; j++)
				// 	{
				// 		var order = selectedOrders[j];
				// 		if(orderList[i] === order)
				// 		{
				// 			orderList.splice(i, 1);
				// 			orderList.unshift(order);
				// 			break;
				// 		}
				// 	}
				// }
				// this.getView().getModel("orders").setProperty("/results", orderList);
				//
				// var itemList = list.getAggregation("items");
				//
				// for(var i = 0; i< itemList.length; i++)
				// {
				// 	for(var j= 0; j< selectedOrders.length; j++)
				// 	{
				// 		var orderItem = selectedOrders[j];
				// 		if(itemList[i].getBindingContext("orders").getObject() == orderItem)
				// 		{
				// 			itemList[i].setSelected(true);
				// 			break;
				// 		}
				// 	}
				//
				// }
				// var page = this.getView().page;
				// page.scrollTo(0);
				//-----------------------------------------------------------------------

				//---------------------------------------------------------------------
				// var list = evt.getSource();
				// var clickedItem = evt.mParameters.listItem;
				// var selected = clickedItem.isSelected();
				// var clickedOrder = clickedItem.getBindingContext("orders").getObject()
				// var orderList = this.getView().getModel("orders").getData().results;
				//
				// for(var  i = 0 ; i< orderList.length; i++)
				// {
				// 	if(orderList[i] === clickedOrder )
				// 	{
				// 		orderList.splice(i, 1);
				// 		orderList.unshift(clickedOrder);
				// 		break;
				// 	}
				// }
				//
				//
				// this.getView().getModel("orders").setProperty("/results", orderList);
				// var itemList = list.getAggregation("items");
				//
				// for(var i = 0; i< itemList.length; i++)
				// {
				//
				// 		if(itemList[i].getBindingContext("orders").getObject() == clickedOrder)
				// 		{
				// 			itemList[i].setSelected(selected);
				// 			break;
				// 		}
				//
				// }
				//-------------------------------------------------------------------------------------

				// var list = evt.getSource();
				// var orderList = this.getView().getModel("orders").getData().results;
				// var selectedItem = evt.mParameters.listItem;
				// var order = selectedItem.getBindingContext("orders").getObject();
				// order.isSelected = selectedItem.isSelected();
				// var sorter = new sap.ui.model.Sorter('isSelected', true);
				// sorter.fnCompare = function(val1, val2)
				// {
				// 	val1  = (val1 === undefined)? false : val1;
				// 	val2  = (val2 === undefined)? false : val2;
				// 	if (val1>val2)
				// 		return 1;
				// 	else {
				// 		if(val1 === val2)
				// 			return 0;
				// 		else {
				// 			return -1;
				// 		}
				// 	}
				// }
				// list.getBinding("items").sort(sorter);


	},


});
