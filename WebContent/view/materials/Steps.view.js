sap.ui.jsview("view.materials.Steps", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf view.Materials.ServiceOrders
	 */
	getControllerName: function () {
		return "view.materials.Steps";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 * @memberOf view.Materials.ServiceOrders
	 */
	createContent: function (oController) {

		this.page = new sap.m.Page({
			customHeader: new sap.m.Bar({
				contentLeft: [
					new sap.m.Button({
						icon: "sap-icon://nav-back",
						press: [oController.onNavBackPress, oController]
					}),
					new sap.m.Button({
						icon: "sap-icon://home",
						press: [oController.onHomePress, oController]
					})],
				// contentMiddle: [new sap.m.Label({
				// 		text: "{i18n>HEADQUARTER}: {header>/Tplnr}"
				// 	})],

			}),
			footer: new sap.m.Bar({
                contentMiddle: [
                    new sap.m.Button({
                        text: "{i18n>SELECT_ALL}",
                        icon: "sap-icon://multi-select",
                        press: [oController.onSelectAllPress, oController]
                    })
                ],
				contentRight: [
					new sap.m.Button({
						text: "{i18n>FORWARD}",
						press: [oController.onForwardPress, oController],
					})],
				contentLeft:[
					new sap.m.Button({
						text: "{i18n>BACK}",
						press: [oController.onNavBackPress, oController],
					}),
				]
			})
		});

// 		this.list = new sap.m.List({
//
// 			//visible: {
// 			//    path: "ods>/Bemot",
// 			//    formatter: function(Bemot) {
// 			//        if (Bemot !== "01") {
// 			//            return true;
// 			//        } else return false;
// 			//    }
// 			//},
// 			headerDesign: "Plain",
// 			//			mode: "MultiSelect",
// 			columns: [new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>ODS_MATERIAL}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}), new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>ODS_MATERIAL_SERIAL}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}), new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>ODS_MATERIAL_DESCRIPTION}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}), new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>Q_ODS}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}),
// 								new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>Q_STOCK}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}), new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>Q_ISSUE}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}),
// 								new sap.m.Column({
// 					hAlign: "Center",
// 					vAlign: "Middle",
// 					minScreenWidth: "Tablet",
// 					demandPopin: true,
// 					popinDisplay: "Inline",
// 					header: new sap.m.Label({
// 						text: "{i18n>ISSUE}",
// 						design: sap.m.LabelDesign.Bold
// 					})
// 				}), ]
// 		});
//
// 		var template = new sap.m.ColumnListItem({
// 			cells: [new sap.m.Text({
// 					text: "{material>Matnr}"
// 				}), new sap.m.Text({
// 					text: "{material>Gernr}"
// 				}), new sap.m.Text({
// 					text: "{material>MatText}"
// 				}), new sap.m.Text({
// 					text: "{material>QtyReq}"
// 				}).addStyleClass("textQty"), new sap.m.Text({
// 					text: "{material>QtyStock}"
// 				}), new sap.m.Input({
// 					valueState: "{material>QtyIssue_state}",
// 					value: "{material>QtyIssue}",
// //					enabled: {
// //						path: "ods>/Bemot",
// //						formatter: function (Bemot) {
// //							if (Bemot !== "01") {
// //								return true;
// //							} else return false;
// //						}
// //					}
// 				}).addStyleClass("inputIssue"),
// 							new sap.m.CheckBox({
// 					selected: {
// 						path: "material>Status",
// 						formatter: function (status) {
// 							if (status === "") {
// 								return false;
// 							} else {
// 								return true;
// 							}
// 						}
// 					},
// 					select: [oController.onSelectPress, oController],
// //					enabled: {
// //						path: "ods>/Bemot",
// //						formatter: function (Bemot) {
// //							if (Bemot !== "01") {
// //								return true;
// //							} else return false;
// //						}
// //					}
// 				}),
// 						 ]
// 		});
//
// 		this.list.bindAggregation("items", {
// 			path: "material>/results",
// 			template: template,
// 		});
//
// 		this.page.addContent(this.list);
//
// 		this.listStock = new sap.m.List({
// 			headerDesign: "Plain",
// 			//			mode: "MultiSelect",
// 			columns: [new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>STOCK_MATERIAL}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>STOCK_MATERIAL_SERIAL}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>ODS_MATERIAL_DESCRIPTION}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>Q_ODS}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>Q_STOCK}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>Q_ISSUE}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), new sap.m.Column({
// 				hAlign: "Center",
// 				vAlign: "Middle",
// 				minScreenWidth: "Tablet",
// 				demandPopin: true,
// 				popinDisplay: "Inline",
// 				header: new sap.m.Label({
// 					text: "{i18n>ISSUE}",
// 					design: sap.m.LabelDesign.Bold
// 				})
// 			}), ],
// 		});
//
// 		var templateStock = new sap.m.ColumnListItem({
// 			cells: [new sap.m.Text({
// 				text: "{materialStock>Matnr}"
// 			}), new sap.m.Text({
// 				text: "{materialStock>Gernr}"
// 			}), new sap.m.Text({
// 				text: "{materialStock>MatText}",
// 				wrapping: true
// 			}), new sap.m.Text({
// 				text: "{materialStock>QtyReq}"
// 			}).addStyleClass("textQty"), new sap.m.Text({
// 				text: "{materialStock>QtyStock}"
// 			}), new sap.m.Input({
// 				valueState: "{materialStock>QtyIssue_state}",
// 				value: "{materialStock>QtyIssue}",
// 				enabled: {
// 					path: "ods>/Bemot",
// 					formatter: function (Bemot) {
// 						if (Bemot !== "01") {
// 							return true;
// 						} else return false;
// 					}
// 				}
// 			}).addStyleClass("inputIssue"), new sap.m.CheckBox({
// 				selected: {
// 					path: "materialStock>Status",
// 					formatter: function (status) {
// 						if (status === "") {
// 							return false;
// 						} else {
// 							return true;
// 						}
// 					}
// 				},
// 				select: [oController.onSelectStockPress, oController],
// 				enabled: {
// 					path: "ods>/Bemot",
// 					formatter: function (Bemot) {
// 						if (Bemot !== "01") {
// 							return true;
// 						} else return false;
// 					}
// 				}
// 			}), ]
// 		});
//
// 		this.listStock.bindAggregation("items", {
// 			path: "materialStock>/results",
// 			template: templateStock
// 		});
//
// 		this.page.addContent(this.listStock);
//
// 		this.listStock.addStyleClass("tableStock");
		// this.listStock.addStyle("tableStock");

		return this.page;
	}

});
