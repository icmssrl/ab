sap.ui.jsview("view.materials.StepMaterials", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf view.Materials.ServiceOrders
	 */
	getControllerName: function () {
		return "view.materials.StepMaterials";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 * @memberOf view.Materials.ServiceOrders
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({
			customHeader: new sap.m.Bar({
				contentLeft: [new sap.m.Button({
					icon: "sap-icon://nav-back",
					press: [oController.onNavBackPress, oController]
				}),
				new sap.m.Button({
					icon: "sap-icon://home",
					press: [oController.onHomePress, oController]
				})],
				// contentMiddle: [new sap.m.Label({
				// 	text: {
				// 		parts: [{
				// 			path: "ODS>/Aufnr"
				// 		}, {
				// 			path: "ODS>/Ktext"
				// 		}, ],
				// 		formatter: function (Aufnr, Ktext) {
				// 			return Aufnr + " " + Ktext;
				// 		}
				// 	}
				// })],
				contentMiddle: [
					new sap.m.Label({
					hAlign:"Left",
					text: " {ODS>/Ktext}"
				}).addStyleClass("nameText")],
				contentRight:
				[
					// new sap.m.Label({text: " {synchro>/dateTime}", visible:"{synchro>/en}"}).addStyleClass("synchroLabel"),
					new sap.m.Button({
						icon: "sap-icon://synchronize",
						press: [oController.onSynchronize, oController]
					})
				]
			}),
            subHeader:new sap.m.Toolbar({
				content:[
					new sap.m.SearchField({
					liveChange: [oController.onMaterialsSearch, oController],
				})]

		 }),

		});
		this.footerBar = new sap.m.Bar({
			/*contentMiddle: [
				new sap.m.Button({
					text: "{i18n>SELECT_ALL}",
					icon: "sap-icon://multi-select",
					press: [oController.onSelectAllPress, oController],
					enabled: {
						path: "ods>/Bemot",
						formatter: function (Bemot) {
							if (Bemot !== "07") {
								return true;
							} else return false;
						}
					}
				}),
				new sap.m.Button({
					text: "{i18n>SAVE}",
					icon: "sap-icon://save",
					press: [oController.onSavePress, oController],
					enabled: {
						path: "ods>/Bemot",
						formatter: function (Bemot) {
							if (Bemot !== "07") {
								return true;
							} else return false;
						}
					}
				}),
			],*/
			contentLeft:
			[
				new sap.m.Button({
					text: "{i18n>BACK}",
					press: [oController.onBackPress, oController],

				})
			]
		});
		this.page.setFooter(this.footerBar);
		this.list = new sap.m.List({

			//visible: {
			//    path: "ods>/Bemot",
			//    formatter: function(Bemot) {
			//        if (Bemot !== "01") {
			//            return true;
			//        } else return false;
			//    }
			//},
			// growing:true,
			// growingThreshold:10,
			// growingScrollToLoad:true,
			headerDesign: "Plain",
			//			mode: "MultiSelect",
			updateFinished: [oController.onUpdateFinished, oController],
			columns: [new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					width:"15%",
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>ODS_MATERIAL}",
						design: sap.m.LabelDesign.Bold
					})
				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					width:"15%",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>ODS_MATERIAL_SERIAL}",
						design: sap.m.LabelDesign.Bold
					})
				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					// width:"30%",
					// header: new sap.m.Label({
					// 	text: "{i18n>ODS_MATERIAL_DESCRIPTION}",
					// 	design: sap.m.LabelDesign.Bold
					// })
				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>Q_ODS}",
						design: sap.m.LabelDesign.Bold
					})
				}),
								new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>Q_STOCK}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>ISSUE}",
					design: sap.m.LabelDesign.Bold
				})
			}),
				new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>Q_ISSUE}",
						design: sap.m.LabelDesign.Bold
					})
				}),
								 ]
		});

		var template = new sap.m.ColumnListItem({
			cells: [new sap.m.Text({
					text: "{material>Matnr}"
				}).addStyleClass("matnrText"),
				new sap.m.Text({
					text: "{material>Gernr}"
				}).addStyleClass("serialText"),
				new sap.m.Text({
					text: "{material>MatText}"
				}).addStyleClass('descrText'),
				 new sap.m.Text({
					text: "{material>QtyReq}"
				}).addStyleClass("textQty"),
				new sap.m.Text({
					text: "{material>QtyStock}"
				}),
				// new sap.m.Switch(
				// {
				// 		state:{
				// 			parts:
				// 			[{
				// 					path: "material>Status"
				// 			},
				// 			{
				// 					path: "ods>/Bemot"
				// 			}],
				// 			formatter: function (status, bemot) {
				// 				if (status === "") {
				// 					if(bemot === "01")
				// 						return true;
				// 					else
				// 						return false;
				// 				} else {
				// 					return true;
				// 				}
				// 			}
				// 		},
				// 		enabled: {
				// 				path: "ods>/Bemot",
				// 				formatter: function (Bemot) {
				// 					if (Bemot !== "01") {
				// 						return true;
				// 					} else return false;
				// 				}
				// 		},
				//
				// }),check
				this.checkBox = new sap.m.CheckBox({
                    selected: {
				parts:
				[{
						path: "material>Status"
				},
				{
						path: "material>check"
				}],
				formatter: function (status, check) {
					if (status === "") {
						if(check === true)
							return true;
						else
							return false;
					} else {
						return true;
					}
				}
			},
//			selected: {
//				parts:
//				[{
//						path: "material>Status"
//				},
//				{
//						path: "ods>/Bemot"
//				}],
//				formatter: function (status, bemot) {
//					if (status === "") {
//						if(bemot === "01")
//							return true;
//						else
//							return false;
//					} else {
//						return true;
//					}
//				}
//			},
			select: [oController.onSelectPress, oController],
            enabled: {
//				path: "material>Status",
                parts:
				[{
						path: "material>Status"
				},
				{
						path: "material>toSync"
				},
				{
						path: "material>check"
				}],
				formatter: function (status, toSync, check) {
					if (status === "") {
                        return true;
					} else {
                        if (!toSync) {
//                            if (!check) {
//                                return true;
//                            } else {
                                return false;
//                            }
                        } else {
//                            if (check) {
//                                return true;
//                            } else {
                                return true;
//                            }
                        }
						
//                        return false;
                    }
				}
			}
//			enabled: {
//				path: "ods>/Bemot",
//				formatter: function (Bemot) {
//					if (Bemot !== "01") {
//						return true;
//					} else return false;
//				}
//			}
		}).addStyleClass("conferma"),
				 new sap.m.Input({
					 type:"Number",
					valueState: "{material>QtyIssue_state}",
					value:
					{
//                        parts:
//                        [{
//						  path: "material>QtyIssue"
//                        },
//                        {
//                          path: "material>Status"
//                        }],
						path:"material>QtyIssue",
						formatter:function(value, Status)
						{
							if(value == "0" || value == 0)
								return "";
							else {
//                                if(Status === "") {
//                                    value = 0
//                                }
								return value;
							}
                            
						}
					},

					liveChange:[oController.setValue, oController],
                     enabled: {
//				path: "material>Status",
                parts:
				[{
						path: "material>Status"
				},
				{
						path: "material>toSync"
				},
				{
						path: "material>check"
				}],
				formatter: function (status, toSync, check) {
//					if (status !== "") {
//                        return false;
//					} else {
                        if (check) {
//                        if (!toSync) {
                            if (!toSync) {
                                return false;
                            } else {
                                return true;
                            }
                        } else {
                            return false;
                        }
						
//                        return false;
//                    }
				}
			
//					enabled: {
//                        parts:
//                        [{
//                                path: "material>Status"
//                        },
//                         {
//                                path: "material>check"
//                        }],
//                        formatter: function (status, select) {
//                            if (status === "") {
//                                if(select)
//                                    return true;
//                                else
//                                    return false;
//                            } else {
////                                return true;
//                                return false;
//                            }
//                        }
//                        parts:
//                        [{
//                                path: "material>Status"
//                        },
//                        {
//                                path: "ods>/Bemot"
//                        },
//                         {
//                                path: "material>check"
//                        }],
//                        formatter: function (status, bemot, select) {
//                            if (status === "") {
//                                if(bemot === "01" && !select)
//                                    return true;
//                                else
//                                    return false;
//                            } else {
//                                return true;
//                            }
//                        }
//						parts: [{
//								path: "ODS>/Bemot"
//					 		}, {
//					 			path: "material>Status"
//					 		} ],
//						formatter: function (Bemot, status) {
//
//							if(status !== "A")
//								return false;
//
//							if (Bemot !== "01") {
//								return true;
//							} else return false;
//						}
					}
                     
				}).addStyleClass("inputIssue")

			]
		});

		this.list.bindAggregation("items", {
			path: "material>/results",
			template: template,
		});


		this.page.addContent(this.list);

		this.listStock = new sap.m.List({
			headerDesign: "Plain",
//			growing:true,
//			growingThreshold:10,
			// growingScrollToLoad:true,
			updateFinished: [oController.onUpdateFinished, oController],
			//			mode: "MultiSelect",
			columns: [new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				width:"15%",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>STOCK_MATERIAL}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				width:"15%",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>STOCK_MATERIAL_SERIAL}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				// width:"30%",
				popinDisplay: "Inline",
				// header: new sap.m.Label({
				// 	text: "{i18n>ODS_MATERIAL_DESCRIPTION}",
				// 	design: sap.m.LabelDesign.Bold
				// })
			}), new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>Q_ODS}",
					design: sap.m.LabelDesign.Bold
				})
			}), new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>Q_STOCK}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>ISSUE}",
					design: sap.m.LabelDesign.Bold
				})
			}),
			 new sap.m.Column({
				hAlign: "Left",
				vAlign: "Middle",
				minScreenWidth: "Tablet",
				demandPopin: true,
				popinDisplay: "Inline",
				header: new sap.m.Label({
					text: "{i18n>Q_ISSUE}",
					design: sap.m.LabelDesign.Bold
				})
			})
		]
		});

		var templateStock = new sap.m.ColumnListItem({
			cells: [new sap.m.Text({
				text: "{materialStock>Matnr}"
			}).addStyleClass('matnrText'),
			new sap.m.Text({
				text: "{materialStock>Gernr}"
			}).addStyleClass('serialText'),
			new sap.m.Text({
				text: "{materialStock>MatText}",
				wrapping: true
			}).addStyleClass('descrText'),
			 new sap.m.Text({
				text: "{materialStock>QtyReq}"
			}).addStyleClass("textQty"), new sap.m.Text({
				text: "{materialStock>QtyStock}"
			}),
			new sap.m.CheckBox({
				selected: {
					path: "materialStock>Status",
					formatter: function (status) {
						if (status === "") {
								return false;
						} else {
							return true;
						}
					}
				},
				select: [oController.onSelectStockPress, oController],
				// enabled: {
				// 	path: "ods>/Bemot",
				// 	formatter: function (Bemot) {
				// 		if (Bemot !== "01") {
				// 			return true;
				// 		} else return false;
				// 	}
				// }
			}).addStyleClass("conferma"),
			new sap.m.Input({
				type:"Number",
				valueState: "{materialStock>QtyIssue_state}",

				value:
				{
						path:"materialStock>QtyIssue",
						formatter:function(value)
						{
							if(value == "0" || value == 0)
								return "";
							else {
								return value;
							}
						}
				},
				liveChange:[oController.setValue, oController],
				enabled: {

					parts: [{
							path: "ODS>/Bemot"
				 		}, {
				 			path: "materialStock>Status"
				 		}, ],
					formatter: function (Bemot, status) {

						if(status !== "A")
							return false;

						if (Bemot !== "07") {
							return true;
						} else return false;
					}
				}
			}).addStyleClass("inputIssue"), ]
		});

		this.listStock.bindAggregation("items", {
			path: "materialStock>/results",
			template: templateStock
		});

		this.page.addContent(this.listStock);

		this.listStock.addStyleClass("tableStock");
		// this.listStock.addStyle("tableStock");

		return this.page;
	}

});
