sap.ui.jsview("view.materials.ClientOrders", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf view.Materials.ServiceOrders
	 */
	getControllerName: function () {
		return "view.materials.ClientOrders";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 * @memberOf view.Materials.ServiceOrders
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({
			id:"clientPageId",
			customHeader: new sap.m.Bar({
				contentLeft: [
					new sap.m.Button({
						icon: "sap-icon://home",
						press: [oController.onHomePress, oController]
					}),
					new sap.m.Button({
						icon: "sap-icon://alphabetical-order",
						press: [oController.onSort, oController]
					})
				],
				contentMiddle: [new sap.m.Label({
					text: "{user>/ESname}"
				})],
				contentRight: [
					new sap.m.Label({text: "{synchro>/dateTime}", visible:"{synchro>/en}"}).addStyleClass("synchroLabel"),
					new sap.m.Button({
						icon: "sap-icon://synchronize",
						press: [oController.onSynchronize, oController]
					}),
				]
			}),
			subHeader:new sap.m.Toolbar({
				content:[
					this.search = new sap.m.SearchField({
					liveChange: [oController.onClientsSearch, oController],
				})]

		 }),
		// 	footer: new sap.m.Bar({
		// 		contentMiddle: [
		// 		new sap.m.Button({
		// 				enabled: "{enable>/steps}",
		// 				text: "{i18n>STEPS}",
		// 				icon: "sap-icon://step",
		// 				press: [oController.onStepsPress, oController]
		// 			}),
		// 		new sap.m.Button({
		// 				enabled: "{enable>/close}",
		// 				text: "{i18n>CLOSE}",
		// 				icon: "sap-icon://sys-cancel",
		// 				press: [oController.onClosePress, oController]
		// 			}),
		// 		new sap.m.Button({
		// 				//enabled: "{enable>/detail}",
		// 				text: "{i18n>DETAIL}",
		// 				icon: "sap-icon://action",
		// 				press: [oController.onMaterialPress, oController]
		// 			}),
		// 		]
		// 	})
		});
//---------------------------------------------------------------
		// this.search = new sap.m.SearchField({
		// 	liveChange: [oController.onClientsSearch, oController],
		// });
		//
		// this.page.addContent(this.search);
//--------------------------------------------------------------
		this.list = new sap.m.List({
			// selectionChange: [oController.onSelectionChange, oController],
			// id:"clientListId",
			headerDesign: "Plain",
			growing:true,
			growingThreshold:20,
			growingScrollToLoad:true,
			// mode: "SingleSelectMaster",
			columns: [new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>CLIENT}",
						design: sap.m.LabelDesign.Bold
					})
				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>HEADQUARTER}",
						design: sap.m.LabelDesign.Bold
					})
				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay: "Inline",
					header: new sap.m.Label({
						text: "{i18n>DESCRIPTION}",
						design: sap.m.LabelDesign.Bold
					})
				})
				// , new sap.m.Column({
				// 	hAlign: "Center",
				// 	vAlign: "Middle",
				// 	minScreenWidth: "Tablet",
				// 	demandPopin: true,
				// 	popinDisplay: "Inline",
				// 	header: new sap.m.Label({
				// 		text: "{i18n>CLIENT}",
				// 		design: sap.m.LabelDesign.Bold
				// 	})
				// }),
			// new sap.m.Column({
			// 	hAlign : "Center",
			// 	vAlign : "Middle",
			// 	header : new sap.m.Label({
			// 		design : sap.m.LabelDesign.Bold
			// 	})
			// }),new sap.m.Column({
			// 	hAlign : "Center",
			// 	vAlign : "Middle",
			// 	header : new sap.m.Label({
			// 		design : sap.m.LabelDesign.Bold
			// 	})
			// }),
			]
		});

		var template = new sap.m.ColumnListItem({
			type: "Active",

			press: [oController.onItemPress, oController],
			cells: [new sap.m.Text({
					// text: "{orders>Aufnr}"
					text:{path:'clients>Name1', formatter:util.Formatter.formatClientName}
				}), new sap.m.Text({
					text: "{clients>Tplnr}"
				}), new sap.m.Text({
					text: "{clients>Pltxt}"
				}),
			// new sap.m.Button({
			// 	type: "Transparent",
			// 	icon : "sap-icon://sys-cancel",
			// 	press: [oController.onClosePress, oController]
			// }),
			// new sap.m.Button({
				// visible: {
				// 	path: "orders>Bemot",
				// 	formatter: function (Bemot){
				// 		if (Bemot == "03")
				// 		{
				// 			return true;
				// 		}
				// 		else return false;
				// 	}

				// },
				// enable: {
				// 	path: "orders>/Materials",
				// 	formatter: function(materials)
				// 	{
				// 		console.log(materials);
				// 	}

				// },
			// 	type: "Transparent",
			// 	icon : "sap-icon://action",
			// 	press: [oController.onMaterialPress, oController]

			// }),
			]
		});

		this.list.bindAggregation("items", {
			path: "clients>/results",
			template: template
		});

		this.page.addContent(this.list);

		return this.page;
	}

});
