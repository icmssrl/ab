sap.ui.jsview("view.materials.ServiceOrders", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf view.Materials.ServiceOrders
	 */
	getControllerName: function () {
		return "view.materials.ServiceOrders";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 * @memberOf view.Materials.ServiceOrders
	 */
	createContent: function (oController) {
		this.page = new sap.m.Page({


			customHeader: new sap.m.Bar({
				contentLeft: [
					new sap.m.Button({
						icon: "sap-icon://nav-back",
						press: [oController.onNavBackPress, oController]
					}),
					new sap.m.Button({
						icon: "sap-icon://home",
						press: [oController.onHomePress, oController]
					}),
					new sap.m.Button({
						icon: "sap-icon://alphabetical-order",
						press: [oController.onSort, oController]
					})
				],
				contentMiddle: [new sap.m.Label({
					text: "{header>/Tplnr}"
				})],
				contentRight: [
					new sap.m.Button({
						icon: "sap-icon://synchronize",
						press: [oController.onSynchronize, oController]
					}),
				]
			}),
			//-------------------------------------
			subHeader:new sap.m.Toolbar({
				content:[
 		 		 	this.ordersSearch = new sap.m.SearchField({
 		 		 	liveChange: [oController.onOrdersSearch, oController],
				})]

		 }),


			footer: new sap.m.Bar({
				contentMiddle: [
				new sap.m.Button({

						text: "{i18n>RUNORDER}",
						icon: "sap-icon://begin",
						press: [oController.onElaboratePress, oController]
					})

				]
			})
		});
//--------------------------commmented to block SearchField--------
		// this.search = new sap.m.SearchField({
		// 	liveChange: [oController.onOrdersSearch, oController],
		// });
		//
		// this.page.addContent(this.search);
// ----------------------New Part-------------------------





//----------------------------------------------------
		this.list = new sap.m.List({

			selectionChange: [oController.onSelectionChange, oController],

			headerDesign:"Plain",
			includeItemInSelection :true,

			mode: "MultiSelect",

			growing:true,
			growingThreshold:20,

			columns: [
				new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay:"Inline",
					header: new sap.m.Label({
						text: "{i18n>ORDER}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay:"Inline",

					header: new sap.m.Label({
						text: "{i18n>DATE}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay:"Inline",

				}), new sap.m.Column({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay:"Inline",

					header: new sap.m.Label({
						text: "{i18n>CLIENT}",
						design: sap.m.LabelDesign.Bold
					})
				}),
				new sap.m.Column
				({
					hAlign: "Left",
					vAlign: "Middle",
					minScreenWidth: "Tablet",
					demandPopin: true,
					popinDisplay:"Inline",

					header: new sap.m.Label({
						text: "{i18n>NOTES}",
						design: sap.m.LabelDesign.Bold
					})

				})

			]
		});


		var template = new sap.m.ColumnListItem({

			type:"Active",

			visible:
			{
				path:"orders>",
				formatter:oController.checkMaterialStatus
			},

			cells: [
				new sap.m.Text({
					text: "{orders>Aufnr}"
				}).addStyleClass("orderStyle"),
				new sap.m.Text({
					text: {
						path: "orders>Gstrp",
						formatter: util.Formatter.date
					}
				}).addStyleClass("dateStyle"),
				new sap.m.Text({
					text: "{orders>Ktext}"
				}),
				 new sap.m.Text({
					text: "{orders>Name1}"
				}),

				new sap.ui.core.Icon({
					src: "sap-icon://notes",
					press: [oController.onDetailPress, oController],
					color:"blue",
					size:"1.5em",
					visible:
					{
						path:"orders>",
						formatter: function(order)
						{
							if(order.Text.results.length>0)
								return true;
							else
								return false;
						}
					}
				}),

			]
		}).addStyleClass("noCheckbox");

		this.list.bindAggregation("items", {
			path: "orders>/results",
			template: template
		});

		this.page.addContent(this.list);

		return this.page;
	}

});
