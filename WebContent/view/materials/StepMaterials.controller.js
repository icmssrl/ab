jQuery.sap.require("model.ODS");
jQuery.sap.require("model.Timesheet");
jQuery.sap.require("model.Fiori");
jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");

jQuery.sap.require("model.Material");


jQuery.sap.require("util.Error");
jQuery.sap.require("util.Formatter");
jQuery.sap.require("util.Common");
jQuery.sap.require("model.ActiveOrders");
jQuery.sap.require("sap.m.MessageBox");

sap.ui.controller("view.materials.StepMaterials", {

    /**
     * Called when a controller is instantiated and its View controls (if available) are already created.
     * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
     * @memberOf view.Materials.ServiceOrders
     */
    onInit: function () {
        this.view = this.getView();
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this._handleRouteMatched, this);
        util.Common.setFunctionsContext(this);

        var bus = sap.ui.getCore().getEventBus();
        bus.subscribe("synchronize", "complete", this.onSynchronizeComplete, this);

    },

    _handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        this.name = name;

        if ("materials.StepMaterials" !== name) {
            return;
        }

        this.setCssForLandscape();
        sap.ui.Device.orientation.attachHandler(this.setCssForLandscape);

        this.headquarter = evt.getParameters().arguments.headquarter;
        this.name = evt.getParameters().arguments.name;
        //Probably to check if it's an int
        this.numOrder = parseInt(evt.getParameter("arguments").numOrder);
        var nextButton = undefined;
        this.getView().footerBar.removeAllContentRight();
        if (this.numOrder < model.ActiveOrders.getOrders().length - 1) {
            nextButton = new sap.m.Button({
                text: "{i18n>NEXT}",
                press: [this.onNextPress, this]
            });

        } else {
            nextButton = new sap.m.Button({
                text: "{i18n>CLOSE}",
                icon: "sap-icon://sys-cancel",
                press: [this.onClosePress, this]
            });
        }

        this.getView().footerBar.addContentRight(nextButton);
        this.ods = model.ActiveOrders.getOrder(this.numOrder).value.Aufnr;

        this.ordersModel = model.ODS.getModel();
        this.view.setModel(this.ordersModel, "orders");


        this.iModel = model.i18n.getModel();

        //Imposto il modello del materiale
        this.view.setModel(model.ODS.getODS(this.ods), "ods");
        this.view.setModel(model.ODS.getMaterialsInODS(this.ods), "material");
        
        /* Fabio 19/10/2016 non serve più*/ 
        /* Fabio 14/04/2016 */
//        if (this.view.getModel("material").getData().results.length !== 0) {
//            for (var i = 0; i < this.view.getModel("material").getData().results.length; i++) {
//                if (this.view.getModel("material").getData().results[i].Bemot === "01") {
//                    if (!this.view.getModel("material").getData().results[i].check) {
//                        this.view.getModel("material").getData().results[i].check = false;
//                    }
//                }
//            }
//        }
        /* Fabio 14/04/2016 */
        /* Fabio 19/10/2016 */
        
        this.view.setModel(model.ODS.getMaterialsInStock(this.ods), "materialStock");
        this.view.setModel(model.ODS.getODS(this.ods), "ODS");
        this.view.setModel(model.User.getModel(), "user");

        this.synchroModel = new sap.ui.model.json.JSONModel({
            "dateTime": model.Synchronize.getDateTime(),
            "en": model.Synchronize.isSynchronized()
        });
        this.view.setModel(this.synchroModel, "synchro");
        //---------------

        /* Fabio 11/04/2016 */
        var lang = sap.ui.getCore().getConfiguration().getLanguage();
        if (lang !== "it" && lang !== "it-IT") {
            var patternData = "MM-dd-yyyy";
        } else {
            var patternData = "dd-MM-yyyy";
        }
        var enabled = {
            lang: patternData,
            select: true
        };
        this.enabledModelStep = new sap.ui.model.json.JSONModel(enabled);
        this.getView().setModel(this.enabledModelStep, "enabledFG");

        //        var mat = model.ActiveOrders.getOrders()[0].value.Materials.results;
        //        for(var i=0; i<mat.length;i++){
        //            if(mat[i].Status === "") {
        //                mat[i].QtyIssue = 0
        //            }
        //        }
        /* Fabio 11/04/2016 */
    },

    onUpdateFinished: function () {
        this.setCssForLandscape(undefined, 0);
    },

    setCssForLandscape: function (e, time) {

        if (time !== 0) {
            if (!time) time = 500;
            util.Busy.show();
        }

        var ls;
        if (e) {
            ls = e.landscape;
        } else {
            ls = window.innerHeight < window.innerWidth;
        }

        if (!ls) {
            setTimeout(function () {

                // Conferma
                var input = jQuery(".conferma").parent();
                var row = input.parent();
                input.css('width', '40%');
                row.css('float', 'left');
                row.css('width', '50%');
                row.css('margin-right', '10px');

                var t = row.children().find("label").parent();
                t.css('margin-top', '5px');

                // InputIssue
                var input = jQuery(".inputIssue").parent();
                var row = input.parent();
                input.css('width', '40%');
                //				row.css('float', 'left');
                row.css('width', '45%');

                var t = row.children().find("label").parent();
                t.css('margin-top', '5px');

                // Quantità
                var text = jQuery(".textQty").parent();
                var textRow = text.parent();
                textRow.css('float', 'left');
                textRow.css('width', '50%');
                textRow.css('margin-right', '10px');

                //Material
                var matnr = jQuery(".matnrText").parent();
                jQuery(".matnrText").css('width', '100%');
                matnr.css('width', '60%');
                var matnrRow = matnr.parent();
                matnrRow.css('float', 'left');
                matnrRow.css('width', '50%');
                matnrRow.css('margin', '0%');
                //
                // //Serial

                var serial = jQuery(".serialText").parent();
                var serialRow = serial.parent();
                // dateRow.css('float', 'left');
                serialRow.css('width', '50%');
                serialRow.css('margin', '0%');

                var descr = jQuery(".descrText").parent();
                descr.css('width', '100%');
                var descrRow = descr.parent();
                descrRow.css('clear', 'left');
                // descrRow.css('margin-top', '1em');
                descrRow.css('padding-top', '0.5em');
                // descrRow.css('padding-bottom', '0.5em');
                // descrRow.css('margin-top', '0.5em');
                // descrRow.css('margin-bottom', '0.5em');
                // var serial = jQuery(".serialText").parent();
                // var serialRow = serial.parent();



                //----------MultiDialog----------

                var multi = jQuery(".multiText").parent();
                var multiRow = multi.parent();
                multiRow.css('width', '60%');



                util.Busy.hide();
            }, time);
        } else {
            util.Busy.hide();
        }

    },

    userModelSuccess: function (oModel) {
        this.view.setModel(oModel, "user");
    },

    ODSSuccess: function (oModel) {
        this.view.setModel(oModel, "orders");
    },

    onSavePress: function () {
        var onSave = jQuery.proxy(this.onSave, this);
        setTimeout(onSave, 300);
    },

    onSave: function () {

        var items = [];

        var materials = this.view.getModel("material").oData.results;
        /* Fabio 11/04/2016 */
        var stock = this.view.getModel("materialStock").oData.results;
        //        var selectedItems = model.ActiveOrders.getOrders();
        //		var stock = selectedItems[0].value.Materials.results;
        /* Fabio 11/04/2016 */

        var errorFound = false;

        //Carico i materiali dell'ordine
        for (var i = 0; i < materials.length; i++) {
            var mt = materials[i];

            // Check sulle quantità inserite
            var qi = parseFloat(mt.QtyIssue);
            var qr = parseFloat(mt.QtyStock);

            if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
                mt.QtyIssue_state = "Error";
                errorFound = true;
            } else {
                mt.QtyIssue = qi.toString();
                mt.QtyStock = qr.toString();
                mt.QtyIssue_state = "None";
            }

            items.push(materials[i]);
        }

        //Carico i materiali che sono solo in stock
        for (i = 0; i < stock.length; i++) {

            var mt = stock[i];

            // Check sulle quantità inserite
            var qi = parseInt(mt.QtyIssue);
            var qr = parseInt(mt.QtyStock);

            if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
                mt.QtyIssue_state = "Error";
                errorFound = true;
            } else {
                mt.QtyIssue = qi.toString();
                mt.QtyStock = qr.toString();
                mt.QtyIssue_state = "None";
            }

            items.push(stock[i]);
        }

        if (items.length !== 0) {
//            var temp = _.find(items, {
//                'Status': "A"
//            });
//            if(!temp){
//                items[0].Status = "A";
//            }
            /* 19/10/2016 */
            for (i= 0; i< items.length; i++) {
                if (items[i].check) {
                    items[i].Status = "A";
                }
            }
            /* 19/10/2016 */
        }

        if (errorFound) {
            this.getView().getModel("material").refresh();
            this.getView().getModel("materialStock").refresh();
            sap.m.MessageBox.alert(this.iModel.getProperty("CHECK_QUANTITY"));
            return;
        }

        var odsModel = model.ODS.getModel();
        var ods = odsModel.oData;

        for (var i = 0; i < ods.results.length; i++) {
            var odst = ods.results[i];
            if (odst.Aufnr == this.ods) {
                odst.Materials.results = items;
            }
        }
        odsModel.setData(ods);
        model.ODS.saveModel();

        this.getView().getModel("material").refresh();
        this.getView().getModel("materialStock").refresh();

        sap.m.MessageBox.alert(this.iModel.getProperty("SAVED"));
    },

    onActivatePress: function () {
        var oModel = this.view.getModel("orders");
        var selectedContexts = this.view.list.getSelectedContexts();
        var selectedItems = [];

        for (var i = 0; i < selectedContexts.length; i++) {
            var item = oModel.getProperty(selectedContexts[i].getPath());
            selectedItems.push(item);
        }
        model.ODS.setSelectedItems(selectedItems);
    },

    onNavBackPress: function () {
        sap.ui.Device.orientation.detachHandler(this.setCssForLandscape);
        this.onBackPress();
    },

    onSelectPress: function (evt) {
        selected = evt.getParameter("selected");
        var mModel = this.view.getModel("material");
        var path = evt.getSource().getBindingContext("material").getPath();
        if (selected === true) {
            mModel.setProperty(path + "/Status", "A");
            mModel.setProperty(path + "/toSync", true);
            mModel.setProperty(path + "/check", true);
        } else {
            mModel.setProperty(path + "/check", false);
            mModel.setProperty(path + "/toSync", false);
//            mModel.setProperty(path + "/QtyIssue", 0);
            mModel.setProperty(path + "/QtyIssue", parseInt(mModel.getProperty(path + "/QtyReq")) < parseInt(mModel.getProperty(path + "/QtyStock")) ? mModel.getProperty(path + "/QtyReq") : mModel.getProperty(path + "/QtyStock"));
            mModel.setProperty(path + "/Status", "");
        }
        this.setCssForLandscape(undefined, 0);
    },

    onSelectStockPress: function (evt) {

        selected = evt.getParameter("selected");
        var mModel = this.view.getModel("materialStock");

        var path = evt.getSource().getBindingContext("materialStock").getPath();
        if (selected === true) {
            mModel.setProperty(path + "/Status", "A");
            mModel.setProperty(path + "/check", true);
        } else {
            if (mModel.getProperty(path + "/Status") == "A") {
                mModel.setProperty(path + "/QtyIssue", 0);
                mModel.setProperty(path + "/check", false);
            }
            mModel.setProperty(path + "/Status", "");
        }

        this.setCssForLandscape(undefined, 0);
    },

    onSelectAllPress: function (evt) {
        var model = this.getView().getModel("material");
        var materials = model.getData().results;

        for (var i = 0; i < materials.length; i++) {
            materials[i].Status = "A";
        }

        model.refresh();
    },
    onBackPress: function (evt) {
        if (this.numOrder > 0) {
            this.router.navTo("materials.StepMaterials", {
                "headquarter": this.headquarter,
                "name": this.name,
                "numOrder": this.numOrder - 1
            });
        } else {
            this.router.navTo("materials.Steps", {
                "headquarter": this.headquarter,
                "name": this.name
            });
        }
    },
    onNextPress: function (evt) {
        if (!this.checkMaterialQuantity())
            return;
        this.router.navTo("materials.StepMaterials", {
            "headquarter": this.headquarter,
            "name": this.name,
            "numOrder": this.numOrder + 1
        });
    },

    onClosePress: function (evt) {

        if (!this.checkMaterialQuantity())
            return;

        var selectedItems = model.ActiveOrders.getOrders();

        if (selectedItems === false || selectedItems.length == 0) return;

        // if(!this.checkZPoints(selectedItems))
        // {
        // 		sap.m.MessageBox.alert(model.i18n.getModel().getProperty("FAILED_CLOSE_DIFFERENT_ZPOINT"));
        // 		return;
        // }

        var iModel = model.i18n.getModel();

        if (selectedItems.length == 1) {
            // Creo il modello relativo all'ordine
            // var path = selectedItems[0].getBindingContext("orders").getPath();
            //
            // var mModel = this.view.getModel("orders");
            var order = selectedItems[0].value;

            // Imposto il campo per la data di chiusura
            order.closeDate = undefined;
            var oModel = new sap.ui.model.json.JSONModel(order);

            var tmpDate = new Date(order.Idate);
            tmpDate.setMinutes(tmpDate.getTimezoneOffset())
            var year = tmpDate.getFullYear().toString();
            // In sap è stato impostato che se la data è 2999 non richiede la lettura delle ore motore
            if (year !== "2999") {
                // Apro il Dialog per la chiusura dell'ordine
                this.closeDialog = sap.ui.jsfragment("view.fragment.CloseDialog", this);
                this.getView().addDependent(this.closeDialog);
                this.closeDialog.setModel(oModel, "order");
                this.closeDialog.setModel(iModel, "i18n");
                this.closeDialog.open();
            } else {
                var onClose = function (oAction) {
                    if (oAction === "OK") {
                        // FIXME Imposto una data e ora valida perchè 2999 non viene accettata
                        order.Idate = new Date(order.Idate.toString());
                        order.Itime = util.Formatter.currentTime();
                        this.closeOrder();
                    }
                };
                onClose = jQuery.proxy(onClose, this);

                sap.m.MessageBox.confirm(model.i18n.getModel().getProperty("CONFIRM_CLOSE"), {
                    title: "Confirm", // default
                    onClose: onClose // default
                });
            }
        } else {
            // Creo il modello relativo all'ordine
            // var paths =[];
            var mModel = this.view.getModel("orders");
            var orders = [];
            var multiConfirmObj = {
                "results": []
            };
            for (var i = 0; i < selectedItems.length; i++) {
                // Creo il modello relativo all'ordine
                // paths.push(selectedItems[i].getBindingContext("orders").getPath());
                // var order = mModel.getProperty(paths[i]);
                var order = selectedItems[i].value;
                //If the order is already closed, it won't close again
                if (this.checkMaterialStatus(order)) {
                    orders.push(order);
                }
            }
            multiConfirmObj.results = orders;
            for (var j = 0; j < orders.length; j++) {
                var tmpDate = new Date(orders[j].Idate);
                var year = tmpDate.getFullYear().toString();
                if (year !== "2999") {
                    //Manage order with required engine reading
                    if (!multiConfirmObj.reads) {
                        multiConfirmObj.reads = [];
                    }
                    multiConfirmObj.reads.push(orders[j]);
                    if (!multiConfirmObj.lastReadDate ||
                        ((multiConfirmObj.lastReadDate < orders[j].Idate) ||
                            ((multiConfirmObj.lastReadDate == orders[j].Idate) && ((multiConfirmObj.lastReadTime) && (multiConfirmObj.lastReadTime < orders[j].Itime))))) {
                        multiConfirmObj.lastReadDate = orders[j].Idate;
                        multiConfirmObj.lastReadTime = orders[j].Itime;
                        multiConfirmObj.lastCounter = orders[j].Recdv;
                    }
                }
            }
            //if orders requiring engine reading exist
            if (multiConfirmObj.reads && multiConfirmObj.reads.length > 0) {
                var multiModel = new sap.ui.model.json.JSONModel(multiConfirmObj);
                this.multiCloseDialog = sap.ui.jsfragment("view.fragment.MultiCloseDialog", this);
                this.getView().addDependent(this.multiCloseDialog);
                this.multiCloseDialog.setModel(multiModel, "multi");
                this.multiCloseDialog.setModel(iModel, "i18n");
                this.multiCloseDialog.open();
            } else {
                var onClose = function (oAction) {
                    if (oAction === "OK") {

                        // FIXME Imposto una data e ora valida perchè 2999 non viene accettata
                        for (var l = 0; l < multiConfirmObj.results.length; i++) {
                            multiConfirmObj.results[l].Idate = new Date(multiConfirmObj.results[l].Idate.toString());
                            multiConfirmObj.results[l].Itime = util.Formatter.currentTime();
                        }
                        this.closeOrder();
                    }
                };
                onClose = jQuery.proxy(onClose, this);

                sap.m.MessageBox.confirm(model.i18n.getModel().getProperty("CONFIRM_CLOSE"), {
                    title: "Confirm", // default
                    onClose: onClose // default
                });
            }


        }



    },

    closeConfirmMulti: function () {

        var selectedItems = model.ActiveOrders.getOrders();
        var paths = [];
        var items = [];
        var orderModel = this.view.getModel("orders");
        for (var i = 0; i < selectedItems.length; i++) {
            // paths.push(selectedItems[i].getBindingContext("orders").getPath());
            items.push(selectedItems[i]);
        }


        var selectedOrderModel = this.multiCloseDialog.getModel("multi");
        // Se non ho inserito un contatore allora non esco
        if (!selectedOrderModel.getData().actualrecdv) {
            selectedOrderModel.setProperty("/actualrecdv_state", "Error");
            return;
        }

        if (!selectedOrderModel.getData().closeDate) {
            selectedOrderModel.setProperty("/closeDate_state", "Error");
            return;
        }
        if (!this.checkDateValue(selectedOrderModel))
            return;

        this.multiCloseDialog.close();
        this.multiCloseDialog.destroy();
        for (var j = 0; j < selectedItems.length; j++) {
            var tmpDate = new Date(selectedItems[j].value.Idate);
            var year = tmpDate.getFullYear().toString();
            if (year !== "2999") {
                orderModel.setProperty(selectedItems[j].path + "/Recdv", selectedOrderModel.getData().actualrecdv);
            }
            orderModel.setProperty(selectedItems[j].path + "/Idate", selectedOrderModel.getData().closeDate);
            orderModel.setProperty(selectedItems[j].path + "/Itime", util.Formatter.currentTime());

        }

        orderModel.refresh();

        this.closeOrder();
    },

    closeConfirm: function () {

        var selectedItem = model.ActiveOrders.getOrders()[0];
        var path = selectedItem.path;
        var orderModel = this.view.getModel("orders");
        var item = orderModel.getProperty(path);

        var selectedOrderModel = this.closeDialog.getModel("order");
        // Se non ho inserito un contatore allora non esco
        if (!item.actualrecdv) {
            selectedOrderModel.setProperty("/actualrecdv_state", "Error");
            return;
        }

        if (!item.closeDate) {
            selectedOrderModel.setProperty("/closeDate_state", "Error");
            return;
        }

        if (!this.checkDateValue(selectedOrderModel))
            return;

        this.closeDialog.close();
        this.closeDialog.destroy();
        item.Recdv = item.actualrecdv;
        // Quando confermo la chiusura ripasso il dato al campo dell'odata.
        item.Idate = new Date(item.closeDate);
        item.Itime = util.Formatter.currentTime();
        orderModel.refresh();

        this.closeOrder();
    },
    closeOrder: function () {
        var selectedItems = model.ActiveOrders.getOrders();

        for (var i = 0; i < selectedItems.length; i++) {

            var obj = this.view.getModel("orders").getProperty(selectedItems[i].path);
            if (this.checkMaterialStatus(obj)) {
                // var path = selectedItems[i].getBindingContext("orders").getPath();
                var orderModel = this.view.getModel("orders");
                var item = orderModel.getProperty(selectedItems[i].path);

                var materials = item.Materials.results;
                for (var j = 0; j < materials.length; j++) {
                    var material = materials[j];

                    // Se ho un ordine che non deve visualizzare i materiali
                    if (item.Bemot === "07") {
                        if (material.Aufnr !== "") {
                            material.Status = "B";
                        }
                    }
                    // Se ho un ordine che visualizza i materiali
                    else {
                        // Se ho un materiale dell'ordine
                        if (!material.check) {
                            material.QtyIssue = "0.000";
                        }
                        if (material.Aufnr !== "") {
                            // Se non è fleggato lo segno come da scaricare ma imposto la quantità a 0
                            if (material.Status === "") {
                                material.Status = "B";
//                                if (material.Bemot !== "01")
//                                    material.QtyIssue = "0.000";
                                
                            } else
                            // Se è stato fleggato lo passo normale
                            {
                                material.Status = "B";
                            }
                        } else
                        // Se ho un materiale dello stock
                        {
                            // Se è stato fleggato lo passo
                            if (material.Status === "A") {
                                material.Status = "B";
                            }
                        }
                    }

                    // if (item.Bemot !== "01") {
                    // 	if (material.Status === "A") {
                    // 		material.Status = "B";
                    // 	}
                    // } else {
                    // 	if (material.Aufnr !== "") {
                    // 		material.Status = "B";
                    // 	}
                    // }
                }
                orderModel.setProperty(selectedItems[i].path, item);
            }
        }
        model.ODS.model = orderModel;
        model.ODS.saveModel();

        var iModel = model.i18n.getModel();
        // this.getView().getModel("enable").setProperty("/close", false);
        // this.getView().getModel("enable").setProperty("/steps", false);

        sap.m.MessageBox.alert(iModel.getProperty("ODS_CLOSED"), {
            title: "Alert",
            onClose: jQuery.proxy(function () {
                this.ordersModel.refresh();
                this.router.navTo("materials.ServiceOrders", {
                    "headquarter": this.headquarter,
                    "name": this.name
                });
                //                        this.onSynchronize();
            }, this)
        });
        // sap.m.MessageBox.alert(iModel.getProperty("ODS_CLOSED"));
        // this.ordersModel.refresh();
        // this.router.navTo("materials.ServiceOrders", {"headquarter":this.headquarter, "name":this.name});



    },

    multiCloseCancel: function () {
        this.multiCloseDialog.close();
        this.multiCloseDialog.destroy();
        this.setCssForLandscape();
    },

    closeCancel: function () {
        this.closeDialog.close();
        this.closeDialog.destroy();
        this.setCssForLandscape();
    },

    checkMaterialStatus: function (obj) {
        var materials = obj.Materials.results;
        var close = true;
        for (var k = 0; k < materials.length; k++) {
            var status = materials[k].Status;
            if (status === "B") {
                close = false;
            }
        }
        return close;

    },


    onContDateChange: function (evt) {
        var model = this.closeDialog.getModel("order");
        var iModel = this.getView().getModel("i18n");
        var value = util.Formatter.dateShortToDateISO(evt.getParameters().newValue);
        var valueDate = new Date(value);
        var now = new Date();
        if (valueDate > now) {
            sap.m.MessageBox.alert(iModel.getProperty("INVALID_READING_DATE"));
            model.setProperty("/closeDate_state", "Error");
            model.setProperty("/closeDate", "");
            evt.getSource().setValue();
        } else {
            model.setProperty("/closeDate", value);
            // model.setProperty("/closeDate_state", "None");
            model.setProperty("/closeDate_state", this.checkDateValue(model) ? "None" : "Error");
            model.refresh();
        }

        // var value = util.Formatter.dateShortToDateISO(evt.getParameters().newValue);
        // model.setProperty("/closeDate", value);
    },
    onMultiContDateChange: function (evt) {
        var model = this.multiCloseDialog.getModel("multi");
        var iModel = this.getView().getModel("i18n");
        var value = util.Formatter.dateShortToDateISO(evt.getParameters().newValue);
        var valueDate = new Date(value);
        var now = new Date();
        if (valueDate > now) {
            sap.m.MessageBox.alert(iModel.getProperty("INVALID_READING_DATE"));
            model.setProperty("/closeDate_state", "Error");
            model.setProperty("/closeDate", "");
            evt.getSource().setValue();
        } else {
            model.setProperty("/closeDate", value);
            // model.setProperty("/closeDate_state", "None");
            model.setProperty("/closeDate_state", this.checkDateValue(model) ? "None" : "Error");
        }

    },
    onHomePress: function () {
        this.router.navTo("home");
    },
    onSynchronize: function () {
        util.Synchronize.begin();
    },

    onSynchronizeComplete: function () {
        this.synchroModel.setData({
            "dateTime": model.Synchronize.getDateTime(),
            "en": model.Synchronize.isSynchronized()
        });
        this.synchroModel.refresh();
    },

    setValue: function (evt) {
        console.log(evt);
        var value = evt.getSource().getValue();
        var material = evt.getSource().getBindingContext("material") ? evt.getSource().getBindingContext("material").getObject() : evt.getSource().getBindingContext("materialStock").getObject();
        material.QtyIssue = value;
    },

    checkMaterialQuantity: function () {

        var items = [];

        var materials = this.view.getModel("material").oData.results;
        var stock = this.view.getModel("materialStock").oData.results;

        var errorFound = false;

        //Carico i materiali dell'ordine
        for (var i = 0; i < materials.length; i++) {
            var mt = materials[i];

            // Check sulle quantità inserite
            var qi = parseFloat(mt.QtyIssue);
            var qr = parseFloat(mt.QtyStock);

            if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
                mt.QtyIssue_state = "Error";
                errorFound = true;
            } else {
                mt.QtyIssue = qi.toString();
                mt.QtyStock = qr.toString();
                mt.QtyIssue_state = "None";
            }
        }

        //Carico i materiali che sono solo in stock
        for (i = 0; i < stock.length; i++) {

            var mt = stock[i];

            // Check sulle quantità inserite
            var qi = parseInt(mt.QtyIssue);
            var qr = parseInt(mt.QtyStock);

            if ((!qi && qi !== 0) || (!qr && qr !== 0) || qi > qr) {
                mt.QtyIssue_state = "Error";
                errorFound = true;
            } else {
                mt.QtyIssue = qi.toString();
                mt.QtyStock = qr.toString();
                mt.QtyIssue_state = "None";
            }

        }
        this.getView().getModel("material").refresh();
        this.getView().getModel("materialStock").refresh();

        if (errorFound) {
            // this.getView().getModel("material").refresh();
            // this.getView().getModel("materialStock").refresh();
            sap.m.MessageBox.alert(this.getView().getModel("i18n").getProperty("CHECK_QUANTITY"));
            return false;
        }
        return true;
        // var odsModel = model.ODS.getModel();
        // var ods = odsModel.oData;
        //
        // for (var i = 0; i < ods.results.length; i++) {
        // 	var odst = ods.results[i];
        // 	if (odst.Aufnr == this.ods) {
        // 		odst.Materials.results = items;
        // 	}
        // }
        // odsModel.setData(ods);
        // // model.ODS.saveModel();
        //
        // this.getView().getModel("material").refresh();
        // this.getView().getModel("materialStock").refresh();

        // sap.m.MessageBox.alert(this.iModel.getProperty("SAVED"));
    },
    checkDateValue: function (model) {
        var iModel = this.getView().getModel("i18n");
        var newCounterValue = model.getProperty("/actualrecdv");
        var oldCounterValue = model.getProperty("/lastCounter") ? model.getProperty("/lastCounter") : model.getProperty("/Recdv");
        var newDate = model.getProperty("/closeDate") ? model.getProperty("/closeDate") : "";
        var oldDate = model.getProperty("/lastReadDate") ? model.getProperty("/lastReadDate") : model.getProperty("/Idate");
        oldDate = (!oldDate || oldDate === "") ? "" : util.Formatter._parseToDate(oldDate);
        // // console.log("(newCounterValue ==="" || newDate ===""):"+(newCounterValue ==="" || newDate ==="").toString());
        // // console.log("(newCounterValue>=oldCounterValue && newDate>=oldDate):"+(newCounterValue ==="" || newDate ==="").toString());
        // // console.log("(newCounterValue<oldCounterValue && newDate<oldDate):"+(newCounterValue<oldCounterValue && newDate<oldDate).toString());
        if (newDate !== "") {
            newDate.setHours(12);
        }
        var newDateState = (newCounterValue === "" || newDate === "" || oldCounterValue === "" || oldDate === "") ? true : (parseInt(newCounterValue) >= parseInt(oldCounterValue) && newDate >= oldDate) || (parseInt(newCounterValue) < oldCounterValue && newDate < oldDate);
        newDateState ? model.setProperty("/closeDate_state", "None") : model.setProperty("/closeDate_state", "Error");
        if (!newDateState) {
            sap.m.MessageToast.show(iModel.getProperty("DATE_COUNTER_ERROR"));
        }
        return newDateState;
    },
    onCounterChange: function (evt) {
        var value = evt.getSource().getValue();
        var model = this.getView().getDependents()[0].getModel("order") ? this.getView().getDependents()[0].getModel("order") : this.getView().getDependents()[0].getModel("multi");
        this.checkDateValue(model) ? evt.getSource().setValueState("None") : evt.getSource().setValueState("Error");
    },

    onMaterialsSearch: function (evt) {
        var val = evt.getParameter("newValue");
        var aFilters = [];
        aFilters.push(new sap.ui.model.Filter("Matnr", sap.ui.model.FilterOperator.Contains, val));
        aFilters.push(new sap.ui.model.Filter("MatText", sap.ui.model.FilterOperator.Contains, val));

        var oFilter = new sap.ui.model.Filter({
            filters: aFilters
        });
        this.getView().list.getBinding("items").filter([oFilter]);
        this.getView().listStock.getBinding("items").filter([oFilter]);
        //
        this.getView().list.getBinding("items").refresh();
        this.getView().listStock.getBinding("items").refresh();
    }
});
