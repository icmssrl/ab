jQuery.sap.declare("util.Synchronize");
jQuery.sap.require("model.Synchronize");

util.Synchronize = {

    dialog: new sap.m.BusyDialog({}),
    sync: true,
    log: "*** ",

    begin: function () {
        this.dialog.open();
        util.Common.setFunctionsContext(this);
        // model.User.logout(this.updateTimesheet, this.synchronizeError);
        log = "*** ";
        this.loadCountries();
    },

    loadCountries: function () {
        log = log + "Loading Coutries...";

        model.Countries.oDataToStorage(this.loadUser, this.synchronizeError);
    },

    loadUser: function () {
        log = log + "Loading User Data... ";

        model.User.oDataToStorage(this.loadTimesheetList, this.synchronizeError);
    },

    loadTimesheetList: function () {
        log = log + "Loading TimesheetList... ";

        model.TimesheetList.oDataToStorage(this.updateTimesheet, this.synchronizeError);
    },

    updateTimesheet: function () {
        log = log + "Updating Timesheet... ";

        model.Fiori.synchronizeTimesheet(this.updateODS, this.synchronizeError);
    },

    updateODS: function () {  // aggiunte funzioni -> FABIO 17/02/2016 spostata sopra di due posizioni
        log = log + "Updating ODS... ";

        //            model.Fiori.synchronizeODS(this.updateMaterial, this.synchronizeError);

        model.Fiori.synchronizeODS().then(this.updateMaterial, this.synchronizeError);  // aggiunte funzioni -> FABIO 17/02/2016
    },

    updateMaterial: function () {
        log = log + "Updating Material... ";

        //            model.Fiori.synchronizeMaterials(this.updateFasi, this.synchronizeError);

        model.Fiori.synchronizeMaterials().then(this.updateFasi, this.synchronizeError);  // aggiunte funzioni -> FABIO 17/02/2016
    },

    updateFasi: function () {
        log = log + "Updating Fasi... ";

        model.Fiori.synchronizeFasi(this.loadODS, this.synchronizeError);
    },

    //	updateODS: function () {
    //		log = log + "Updating ODS... ";
    //
    //		model.Fiori.synchronizeODS(this.loadODS, this.synchronizeError);
    //	},

    loadODS: function () {
        log = log + "Loading ODS... ";

        model.ODS.oDataToStorage(this.loadODST, this.synchronizeError);
    },

    loadODST: function () {
        log = log + "Loading ODST... ";

        model.ODST.oDataToStorage(this.loadTimesheet, this.synchronizeError);
    },

    loadTimesheet: function () {
        log = log + "Loading Timesheet... ";

        model.Timesheet.oDataToStorage(this.synchronizeComplete, this.synchronizeError);
    },

    synchronizeComplete: function () {
        log = log + "Synch Complete! ";
        var iModel = model.i18n.getModel();

        sap.m.MessageBox.alert(iModel.getProperty("SYNCHRONIZE_COMPLETE"));
        this.dialog.close();
        var lang = sap.ui.getCore().getConfiguration().getLanguage();
        model.Synchronize.setDateTime(lang);
        sap.ui.getCore().getEventBus().publish("synchronize", "complete");
    },

    synchronizeError: function (e) {
        var iModel = model.i18n.getModel();
        this.dialog.close();

        var message = iModel.getProperty("SYNCHRONIZE_ERROR");

        message = message + ": " + util.Error.getErrorMessage(e);
        sap.m.MessageBox.alert(message);
    },

    refreshConnection: function () {
        log = log + "Loading Timesheet... ";

        var username = model.Credential.getUsername();
        var password = model.Credential.getPassword();
        var tok = username + ':' + password;
        var hash = btoa(tok);
        var auth = "Basic " + hash;

        console.log(hash);
        console.log(auth);

        $.ajaxSetup({
            headers: {
                "Authorization": auth
            }
        });

        var success = function () {
            console.log("refreshConnection")
        };
        success = $.proxy(success, this);

        var error = function (e) {
            console.error(e)
        };
        error = $.proxy(error, this);

        var url = setting.Core.serverUrl + "ZFIORI_SRV/$metadata"

        $.ajax({
            url: url,
            type: "GET", //or POST?
            // dataType: "jsonp",
            xhrFields: {
                withCredentials: true
            },
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", auth);
            },
            success: success,
            error: error
        });
    },
    // ---------------------------------------
};
