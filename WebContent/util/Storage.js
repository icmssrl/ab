jQuery.sap.declare("util.Storage");
jQuery.sap.require("jquery.sap.storage");

util.Storage = {
		
		put: function(id, data)
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.put(id, data);
		},
		
		get: function(id)
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);  
			return oStorage.get(id);
		},

		remove: function(id)
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);  
			oStorage.remove(id); 
		}
};