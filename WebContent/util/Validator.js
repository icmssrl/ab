jQuery.sap.declare("util.Validator");


util.Validator = {
	required: function(control) {
        var val = control.getValue();
        if (val !== undefined && val !== "") {
            control.setValueState("Success");
            return true;
        } else {
            control.setValueState("Error");
            return false;
        }
    },
};