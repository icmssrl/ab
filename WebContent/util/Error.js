jQuery.sap.declare("util.Error");
jQuery.sap.require("sap.m.MessageBox");

util.Error =
{
	showStandardError: function(err)
	{
		var jsonErr = JSON.parse(err.response.body);
		console.log(jsonErr);
		sap.m.MessageBox.alert("Errore: " + jsonErr.error.message.value);
	},

	getErrorMessage: function(error) {
		var message = "";
		if (error !== undefined && error.response && error.response.body) {
			var body = error.response.body;
			var msg = JSON.parse(body);
			if (msg !== undefined && msg.error !== undefined && msg.error.innererror !== undefined) {

				if (msg.error.message && msg.error.message.value)
				{
					return msg.error.message.value;
				}

				if (msg.error.innererror.errordetails !== undefined)
				{
					var msgs = msg.error.innererror.errordetails;
					for (var i = 0; i < msgs.length; i++) {
						var item = msgs[i];
						if (item.code && item.message) {
							message = message + item.message + " (" + item.code + ")" + ".\n";
						}

						if (item.message && !item.code) {
							message = message + item.message + ".\n ";
							break;
						}
					}
				}
			}
			return message;
		}

		if (error.message && error.name) {
			message = error.name + ": " + error.message;
			return message;
		}

		if (error) {
			return error.toString();
		}
		return message;
	},

};
