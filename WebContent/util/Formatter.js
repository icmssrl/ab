jQuery.sap.declare("util.Formatter");


jQuery.sap.require("sap.ui.core.format.DateFormat");

// jQuery.sap.require("util.DebugDate");

util.Formatter = {
    date: function (value) {
        if (value) {
            if (typeof (value) !== "string") {
                if (value.getHours() === 0) {
                    value.setHours(12)
                }
            }
            var lang = sap.ui.getCore().getConfiguration().getLanguage();
            if (lang !== "it" && lang !== "it-IT") {
                var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                    pattern: "MM-dd-yyyy"
                });
            } else {
                var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                    pattern: "dd-MM-yyyy"
                });
            }
            var date = new Date(value);
            date.setDate(date.getUTCDate())
            var date = oDateFormat.format(date);

            if (date.indexOf("NaN") > -1) {
                return "";
            } else {
                return date;
            }
        } else {
            return "";
        }
    },

    time: function (value) {

        if (value === "" || value === undefined) {
            return "00:00";
        }

        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;

        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
            pattern: "HH:mm"
        });
        var timeStr = timeFormat.format(new Date(value.ms + TZOffsetMs));
        return timeStr;
    },

    timeToMs: function (value) {
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
            pattern: "HH:mm"
        });
        return timeFormat.parse(value).getTime() - TZOffsetMs;
    },

    timeToMsNoTz: function (value) {
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
            pattern: "HH:mm"
        });
        return timeFormat.parse(value).getTime();
    },

    dateSap: function (value) {
        //FIXME
        //added to modify report---
        value = new Date(value);
        //------------------
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000 * 2;
        return new Date(value.getTime() - TZOffsetMs);
    },
    dateToSap: function (value) {
        var now = new Date();
        if (now.getTimezoneOffset() < 0) {
            var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000 * 2;
            return new Date(value.getTime() - TZOffsetMs);
        }
        return new Date(value);
    },

    dateTimeSap: function (value) {
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddTHH%3Amm%3Ass"
        });
        var date = this._parseToDate(value);
        var timeStr = oDateFormat.format(date);
        return timeStr;
    },
    // dateTimeSap: function(value) {
    // 	var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
    // 		pattern: "yyyy-MM-ddTHH%3Amm%3Ass"
    // 	});
    // 	var timeStr = oDateFormat.format(new Date(value));
    // 	return timeStr;
    // },
    //
    // dateTimeSapEntry: function(value) {
    // 	if (value === null || value === undefined || value == "") {
    // 		return null;
    // 	}
    // 	var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
    // 		pattern: "yyyy-MM-ddTHH:mm:ss"
    // 	});
    // 	var timeStr = oDateFormat.format(new Date(value));
    // 	return timeStr;
    // },

    dateTimeFromSap: function (value) {
        if (value === null || value === undefined || value == "") {
            return null;
        }
        if (value.getTimezoneOffset() > 0) {
            var timezoneMs = value.getTimezoneOffset() * 60 * 1000;
            var dateTime = value.getTime();
            return new Date(dateTime + timezoneMs);
        } else
            return new Date(value);

    },
    dateTimeSapEntry: function (value) {

        if (value === null || value === undefined || value == "") {
            return null;
        }

        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddTHH:mm:ss"
        });

        if (value.toISOString) {
            var date = value;
        } else {
            var date = this._parseToDate(value);
        }

        var timeStr = oDateFormat.format(date);
        return timeStr;

    },
    _parseToDate: function (value) {
        var infos = this._stringToDatePiece(value);
        var date = new Date();
        //		date.setDate(infos.day);
        date.setYear(infos.year);
        date.setMonth(infos.month - 1);
        //		date.setYear(infos.year);
        date.setDate(infos.day);
        date.setMinutes(infos.minute);
        date.setHours(infos.hour);
        date.setSeconds(infos.second);
        return date;

    },
    _stringToDatePiece: function (value) {
        var datePiece = value.split(/[-,T,:,Z]/);
        return {
            year: datePiece[0],
            month: datePiece[1],
            day: datePiece[2],
            hour: datePiece[3],
            minute: datePiece[4],
            second: datePiece[5]
        };

    },
    timeSap: function (value) {
        var hours;
        var minutes;
        var seconds;

        if (value === "" || value === undefined) {
            hours = "00";
            minutes = "00";
            seconds = "00";
        } else {
            var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
                pattern: "HH"
            });
            hours = timeFormat.format(new Date(value.getTime()));
            timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
                pattern: "mm"
            });
            minutes = timeFormat.format(new Date(value.getTime()));
            timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
                pattern: "ss"
            });
            seconds = timeFormat.format(new Date(value.getTime()));
        }

        return "PT" + hours + "H" + minutes + "M" + seconds + "S";
    },

    flagToSap: function (value) {
        if (value === true) {
            return "X";
        }
        if (value === false) {
            return "";
        }
        return value;
    },

    stateDateTenDays: function (date) {
        var lead = new Date();
        lead.setDate(lead.getDate() - 10);
        return (date < lead) ? sap.ui.core.ValueState.Error : sap.ui.core.ValueState.Success;
    },

    removeLeadingZero: function (data) {
        return data.replace(/^0+/, '');
    },

    reportListItemType: function (elab) {
        if (elab === "D") {
            return sap.m.ListType.Inactive
        } else {
            return sap.m.ListType.Active;
        }
    },

    currentTime: function () {
        var today = new Date();
        var zero = new Date(0);
        today.setYear(zero.getYear());
        today.setMonth(zero.getMonth());
        today.setDate(zero.getDate());
        today.setMinutes(today.getMinutes() - today.getTimezoneOffset());

        var time = {
            __edmType: "Edm.Time",
            ms: today.getTime()
        };
        return time;
    },

    DateISOtoDate: function (value) {
        if (!value) {
            return undefined;
        }
        value = new Date(value);
        //        date.setDate(date.getUTCDate())
        return value;
    },

    dateShortToDateISO: function (value) {
        var lang = sap.ui.getCore().getConfiguration().getLanguage();
        if (lang !== "it" && lang !== "it-IT") {
            var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                pattern: "MM-dd-yyyy"
            });
        } else {
            var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                pattern: "dd-MM-yyyy"
            });
        }

        var date = new Date(oDateFormat.parse(value));
        //        date.setMinutes(-date.getTimezoneOffset());
        return date;
    },

    transfer: function (value) {
        if (value) {
            if (value === "E") {
                return model.i18n.getModel().getProperty("FOREIGN");
            } else {
                return model.i18n.getModel().getProperty("NATIONAL");
            }
        } else {
            return "";
        }
    },

    totWorkingHours: function (begin, end, pause) {
        begin = begin.ms;
        end = end.ms;
        pause = pause.ms;

        var duration = end - begin - pause;

        var time = util.Formatter.msToTime(duration);

        return time.hours + ":" + time.minutes;
    },

    totTravelTime: function (andata, ritorno) {
        var duration = andata.ms + ritorno.ms;
        var time = util.Formatter.msToTime(duration);
        return time.hours + ":" + time.minutes;
    },

    timeToPrint: function (duration) {
        var time = util.Formatter.msToTime(duration.ms);
        return time.hours + ":" + time.minutes;
    },

    msToHHMM: function (ms) {
        var time = util.Formatter.msToTime(ms);
        return time.hours + ":" + time.minutes;
    },

    msToTime: function (duration) {
        var milliseconds = parseInt((duration % 1000) / 100),
            seconds = parseInt((duration / 1000) % 60),
            minutes = parseInt((duration / (1000 * 60)) % 60),
            hours = parseInt((duration / (1000 * 60 * 60)) % 24);

        var time = {};

        time.milliseconds = milliseconds;
        time.hours = (hours < 10) ? "0" + hours : hours;
        time.minutes = (minutes < 10) ? "0" + minutes : minutes;
        time.seconds = (seconds < 10) ? "0" + seconds : seconds;


        return time;
    },

    toInt: function (data) {
        if (data) return parseInt(data);
        return 0;
    },

    formatClientName: function (value) {
        if (value) {
            var index = value.indexOf("/");
            if (index > 0) {
                return value.substr(0, index);
            } else {
                return value;
            }

        } else {
            return "";
        }

    },

    formatDateToUrl: function (value) {
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "dd-MM-yyyy"
        });
        return dateFormat.format(value);
    },

    formatUrlToDate: function (value) {
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
            pattern: "dd-MM-yyyy"
        });
        return dateFormat.parse(value);
    },

    formatDateToSapFormat: function (value) {
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-dd"
        });
        var date = oDateFormat.format(value);
        // date.setMinutes(-date.getTimezoneOffset());
        return date;
    },

    getCurrentDateTimeToShow: function (lang) {
        var date = new Date();
        var hours = date.getHours() > 9 ? date.getHours() : "0" + date.getHours();
        var minutes = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
        // var seconds  = date.getSeconds() > 9 ? date.getSeconds(): "0"+date.getSeconds();
        var year = date.getFullYear();
        var month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1);
        var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();

        if (lang !== "it" && lang !== "it-IT") {
            return "" + hours + ":" + minutes + "  " + month + "/" + day + "/" + year;
        } else {
            return "" + hours + ":" + minutes + "  " + day + "/" + month + "/" + year;
        }
    },

    formatKm: function (value) {
        if (value == "")
            return 0;
        return parseFloat(value);
    }
};
