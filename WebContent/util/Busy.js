jQuery.sap.declare("util.Busy");

util.Busy = {

  busyDialog: new sap.m.BusyDialog(),

  show: function () {
    //sap.ui.core.BusyIndicator.show();
    util.Busy.busyDialog.open();
  },

  hide: function () {
    //sap.ui.core.BusyIndicator.hide();
    util.Busy.busyDialog.close();
  }
};