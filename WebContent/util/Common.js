jQuery.sap.declare("util.Common");

util.Common = {
	
		/*
		 * Imposta per tutte le funzioni dell'oggetto se stesso come contesto 
		 */
		setFunctionsContext: function(object)
		{
			for (var i in object)
			{
				if (typeof(object[i]) == "function")
				{
					object[i] = $.proxy(object[i], object);
				}
			}
		},
		
		checkEmptyObject: function(obj)
		{
			if (obj === undefined || obj === null || obj == "")
			{
				return true;
			}
			else
			{
				return false;
			}
		},
		
		
};