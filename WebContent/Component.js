jQuery.sap.declare("abgroup.Component");

sap.ui.core.UIComponent.extend("abgroup.Component", {
    metadata: {

        config: {
            resourceBundle: "i18n/text.properties",
        },
        includes : [  //importare css di custom e lib

			"libs/lodash.js",
			"libs/q.js"

			],

        routing: {
            config: {
                viewType: "JS",
                viewPath: "view",
                targetControl: "navContainer",
                targetAggregation: "pages",
                clearTarget: false,
								transition: "show"
            },
            routes: [{
                    pattern: "home",
                    name: "home",
                    view: "Home",
                }, {
                    pattern: "",
                    name: "login",
                    view: "Login"
                }, {
                    pattern: "timesheet/reports/{day}",
                    name: "timesheet.Reports",
                    view: "timesheet.Reports",
                    subroutes:
                    	[{
                            pattern: "timesheet/reportdetail/:?query:",
                            name: "timesheet.ReportDetail",
                            view: "timesheet.ReportDetail"
                        },
                    	 ]
                }, {
	            	pattern: "timesheet/newreport/:day:",
	                name: "timesheet.NewReport",
	                view: "timesheet.ReportDetail",
                }, {
	            	pattern: "timesheet/list",
	                name: "timesheet.List",
	                view: "timesheet.List",
                }, {
	            	pattern: "timesheet/header",
	                name: "timesheet.ReportsHeader",
	                view: "timesheet.ReportsHeader",
                },
                {
                  pattern: "materials",
                    name: "materials.ClientOrders",
                    view: "materials.ClientOrders",
                    subroutes:
                    [{
                      pattern: "materials/{headquarter}/{name}",
                        name: "materials.ServiceOrders",
                        view: "materials.ServiceOrders",
                        // subroutes:	[{
      	                // 		pattern: "materials/{headquarter}/{name}/{ods}",
      	    	          //       name: "materials.Materials",
      	    	          //       view: "materials.Materials",
      	                // 	}]
                    }]
                },
                {
                  pattern: "materials/{headquarter}/{name}/steps",
  	              name: "materials.Steps",
  	              view: "materials.Steps",
                  subroutes:
                  [
                    {

                        pattern: "materialsSteps/{headquarter}/{name}/{numOrder}",
        	              name: "materials.StepMaterials",
        	              view: "materials.StepMaterials"

                    }
                  ]
                },


                // {
	            	// pattern: "materials/{headquarter}",
	              //   name: "materials.ServiceOrders",
	              //   view: "materials.ServiceOrders",
	              //   subroutes:
	              //   	[{
	              //   		pattern: "materials/{ods}",
	    	        //         name: "materials.Materials",
	    	        //         view: "materials.Materials",
	              //   	}]
                // },

            ]
        }
    },

    destroy: function() {
        if (this.routeHandler) {
            this.routeHandler.destroy();
        }
        sap.ui.core.UIComponent.destroy.apply(this, arguments);
    },



    // ATTENZIONE, QUESTA PARTE E' DA SOSTITUIRE CON ROOTVIEW NEL METADATA
    createContent: function() {
        this.view = sap.ui.view({
            id: "app",
            viewName: "view.App",
            type: sap.ui.core.mvc.ViewType.JS
        });

        // Testi
        var oI18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl: [this.getMetadata().getConfig().resourceBundle]
        });
        this.setModel(oI18nModel, "i18n");

        var appStatusModel =  new sap.ui.model.json.JSONModel();
        appStatusModel.setData({});
        sap.ui.getCore().setModel(appStatusModel, "appStatus");

        jQuery.sap.require("sap.ui.core.routing.History");
        jQuery.sap.require("sap.m.routing.RouteMatchedHandler");

        //Require
				jQuery.sap.require("model.Countries");
        jQuery.sap.require("model.Credential");
        jQuery.sap.require("model.Fiori");
        jQuery.sap.require("model.Material");
        jQuery.sap.require("model.ODS");
        jQuery.sap.require("model.ODST");
        jQuery.sap.require("model.Timesheet");
        jQuery.sap.require("model.TimesheetList");
        jQuery.sap.require("model.User");
        jQuery.sap.require("model.Time");
        jQuery.sap.require("model.i18n");

        jQuery.sap.require("util.Common");
        jQuery.sap.require("util.Formatter");
        jQuery.sap.require("util.Storage");
        jQuery.sap.require("util.Validator");
        jQuery.sap.require("util.Synchronize");
				jQuery.sap.require("util.Busy");

        jQuery.sap.require("setting.Core");
        //---------------------------------

        var router = this.getRouter();
        this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
        router.initialize();

        router.myNavBack = function(route, data) {
            var h = sap.ui.core.routing.History.getInstance();
            var url = this.getURL(route, data);
            var direction = h.getDirection(url);
            if ("Backwards" === direction || "NewEntry" === direction) {
                window.history.go(-1);
            } else {
                var replace = true;
                this.navTo(route, data, replace);
            }
        };
        router.myNavBack = jQuery.proxy(router.myNavBack, router);

        return this.view;
    },

});
